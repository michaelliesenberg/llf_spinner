
#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <string.h>
#include <util/delay.h>

#include "serialport1_PC.h"
#include "serialport2_PLD.h"
#include "crcCalc.h"
#include "protokollparser.h"
#include "main.h"
#include "phy.h"

void (*jump) (void) = (void (*)(void))((BOOT_SECTION_START)/2);

void send_IRT_HEADER(COMMAND_TYPE command, RESPONSE_TYPE response);
void send_IRT_BODY(COMMAND_TYPE command, RESPONSE_TYPE response);

void vScanCommandLine() 
{
	if(bCommandHeader || bCommandBody)
	{
		bCommandHeader=false;
		bCommandBody=false;
		
		if(PC_header.COMMAND == START_UPDATE)
		{
			if(!bCommandReceive_Bootloader_Otherside)//If command didnt came from other side
			{
				send_IRT_HEADER(START_UPDATE,OK_STRUCT);
				bWrite_To_Nvm(NVM_BootLoader_UART, 0);//Tell Bootloader to use UART1
			}
			else
			{
				vSendDiagnosticToOtherSide();
				bWrite_To_Nvm(NVM_BootLoader_UART, 1);//Tell Bootloader to use UART2
			}
						
			usart_rx_disable(USART_1);
			usart_tx_disable(USART_1);
			usart_rx_disable(USART_2);
			usart_tx_disable(USART_2);
			
			bWrite_To_Nvm(NVM_BootLoader_BOARD_Type, is_FPGA_Board);//Tell Bootloader board type
			//bWrite_To_Nvm(NVM_BootLoader_ENTRY_CODE, 0x00);//Entry Bootloader with EEPROM
			
			EIND = 1;
			_delay_ms(1000);
			jump();
		}
		else if(PC_header.COMMAND == START_UPDATE_OTHERSIDE)
		{
			bCommandSend_Bootloader_Otherside=true;
		}
		else if(PC_header.COMMAND == START_UPDATE_MAKE_BRIDGE || PC_header.COMMAND == START_UPDATE_OTHERSIDE_ACKNOWLEDGE)
		{
			bCommandSend_Bootloader_Otherside=false;
			bCommandUpdate_Otherside=true;
			send_IRT_HEADER(START_UPDATE_OTHERSIDE,OK_STRUCT);
		}
		else if(PC_header.COMMAND == GET_BODY)
		{	
			if(bOtherSideInBootloader)
				send_IRT_BODY(GET_BODY,OtherSide_In_Bootloader);
			else
				send_IRT_BODY(GET_BODY,OK_STRUCT);
		}
		else if(PC_header.COMMAND == RESET_IRT)
		{
			bSendResettoOtherSide=true;
			if(bIsBody)
			{
				IRT_body.BODY.SW_RESET=1;
			}
			else
			{
				IRT_body.HOLLOW.SW_RESET=1;
			}
		}
		else if(PC_header.COMMAND == SET_CONFIG)
		{
			bWriteConfigToNvm();
		}
	}
	else
	{
		if(bErrorReceivingStruct)
		{
			bErrorReceivingStruct=false;
			send_IRT_HEADER(NORMAL_OPERATION, BAD_STRUCT);
			_delay_ms(100);
			ucReceiverIndex=0;
		}
	}
}

void send_IRT_HEADER(COMMAND_TYPE command, RESPONSE_TYPE response)
{
	IRT_header.COMMAND = command;
	IRT_header.RESPONSE = response;
	IRT_header.VERSION = SW_VERSION_HEADER;
	IRT_header.SIZEBYTES = sizeof(IRT_header);
	

	//memcpy(cCommandBuffer,&IRT_header, sizeof(IRT_header) * sizeof(uint8_t));
	cCommandBuffer[0]=IRT_header.COMMAND;
	cCommandBuffer[1]=IRT_header.RESPONSE;
	cCommandBuffer[2]=(uint8_t)(IRT_header.VERSION>>8);
	cCommandBuffer[3]=(uint8_t)(IRT_header.VERSION);
	cCommandBuffer[4]=(uint8_t)(IRT_header.SIZEBYTES>>8);
	cCommandBuffer[5]=(uint8_t)(IRT_header.SIZEBYTES);
	IRT_header.CRC_CHECK =uiCalculateCrc(&cCommandBuffer[0], ((sizeof(IRT_header)*sizeof(uint8_t)) -2));
	cCommandBuffer[6]=(uint8_t)(IRT_header.CRC_CHECK>>8);
	cCommandBuffer[7]=(uint8_t)(IRT_header.CRC_CHECK);
	
	ucBytesInCommandBuffer=sizeof(IRT_header)*sizeof(uint8_t);

	uint8_t i = 0;
	for(i=0; i<ucBytesInCommandBuffer; i++)
	{
		uint8_t byteToSend = cCommandBuffer[i];
		usart_putchar(USART_1, byteToSend);
	}
}
void send_IRT_BODY(COMMAND_TYPE command, RESPONSE_TYPE response)
{
	IRT_header.COMMAND = command;
	IRT_header.RESPONSE = response;
	IRT_header.VERSION = SW_VERSION_HEADER;
	IRT_header.SIZEBYTES = sizeof(IRT_header);
	
	
	if(is_FPGA_Board)
		IRT_body.board_type = FPGA;
	else
		IRT_body.board_type = TLK;
	
	cCommandBuffer[0]=IRT_header.COMMAND;
	cCommandBuffer[1]=IRT_header.RESPONSE;
	cCommandBuffer[2]=(uint8_t)(IRT_header.VERSION>>8);
	cCommandBuffer[3]=(uint8_t)(IRT_header.VERSION);
	cCommandBuffer[4]=(uint8_t)(IRT_header.SIZEBYTES>>8);
	cCommandBuffer[5]=(uint8_t)(IRT_header.SIZEBYTES);
	
	IRT_header.CRC_CHECK =uiCalculateCrc(&cCommandBuffer[0], ((sizeof(IRT_header)*sizeof(uint8_t)) -2));
	
	cCommandBuffer[6]=(uint8_t)(IRT_header.CRC_CHECK>>8);
	cCommandBuffer[7]=(uint8_t)(IRT_header.CRC_CHECK);
		
	cCommandBuffer[8]=(uint8_t)(IRT_body.irt_status);
	cCommandBuffer[9]=(uint8_t)(IRT_body.board_type);
	cCommandBuffer[10]=(uint8_t)(IRT_body.irt_type);
	cCommandBuffer[11]=0;
	
	cCommandBuffer[12]=(uint8_t)(IRT_body.BODY.VALUE_1V1_1V8>>8);
	cCommandBuffer[13]=(uint8_t)(IRT_body.BODY.VALUE_1V1_1V8);
	cCommandBuffer[14]=(uint8_t)(IRT_body.BODY.VALUE_3V3>>8);
	cCommandBuffer[15]=(uint8_t)(IRT_body.BODY.VALUE_3V3);
	
	cCommandBuffer[16]=(uint8_t)(IRT_body.BODY.VALUE_5V>>8);
	cCommandBuffer[17]=(uint8_t)(IRT_body.BODY.VALUE_5V);
	cCommandBuffer[18]=(uint8_t)(IRT_body.BODY.VALUE_n_5V>>8);
	cCommandBuffer[19]=(uint8_t)(IRT_body.BODY.VALUE_n_5V);
	
	cCommandBuffer[20]=(uint8_t)(IRT_body.BODY.VALUE_2V5>>8);
	cCommandBuffer[21]=(uint8_t)(IRT_body.BODY.VALUE_2V5);
	cCommandBuffer[22]=(uint8_t)(IRT_body.BODY.VALUE_I_3V3>>8);
	cCommandBuffer[23]=(uint8_t)(IRT_body.BODY.VALUE_I_3V3);
	
	cCommandBuffer[24]=(uint8_t)(IRT_body.BODY.VALUE_I_5V>>8);
	cCommandBuffer[25]=(uint8_t)(IRT_body.BODY.VALUE_I_5V);
	cCommandBuffer[26]=0x00;
	cCommandBuffer[27]=0x00;
	
	cCommandBuffer[28]=(uint8_t)(IRT_body.BODY.GAIN>>8);
	cCommandBuffer[29]=(uint8_t)(IRT_body.BODY.GAIN);
	cCommandBuffer[30]=(uint8_t)(IRT_body.BODY.GAIN_MIN>>8);
	cCommandBuffer[31]=(uint8_t)(IRT_body.BODY.GAIN_MIN);
	
	cCommandBuffer[32]=(uint8_t)(IRT_body.BODY.GAIN_MAX>>8);
	cCommandBuffer[33]=(uint8_t)(IRT_body.BODY.GAIN_MAX);
	cCommandBuffer[34]=(uint8_t)(IRT_body.BODY.TEMPERATURE>>8);
	cCommandBuffer[35]=(uint8_t)(IRT_body.BODY.TEMPERATURE);
	
	cCommandBuffer[36]=(uint8_t)(IRT_body.BODY.PHY0_DUPPLEX);
	cCommandBuffer[37]=(uint8_t)(IRT_body.BODY.PHY1_DUPPLEX);
	cCommandBuffer[38]=(uint8_t)(IRT_body.BODY.LLF_PHY0);
	cCommandBuffer[39]=(uint8_t)(IRT_body.BODY.LLF_PHY1);
	
	cCommandBuffer[40]=(uint8_t)(IRT_body.BODY.SW_RESET);
	cCommandBuffer[41]=(uint8_t)(IRT_body.BODY.NEW_CONFIGURATION);
	cCommandBuffer[42]=0x00;
	cCommandBuffer[43]=0x00;
	
	cCommandBuffer[44]=(uint8_t)(IRT_body.BODY.SW_VERSION_1);
	cCommandBuffer[45]=(uint8_t)(IRT_body.BODY.SW_VERSION_2);
	cCommandBuffer[46]=(uint8_t)(IRT_body.BODY.SW_VERSION_3);
	cCommandBuffer[47]=(uint8_t)(IRT_body.BODY.SW_VERSION_4);
	cCommandBuffer[48]=(uint8_t)(IRT_body.BODY.SW_VERSION_5);
	cCommandBuffer[49]=(uint8_t)(IRT_body.BODY.SW_VERSION_6);
	cCommandBuffer[50]=(uint8_t)(IRT_body.BODY.SW_VERSION_7);
	cCommandBuffer[51]=(uint8_t)(IRT_body.BODY.SW_VERSION_8);
	
	cCommandBuffer[52]=(uint8_t)(IRT_body.HOLLOW.VALUE_1V1_1V8>>8);
	cCommandBuffer[53]=(uint8_t)(IRT_body.HOLLOW.VALUE_1V1_1V8);
	cCommandBuffer[54]=(uint8_t)(IRT_body.HOLLOW.VALUE_3V3>>8);
	cCommandBuffer[55]=(uint8_t)(IRT_body.HOLLOW.VALUE_3V3);
	
	cCommandBuffer[56]=(uint8_t)(IRT_body.HOLLOW.VALUE_5V>>8);
	cCommandBuffer[57]=(uint8_t)(IRT_body.HOLLOW.VALUE_5V);
	cCommandBuffer[58]=(uint8_t)(IRT_body.HOLLOW.VALUE_n_5V>>8);
	cCommandBuffer[59]=(uint8_t)(IRT_body.HOLLOW.VALUE_n_5V);
	
	cCommandBuffer[60]=(uint8_t)(IRT_body.HOLLOW.VALUE_2V5>>8);
	cCommandBuffer[61]=(uint8_t)(IRT_body.HOLLOW.VALUE_2V5);
	cCommandBuffer[62]=(uint8_t)(IRT_body.HOLLOW.VALUE_I_3V3>>8);
	cCommandBuffer[63]=(uint8_t)(IRT_body.HOLLOW.VALUE_I_3V3);

	cCommandBuffer[64]=(uint8_t)(IRT_body.HOLLOW.VALUE_I_5V>>8);
	cCommandBuffer[65]=(uint8_t)(IRT_body.HOLLOW.VALUE_I_5V);
	cCommandBuffer[66]=0x00;
	cCommandBuffer[67]=0x00;
	
	cCommandBuffer[68]=(uint8_t)(IRT_body.HOLLOW.GAIN>>8);
	cCommandBuffer[69]=(uint8_t)(IRT_body.HOLLOW.GAIN);
	cCommandBuffer[70]=(uint8_t)(IRT_body.HOLLOW.GAIN_MIN>>8);
	cCommandBuffer[71]=(uint8_t)(IRT_body.HOLLOW.GAIN_MIN);
	
	cCommandBuffer[72]=(uint8_t)(IRT_body.HOLLOW.GAIN_MAX>>8);
	cCommandBuffer[73]=(uint8_t)(IRT_body.HOLLOW.GAIN_MAX);
	cCommandBuffer[74]=(uint8_t)(IRT_body.HOLLOW.TEMPERATURE>>8);
	cCommandBuffer[75]=(uint8_t)(IRT_body.HOLLOW.TEMPERATURE);
	
	cCommandBuffer[76]=(uint8_t)(IRT_body.HOLLOW.PHY0_DUPPLEX);
	cCommandBuffer[77]=(uint8_t)(IRT_body.HOLLOW.PHY1_DUPPLEX);
	cCommandBuffer[78]=(uint8_t)(IRT_body.HOLLOW.LLF_PHY0);
	cCommandBuffer[79]=(uint8_t)(IRT_body.HOLLOW.LLF_PHY1);
	
	cCommandBuffer[80]=(uint8_t)(IRT_body.HOLLOW.SW_RESET);
	cCommandBuffer[81]=(uint8_t)(IRT_body.HOLLOW.NEW_CONFIGURATION);
	cCommandBuffer[82]=0x00;
	cCommandBuffer[83]=0x00;
	
	cCommandBuffer[84]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_1);
	cCommandBuffer[85]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_2);
	cCommandBuffer[86]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_3);
	cCommandBuffer[87]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_4);
	cCommandBuffer[88]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_5);
	cCommandBuffer[89]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_6);
	cCommandBuffer[90]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_7);
	cCommandBuffer[91]=(uint8_t)(IRT_body.HOLLOW.SW_VERSION_8);
	
	cCommandBuffer[92]=0;
	cCommandBuffer[93]=0;
	
	IRT_body.CRCSUM = uiCalculateCrc(&cCommandBuffer[8], ((sizeof(IRT_body)*sizeof(uint8_t)) -2));
	
	cCommandBuffer[94]=(uint8_t)(IRT_body.CRCSUM>>8);
	cCommandBuffer[95]=(uint8_t)(IRT_body.CRCSUM);
	
	ucBytesInCommandBuffer=sizeof(IRT_body)+sizeof(IRT_header);
	
	uint8_t i = 0;
	for(i=0; i<ucBytesInCommandBuffer; i++)
	{
		uint8_t byteToSend = cCommandBuffer[i];
		usart_putchar(USART_1, byteToSend);
	}
}
