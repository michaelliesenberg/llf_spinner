
#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <string.h>
#include <stdio.h>

#include "nvmManagement.h"
#include "crcCalc.h"
#include "serialport1_PC.h"
#include "main.h"


bool bNvmConfigIsEmpty(void) 
{
	nvm_eeprom_read_buffer(NVM_CONFIG_START, &PC_body_UART, sizeof(PC_BODY)*sizeof(uint8_t));
	
	if(bReceivedCrcIsCorrectBYTES(&PC_body_UART, (sizeof(PC_BODY) * sizeof(uint8_t))-2) == PC_body_UART.CRC_Checksum)
	{
		memcpy(&PC_body,&PC_body_UART, sizeof(PC_BODY) * sizeof(uint8_t));
		return false;
	}

    return true;
}
bool bWriteConfigToNvm(void) 
{
	nvm_eeprom_erase_and_write_buffer(NVM_CONFIG_START, &PC_body, sizeof(PC_BODY)*sizeof(uint8_t));
    return true;
}
bool bWrite_To_Nvm(uint16_t address, uint8_t value) 
{
	nvm_eeprom_erase_and_write_buffer(address, &value, sizeof(uint8_t));
    return true;
}