/*
 * main.h
 *
 * Created: 25.01.2018 10:12:49
 *  Author: eyermann
 */ 


#ifndef MAIN_H_
#define MAIN_H_


extern uint16_t uiMsMainLoopCounter;
extern uint8_t is_FPGA_Board;
extern uint8_t SW_VERSION [8];
extern uint16_t SW_VERSION_HEADER;
extern bool bIsBody;
extern bool bFlagUART_MODE;
#endif /* MAIN_H_ */