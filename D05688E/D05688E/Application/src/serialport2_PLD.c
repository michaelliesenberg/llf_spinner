/*
 * serialport_2.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <stdbool.h>
#include <string.h>
#include <avr/interrupt.h>
#include <nvm.h>
#include <limits.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "serialport2_PLD.h"
#include "serialport1_PC.h"
#include "boardAdc.h"
#include "main.h"
#include "phy.h"
#include "crcCalc.h"

bool bIsSending = false;

RECEIVE_TYPE uart2_receive_type = RECEIVE_HEADER;

uint8_t aucRcvBuffer[BUFFER_SIZE];
uint8_t aucRcvBuffer_Helper[BUFFER_SIZE];
uint16_t crcHelp=0;
uint16_t crcRecevied=0;
uint16_t error_counter=0;
uint16_t helpCRC=0;

uint16_t uiVPHYODupplestherSide=0;
uint16_t uiVPHY1DupplestherSide=0;
uint8_t ucOtherSideReceivedByteIndex=0;

Bool bSendBodytoOtherSide=false;
Bool bOtherSideInBootloader=false;
Bool bSendResettoOtherSide=false;
Bool bSendConfigtoOtherSide=false;
void vSendWordToOtherSide(uint16_t uiData);
void vSendCharToOtherSide(uint8_t uiData);
void setData(void);
void reset_MCU(void);
void sendHeaderToOtherSide(void);
HEADER IRT_UART2_header;

void vUART2Init(void) {

    static usart_rs232_options_t usart_2_options = {
        .baudrate = USART_2_BAUDRATE,
        .charlength = USART_2_CHAR_LENGTH,
        .paritytype = USART_2_PARITY,
        .stopbits = USART_2_STOP_BIT,
    };
    ioport_set_pin_dir(USART_2_TX, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(USART_2_RX, IOPORT_DIR_INPUT);
    usart_init_rs232(USART_2, &usart_2_options);
	usart_set_rx_interrupt_level(USART_2, USART_INT_LVL_MED);
}

void vSendWordToOtherSide(uint16_t uiData) 
{
    usart_putchar(USART_2, uiData >> 8);
    usart_putchar(USART_2, uiData & 0xFF);
}
void vSendCharToOtherSide(uint8_t uiData)
{
	usart_putchar(USART_2, uiData);
}

void sendHeaderToOtherSide(void)
{
//Send Header
	if(bCommandSend_Bootloader_Otherside)
	{
		IRT_UART2_header.COMMAND= START_UPDATE;
		bSendBodytoOtherSide=false;
	}
	else if(bCommandReceive_Bootloader_Otherside)
	{
		IRT_UART2_header.COMMAND= START_UPDATE_OTHERSIDE_ACKNOWLEDGE;
		bSendBodytoOtherSide=false;
	}
	else if(bSendResettoOtherSide)
	{
		IRT_UART2_header.COMMAND= RESET_IRT;
		bSendBodytoOtherSide=false;
	}
	else if (bSendBodytoOtherSide)
	{
		IRT_UART2_header.COMMAND= SEND_BODY;
	}
	else
	{				
		IRT_UART2_header.COMMAND= GET_BODY;
	}
					
	IRT_UART2_header.RESPONSE= OK_STRUCT;
	IRT_UART2_header.VERSION=   0x02;
	IRT_UART2_header.SIZEBYTES= 8;
	IRT_UART2_header.CRC_CHECK = bReceivedCrcIsCorrectBYTES(&IRT_UART2_header, sizeof(HEADER)-2);
			
	memcpy(cCommandBuffer,&IRT_UART2_header, sizeof(HEADER));
			
	uint8_t ucIndex;
	for (ucIndex = 0; ucIndex < sizeof(HEADER); ucIndex++)
	{
		usart_putchar(USART_2,cCommandBuffer[ucIndex]);
	}
	_delay_ms(1);
	
	if(bSendResettoOtherSide)
	{
		_delay_ms(10);//Wait for all messages to be transmitted
		reset_MCU();
	}
}

void vSendDiagnosticToOtherSide(void) 
{
	if(!bCommandUpdate_Otherside)
	{
		setData();
		
		//Send Header
		sendHeaderToOtherSide();
		
		if(bSendBodytoOtherSide)
		{
			//bSendBodytoOtherSide=false;

			if(bIsBody)
			{
				memcpy(cCommandBuffer,&IRT_body.BODY, sizeof(IRT_body.BODY) * sizeof(uint8_t));
			}
			else
			{
				memcpy(cCommandBuffer,&IRT_body.HOLLOW, sizeof(IRT_body.HOLLOW) * sizeof(uint8_t));
			}
			
			crcHelp =uiCalculateCrc(&cCommandBuffer, sizeof(IRT_body.BODY) * sizeof(uint8_t));
			cCommandBuffer[sizeof(IRT_body.BODY)]=(uint8_t)(crcHelp>>8);
			cCommandBuffer[sizeof(IRT_body.BODY)+1]=(uint8_t)crcHelp;
			ucBytesInCommandBuffer=sizeof(IRT_body.BODY)+2;
			
			uint8_t ucIndex;
			for (ucIndex = 0; ucIndex < ucBytesInCommandBuffer; ucIndex++)
			{
				usart_putchar(USART_2,cCommandBuffer[ucIndex]);
			}
			_delay_ms(1);
			bIsSending = false;
			
			if(bSendConfigtoOtherSide)
			{
				bSendConfigtoOtherSide=false;
				
				if(bIsBody)
				{
					IRT_body.BODY.NEW_CONFIGURATION=0;
					IRT_body.BODY.PHY0_DUPPLEX = PC_body.BODY.PHY0_DUPPLEX;
					IRT_body.BODY.PHY1_DUPPLEX = PC_body.BODY.PHY1_DUPPLEX;
					IRT_body.BODY.LLF_PHY0 = PC_body.BODY.LLF_PHY0;
					IRT_body.BODY.LLF_PHY1 = PC_body.BODY.LLF_PHY1;
				}
				else
				{
					IRT_body.HOLLOW.NEW_CONFIGURATION=0;
					IRT_body.HOLLOW.PHY0_DUPPLEX = PC_body.HOLLOW.PHY0_DUPPLEX;
					IRT_body.HOLLOW.PHY1_DUPPLEX = PC_body.HOLLOW.PHY1_DUPPLEX;
					IRT_body.HOLLOW.LLF_PHY0 = PC_body.HOLLOW.LLF_PHY0;
					IRT_body.HOLLOW.LLF_PHY1 = PC_body.HOLLOW.LLF_PHY1;
				}
			}
		}
		
		if(error_counter>0)
		{
			error_counter=0;
			ucOtherSideReceivedByteIndex=0;
			uart2_receive_type=RECEIVE_HEADER;
		}
	}
}
void reset_MCU(void)
{
	CCP = CCP_IOREG_gc;
	RST.CTRL =  RST_SWRST_bm;
}
void setData(void)
{
	if(is_FPGA_Board==0)
		IRT_body.irt_status=TLK;
	else
		IRT_body.irt_status=FPGA;
			


	if(bIsBody)
	{
		IRT_body.irt_type = BODY;
		memcpy(&IRT_body.BODY,&irt_data, 24);//24 bytes is the measurement data only
		
		IRT_body.BODY.PHY0_DUPPLEX = PC_body.BODY.PHY0_DUPPLEX;
		IRT_body.BODY.PHY1_DUPPLEX = PC_body.BODY.PHY1_DUPPLEX;
		IRT_body.BODY.LLF_PHY0 = PC_body.BODY.LLF_PHY0;
		IRT_body.BODY.LLF_PHY1 = PC_body.BODY.LLF_PHY1;
		IRT_body.BODY.SW_VERSION_1 = SW_VERSION[0];
		IRT_body.BODY.SW_VERSION_2 = SW_VERSION[1];
		IRT_body.BODY.SW_VERSION_3 = SW_VERSION[2];
		IRT_body.BODY.SW_VERSION_4 = SW_VERSION[3];
		IRT_body.BODY.SW_VERSION_5 = SW_VERSION[4];
		IRT_body.BODY.SW_VERSION_6 = SW_VERSION[5];
		IRT_body.BODY.SW_VERSION_7 = SW_VERSION[6];
		IRT_body.BODY.SW_VERSION_8 = SW_VERSION[7];
			
		if(IRT_body.BODY.NEW_CONFIGURATION==1)//SEND to the other side
		{
			IRT_body.BODY.PHY0_DUPPLEX = PC_body.HOLLOW.PHY0_DUPPLEX;
			IRT_body.BODY.PHY1_DUPPLEX = PC_body.HOLLOW.PHY1_DUPPLEX;
			IRT_body.BODY.LLF_PHY0 = PC_body.HOLLOW.LLF_PHY0;
			IRT_body.BODY.LLF_PHY1 = PC_body.HOLLOW.LLF_PHY1;
			bSendConfigtoOtherSide=true;
		}
	}
	else
	{
		IRT_body.irt_type = HOLLOW;
		memcpy(&IRT_body.HOLLOW,&irt_data, 24);//24 bytes is the measurement data only

		IRT_body.HOLLOW.PHY0_DUPPLEX = PC_body.HOLLOW.PHY0_DUPPLEX;
		IRT_body.HOLLOW.PHY1_DUPPLEX = PC_body.HOLLOW.PHY1_DUPPLEX;
		IRT_body.HOLLOW.LLF_PHY0 = PC_body.HOLLOW.LLF_PHY0;
		IRT_body.HOLLOW.LLF_PHY1 = PC_body.HOLLOW.LLF_PHY1;
		IRT_body.HOLLOW.SW_VERSION_1 = SW_VERSION[0];
		IRT_body.HOLLOW.SW_VERSION_2 = SW_VERSION[1];
		IRT_body.HOLLOW.SW_VERSION_3 = SW_VERSION[2];
		IRT_body.HOLLOW.SW_VERSION_4 = SW_VERSION[3];
		IRT_body.HOLLOW.SW_VERSION_5 = SW_VERSION[4];
		IRT_body.HOLLOW.SW_VERSION_6 = SW_VERSION[5];
		IRT_body.HOLLOW.SW_VERSION_7 = SW_VERSION[6];
		IRT_body.HOLLOW.SW_VERSION_8 = SW_VERSION[7];
		
		if(IRT_body.HOLLOW.NEW_CONFIGURATION==1)//SEND to the other side
		{
			IRT_body.HOLLOW.PHY0_DUPPLEX = PC_body.BODY.PHY0_DUPPLEX;
			IRT_body.HOLLOW.PHY1_DUPPLEX = PC_body.BODY.PHY1_DUPPLEX;
			IRT_body.HOLLOW.LLF_PHY0 = PC_body.BODY.LLF_PHY0;
			IRT_body.HOLLOW.LLF_PHY1 = PC_body.BODY.LLF_PHY1;
			bSendConfigtoOtherSide=true;
		}
	}
}

ISR(USART_2_RX_ISR_HANDLER)
{
    if (usart_rx_is_complete(USART_2)) 
	{
        uint32_t ulReceivedByte;
        ulReceivedByte = usart_getchar(USART_2);
		
		if(!bCommandUpdate_Otherside)
		{
			aucRcvBuffer[ucOtherSideReceivedByteIndex] = ulReceivedByte;
			ucOtherSideReceivedByteIndex++;
			
			if(ucOtherSideReceivedByteIndex  >= sizeof(HEADER) && uart2_receive_type==RECEIVE_HEADER)
			{
				memcpy(&IRT_UART2_header,aucRcvBuffer, sizeof(HEADER) * sizeof(uint8_t));
				
				if( bReceivedCrcIsCorrectBYTES(&aucRcvBuffer, (sizeof(HEADER)* sizeof(uint8_t))-2) == IRT_UART2_header.CRC_CHECK)
				{
					if(IRT_UART2_header.COMMAND == GET_BODY)
					{
						bSendBodytoOtherSide=true;//Send Body
						bOtherSideInBootloader=false;
					}
					else if(IRT_UART2_header.COMMAND == SEND_BODY)
					{
						uart2_receive_type=RECEIVE_BODY;
						bSendBodytoOtherSide=true;
						bOtherSideInBootloader=false;
					}
					else if(IRT_UART2_header.COMMAND == IN_BOOTLOADER)
					{
						bOtherSideInBootloader=true;//Set bootloader bit to PC
						bSendBodytoOtherSide=false;
						
						if(bIsBody)
						{
							memset(&IRT_body.HOLLOW,0, sizeof(IRT_body.HOLLOW) * sizeof(uint8_t));
						}
						else
						{
							memset(&IRT_body.BODY,0, sizeof(IRT_body.BODY) * sizeof(uint8_t));
						}
					}
					else if(IRT_UART2_header.COMMAND == START_UPDATE)
					{
						PC_header.COMMAND = START_UPDATE;
						bCommandHeader=true;
						bCommandReceive_Bootloader_Otherside=true;
						bCommandSend_Bootloader_Otherside=false;
						bSendBodytoOtherSide=false;
					}
					else if(IRT_UART2_header.COMMAND == START_UPDATE_OTHERSIDE_ACKNOWLEDGE)
					{												
						bCommandUpdate_Otherside=true;
						PC_header.COMMAND = START_UPDATE_OTHERSIDE_ACKNOWLEDGE;
						IRT_body.HOLLOW.UPDATE_OTHERSIDE=0;
						IRT_body.BODY.UPDATE_OTHERSIDE=0;
						bCommandSend_Bootloader_Otherside=false;
						bSendBodytoOtherSide=false;
						bCommandHeader=true;
					}
					else if(IRT_UART2_header.COMMAND == RESET_IRT)
					{
						reset_MCU();//RESET COMMAND FROM OTHERSIDE
					}
				}
				else
					error_counter++;
					
				memset(aucRcvBuffer, 0, ucOtherSideReceivedByteIndex);
				ucOtherSideReceivedByteIndex=0;
			}
			else if(ucOtherSideReceivedByteIndex >= (sizeof(IRT_DATA)+2) && uart2_receive_type==RECEIVE_BODY)
			{
				uart2_receive_type = RECEIVE_HEADER;
				
				uint16_t crcHelp =uiCalculateCrc(&aucRcvBuffer, (ucOtherSideReceivedByteIndex * sizeof(uint8_t))-2);
				uint16_t crcRecevied = (uint16_t)(aucRcvBuffer[ucOtherSideReceivedByteIndex-2]<<8) | aucRcvBuffer[ucOtherSideReceivedByteIndex-1];
				
				if(crcHelp == crcRecevied)
				{					
					if(bIsBody)
					{
						memcpy(&IRT_body.HOLLOW,aucRcvBuffer, sizeof(IRT_body.HOLLOW) * sizeof(uint8_t));
					}
					else
					{
						memcpy(&IRT_body.BODY,aucRcvBuffer, sizeof(IRT_body.BODY) * sizeof(uint8_t));
					}			
					
					if((bIsBody && IRT_body.HOLLOW.SW_RESET==1) || (!bIsBody && IRT_body.BODY.SW_RESET==1))
						reset_MCU();//RESET COMMAND FROM OTHERSIDE
					
					if((bIsBody && IRT_body.HOLLOW.NEW_CONFIGURATION==1) || (!bIsBody && IRT_body.BODY.NEW_CONFIGURATION==1))
					{
						if(bIsBody)
						{
							PC_body.BODY.LLF_PHY0=IRT_body.HOLLOW.LLF_PHY0;
							PC_body.BODY.LLF_PHY1=IRT_body.HOLLOW.LLF_PHY1;
							PC_body.BODY.PHY0_DUPPLEX=IRT_body.HOLLOW.PHY0_DUPPLEX;
							PC_body.BODY.PHY1_DUPPLEX=IRT_body.HOLLOW.PHY1_DUPPLEX;
						}
						else
						{
							PC_body.HOLLOW.LLF_PHY0=IRT_body.BODY.LLF_PHY0;
							PC_body.HOLLOW.LLF_PHY1=IRT_body.BODY.LLF_PHY1;
							PC_body.HOLLOW.PHY0_DUPPLEX=IRT_body.BODY.PHY0_DUPPLEX;
							PC_body.HOLLOW.PHY1_DUPPLEX=IRT_body.BODY.PHY1_DUPPLEX;
						}
						
						PC_header.COMMAND = SET_CONFIG;
						PC_body.CRC_Checksum =bReceivedCrcIsCorrectBYTES(&PC_body, ((sizeof(PC_body)*sizeof(uint8_t))-2));
						bCommandBody=true;
					}
				}
				else
				{
					error_counter++;
				}
				
				memset(aucRcvBuffer, 0, ucOtherSideReceivedByteIndex);
				ucOtherSideReceivedByteIndex=0;
			}
		}
		
		if(bCommandUpdate_Otherside)
		{		
			usart_putchar(USART_1,ulReceivedByte);
		}
    }
}