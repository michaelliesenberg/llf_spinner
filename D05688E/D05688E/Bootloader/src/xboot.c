/************************************************************************/
/* XBoot Extensible AVR Bootloader                                      */
/************************************************************************/

#include "xboot.h"

#ifdef USE_INTERRUPTS


volatile unsigned char rx_buff0;
volatile unsigned char rx_buff1;
volatile unsigned char rx_char_cnt;

volatile unsigned char tx_buff0;
volatile unsigned char tx_char_cnt;
#endif // USE_INTERRUPTS

unsigned char buffer[SPM_PAGESIZE];
unsigned char receive_header_buffer[8];
unsigned char receive_count=0;
#ifdef NEED_CODE_PROTECTION
unsigned char protected;
#endif // NEED_CODE_PROTECTION
unsigned char in_bootloader = 0;

// Main code
int main(void)
{
        ADDR_T address = 0;
        
        unsigned char val = 0;
        int i = 0;
        uint32_t j;
        uint8_t k;
		uint8_t passcode_counter=0;
		
        
        #ifdef NEED_CODE_PROTECTION
        protected = 1;
        #endif // NEED_CODE_PROTECTION
        
        
        #ifdef USE_INTERRUPTS
        rx_char_cnt = 0;
        tx_char_cnt = 0;
        #endif // USE_INTERRUPTS
        
        // Initialization section
        // Entry point and communication methods are initialized here
        // --------------------------------------------------
    

        OSC.CTRL |= OSC_RC32MEN_bm; // turn on 32 MHz oscillator
        while (!(OSC.STATUS & OSC_RC32MRDY_bm)) { }; // wait for it to start
        CCP = CCP_IOREG_gc;
        CLK.CTRL = CLK_SCLKSEL_RC32M_gc;


        // interrupts
        #ifdef NEED_INTERRUPTS
        // remap interrupts to boot section
        CCP = CCP_IOREG_gc;
        #ifdef USE_INTERRUPTS
        PMIC.CTRL = PMIC_IVSEL_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm;
        #else
        PMIC.CTRL = PMIC_IVSEL_bm;
        #endif // USE_INTERRUPTS
        #endif // NEED_INTERRUPTS
        
		if (EEPROM_read_byte(NVM_BootLoader_UART)==1)
			UART_OtherSide=1;
		else
			UART_OtherSide=0;
					
		if (EEPROM_read_byte(NVM_BootLoader_BOARD_Type)==1)
			is_FPGA_Board=1;
		else
			is_FPGA_Board=0;
        
        // Initialize UART
        uart_init();
        
        #ifdef USE_WATCHDOG
        WDT_EnableAndSetTimeout();
        #endif // USE_WATCHDOG
		
        k = ENTER_BLINK_COUNT;
        j = ENTER_BLINK_WAIT;
		
		
        while (!in_bootloader && k > 0)
        {
            if (j-- <= 0)
            {
                PORTR.OUTTGL = (1 << LED_PIN);
                j = ENTER_BLINK_WAIT;
                k--;
            }

            // Main trigger section
            // Set in_bootloader here to enter the bootloader
            // Checked when USE_ENTER_DELAY is selected
			
            // --------------------------------------------------
			if (uart_char_received(UART_OtherSide))
			{
				if(uart_cur_char(UART_OtherSide) == CMD_SYNC)
				{
					passcode_counter++;
				
					if(passcode_counter>=5)
						in_bootloader = 1;
				}
			}

            #ifdef USE_WATCHDOG
            WDT_Reset();
            #endif // USE_WATCHDOG
        }
		
		if (EEPROM_read_byte(NVM_BootLoader_ENTRY_CODE)!=0xFF)
			in_bootloader = 1; //Entry bootloader, application is not correct
			
        #ifdef USE_INTERRUPTS
        // Enable interrupts
        sei();
        #endif // USE_INTERRUPTS
        

        
        // Main bootloader        
        while (in_bootloader) 
		{                
                val = get_char();
                
                #ifdef USE_WATCHDOG
                WDT_Reset();
                #endif // USE_WATCHDOG
                
                // Main bootloader parser
                // check autoincrement status
                if (val == CMD_CHECK_AUTOINCREMENT)
                {
                        // yes, it is supported
                        send_char(REPLY_YES);
                }
                // Set address
                else if (val == CMD_SET_ADDRESS)
                {
                        // Read address high then low
                        address = get_2bytes();
                        // acknowledge
                        send_char(REPLY_ACK);
                }
                // Extended address
                else if (val == CMD_SET_EXT_ADDRESS)
                {
                        // Read address high then low
                        //address = ((ADDR_T)get_char() << 16) | get_2bytes();
                        asm volatile (
                                "call get_char"    "\n\t"
                                "mov  %C0,r24"     "\n\t"
                                "call get_2bytes"  "\n\t"
                                "clr  %D0"         "\n\t"
                                : "=r" (address)
                                :
                        );
                        
                        // acknowledge
                        send_char(REPLY_ACK);
                }
                // Chip erase
                else if (val == CMD_CHIP_ERASE)
                {
                    for(uint32_t addr = APP_SECTION_START; addr < APP_SECTION_END; addr += SPM_PAGESIZE)
                    {
	                        Flash_EraseWriteApplicationPage(addr);
	                        // Wait for completion
	                        #ifdef USE_WATCHDOG
	                        while (NVM_STATUS & NVM_NVMBUSY_bp)
	                        {
	                            // reset watchdog while waiting for erase completion
	                            WDT_Reset();
	                        }
	                        #else // USE_WATCHDOG
	                        SP_WaitForSPM();
	                        #endif // USE_WATCHDOG
                    }
                         
                    // Wait for completion
                    #ifdef USE_WATCHDOG
                    while (NVM_STATUS & NVM_NVMBUSY_bp)
                    {
	                        // reset watchdog while waiting for erase completion
	                        WDT_Reset();
                    }
                    #else // USE_WATCHDOG
                    SP_WaitForSPM();
                    #endif // USE_WATCHDOG
                        
                    // Erase EEPROM
                    EEPROM_erase_all(); //TODO FIXME

                    // turn off read protection
                    #ifdef NEED_CODE_PROTECTION
                    protected = 0;
                    #endif // NEED_CODE_PROTECTION
                    EEPROM_write_byte(NVM_BootLoader_ENTRY_CODE,0x00);//No application code in flash
                    // acknowledge
                    send_char(REPLY_ACK);
                }
                #ifdef ENABLE_BLOCK_SUPPORT
                // Check block load support
                else if (val == CMD_CHECK_BLOCK_SUPPORT )
                {
                        // yes, it is supported
                        send_char(REPLY_YES);
                        // Send block size (page size)
                        send_char((SPM_PAGESIZE >> 8) & 0xFF);
                        send_char(SPM_PAGESIZE & 0xFF);
                }
                // Block load
                else if (val == CMD_BLOCK_LOAD)
                {
                        // Block size
                        i = get_2bytes();
                        // Memory type
                        val = get_char();
                        // Load it
                        send_char(BlockLoad(i, val, &address));
                }
                // Block read
                else if (val == CMD_BLOCK_READ)
                {
                        // Block size
                        i = get_2bytes();
                        // Memory type
                        val = get_char();
                        // Read it
                        BlockRead(i, val, &address);
                }
                #endif // ENABLE_BLOCK_SUPPORT
                #ifdef ENABLE_EEPROM_BYTE_SUPPORT
                // Write EEPROM memory
                else if (val == CMD_WRITE_EEPROM_BYTE)
                {
                        EEPROM_write_byte(address, get_char());
                        address++;
                        send_char(REPLY_ACK);
                }
                // Read EEPROM memory
                else if (val == CMD_READ_EEPROM_BYTE)
                {
                        char c = EEPROM_read_byte(address);
                        
                        #ifdef ENABLE_EEPROM_PROTECTION
                        if (protected)
                                c = 0xff;
                        #endif // ENABLE_EEPROM_PROTECTION
                        
                        send_char(c);
                        address++;
                }
                #endif // ENABLE_EEPROM_BYTE_SUPPORT 
               /* #ifdef ENABLE_LOCK_BITS

                // Write lockbits
                else if (val == CMD_WRITE_LOCK_BITS)
                {
                        SP_WriteLockBits( get_char() );
                        send_char(REPLY_ACK);
                }
                // Read lockbits
                else if (val == CMD_READ_LOCK_BITS)
                {
                        send_char(SP_ReadLockBits());
                }
				
                #endif // ENABLE_LOCK_BITS
                #ifdef ENABLE_FUSE_BITS

                // Read low fuse bits
                else if (val == CMD_READ_LOW_FUSE_BITS)
                {
                        send_char(SP_ReadFuseByte(0));
                }
                // Read high fuse bits
                else if (val == CMD_READ_HIGH_FUSE_BITS)
                {
                        send_char(SP_ReadFuseByte(1));
                }
                // Read extended fuse bits
                else if (val == CMD_READ_EXT_FUSE_BITS)
                {
                        send_char(SP_ReadFuseByte(2));
                }
                #endif // ENABLE_FUSE_BITS */
                // Enter and leave programming mode
                else if ((val == CMD_ENTER_PROG_MODE) || (val == CMD_LEAVE_PROG_MODE))
                {
                        // just acknowledge
                        send_char(REPLY_ACK);
                }
                // Exit bootloader
                else if (val == CMD_EXIT_BOOTLOADER)
                {
                        send_char(REPLY_ACK);
                        send_char(REPLY_ACK);
						send_char(REPLY_ACK);
						EEPROM_write_byte(NVM_BootLoader_ENTRY_CODE,0xFF);//Write default value
						in_bootloader = 0;
                }
                // Get programmer type
                else if (val == CMD_PROGRAMMER_TYPE)
                {
                        // serial
                        send_char('S');
                }
                // Return supported device codes
                else if (val == CMD_DEVICE_CODE)
                {
                        // send only this device
                        send_char(123); // TODO
                        // terminator
                        send_char(0);
                }
                // Return program identifier
                else if (val == CMD_PROGRAM_ID)
                {
                        send_char('X');
                        send_char('B');
                        send_char('o');
                        send_char('o');
                        send_char('t');
                        send_char('+');
                        send_char('+');
                }
                // Read software version
                else if (val == CMD_VERSION)
                {
                        send_char('0' + XBOOT_VERSION_MAJOR);
                        send_char('0' + XBOOT_VERSION_MINOR);
                }
                // Read signature bytes
                else if (val == CMD_READ_SIGNATURE)
                {
                        send_char(SIGNATURE_2);
                        send_char(SIGNATURE_1);
                        send_char(SIGNATURE_0);
                }
              /*  #ifdef ENABLE_CRC_SUPPORT
                else if (val == CMD_CRC)
                {
                        uint32_t start = 0;
                        uint32_t length = 0;
                        uint16_t crc;
                        
                        val = get_char();
                        
                        switch (val)
                        {
                                case SECTION_FLASH:
                                        length = PROGMEM_SIZE;
                                        break;
                                case SECTION_APPLICATION:
                                        length = APP_SECTION_SIZE;
                                        break;
                                case SECTION_BOOT:
                                        start = BOOT_SECTION_START;
                                        length = BOOT_SECTION_SIZE;
                                        break;
                                #ifdef ENABLE_API
                                case SECTION_APP:
                                        length = XB_APP_SIZE;
                                        break;
                                case SECTION_APP_TEMP:
                                        start = XB_APP_TEMP_START;
                                        length = XB_APP_TEMP_SIZE;
                                        break;
                                #endif // ENABLE_API
                                default:
                                        send_char(REPLY_ERROR);
                                        continue;
                        }
                        
                        crc = crc16_block(start, length);
                        
                        send_char((crc >> 8) & 0xff);
                        send_char(crc & 0xff);
                }
                #endif // ENABLE_CRC_SUPPORT */
               
				else if (val == CMD_SYNC)
				{
					send_char(REPLY_ACK);
				}
                else 
                {
					receive_header_buffer[receive_count]=val;
					receive_count++;
					
					if(receive_count>=8)
					{
						receive_count--;
						
						if((receive_header_buffer[0] == 0x42	&&
							receive_header_buffer[6] == 0xff	&&
							receive_header_buffer[7] == 0x10)	|| 
						   (receive_header_buffer[0] == 0x42	&&
							receive_header_buffer[6] == 0x78	&&
							receive_header_buffer[7] == 0x12))
						{
							send_char('X');
							send_char('O');
							send_char(0);
							send_char(2);
							send_char(0);
							send_char(8);
							send_char(0xE8);
							send_char(0x3F);
							receive_count=0;
						}
						else
						{
							for(char i=0; i<7;i++)
								receive_header_buffer[i]=receive_header_buffer[i+1];
						}
					}
                }
                // Wait for any lingering SPM instructions to finish
                Flash_WaitForSPM();
                
                // End of bootloader main loop
        }
        
        #ifdef NEED_INTERRUPTS
        // Disable interrupts
        cli();
        #endif // NEED_INTERRUPTS
        
        // Bootloader exit section
        // Code here runs after the bootloader has exited,
        // but before the application code has started
        // --------------------------------------------------
        
        #ifdef ENABLE_API
        #ifdef ENABLE_API_FIRMWARE_UPDATE
        // Update firmware if needed
        install_firmware();
        #endif // ENABLE_API_FIRMWARE_UPDATE
        #endif // ENABLE_API
        

        // Shut down UART
        uart_deinit();
        
        UART_RX_PIN_CTRL = 0;

        
        #ifdef LOCK_SPM_ON_EXIT
        // Lock SPM writes
        SP_LockSPM();
		#endif
        
        LED_PORT.DIRCLR = (1 << LED_PIN);
        LED_PORT.OUTCLR = (1 << LED_PIN);
        

        #ifdef NEED_INTERRUPTS
        // remap interrupts back to application section
        CCP = CCP_IOREG_gc;
        PMIC.CTRL = 0;
        #endif // NEED_INTERRUPTS
        
        #ifdef USE_WATCHDOG
       // WDT_Disable();//TODO Test here
        #endif // USE_WATCHDOG
        
        // --------------------------------------------------
        // End bootloader exit section
        
        // Jump into main code
        asm("jmp 0");
        
        #ifdef __builtin_unreachable
        // Size optimization as the asm jmp will not return
        // However, it seems it is not available on older versions of gcc
        __builtin_unreachable();
        #endif
}


#ifdef USE_INTERRUPTS
unsigned char __attribute__ ((noinline)) get_char(void)
{
        unsigned char ret;
        
        while (rx_char_cnt == 0) { };
        
        cli();
        
        ret = rx_buff0;
        rx_buff0 = rx_buff1;
        rx_char_cnt--;
        
        sei();
        
        return ret;
}

void __attribute__ ((noinline)) send_char(unsigned char c)
{
        while (1)
        {
                cli();
                
                if (tx_char_cnt == 0)
                {
                        tx_buff0 = c;
                        tx_char_cnt = 1;
                        
                        uart_send_char(c,UART_OtherSide);
                        
                        sei();
                        return;
                }
                
                sei();
        }
}

#else

unsigned char __attribute__ ((noinline)) get_char(void)
{
        unsigned char ret;
        
        while (1)
        {
            // Get next character
            if (uart_char_received(UART_OtherSide))
            {
	            return uart_cur_char(UART_OtherSide);
            }
			#ifdef USE_WATCHDOG
            WDT_Reset();
            #endif // USE_WATCHDOG
        }
        
        return ret;
}

void __attribute__ ((noinline)) send_char(unsigned char c)
{
    uart_send_char_blocking(c,UART_OtherSide);
}

#endif // USE_INTERRUPTS

unsigned int __attribute__ ((noinline)) get_2bytes()
{
        // return (get_char() << 8) | get_char();
        unsigned int result;
        asm volatile (
                "call get_char"    "\n\t"
                "push r24"         "\n\t"
                "call get_char"    "\n\t"
                "pop  %B0"         "\n\t"
                "mov  %A0,r24"     "\n\t"
                : "=r" (result)
                :
        );
        return result;
}

void clear_buffer(void)
{
        unsigned char *ptr = buffer;
        for (long i = 0; i < SPM_PAGESIZE; i++)
        {
                *(ptr++) = 0xff;
        }
}

unsigned char BlockLoad(unsigned int size, unsigned char mem, ADDR_T *address)
{
        ADDR_T tempaddress;
		
        #ifdef USE_WATCHDOG
        WDT_Reset();
        #endif // USE_WATCHDOG
        
        // fill up buffer
        for (int i = 0; i < SPM_PAGESIZE; i++)
        {
                char c = 0xff;
                
                if (i < size)
					c = get_char();
                
                buffer[i] = c;
        }
        
        // EEPROM memory type.
        if(mem == MEM_EEPROM)
        {
                EEPROM_write_block(*address, buffer, size);
                (*address) += size;
                
                return REPLY_ACK; // Report programming OK
        } 
        
        // Flash memory type
        else if (mem == MEM_FLASH)
        {
                // NOTE: For flash programming, 'address' is given in words.
                tempaddress = (*address) << 1;  // Store address in page.
                
                (*address) += size >> 1;
                
                if (mem == MEM_FLASH)
                {
                        #ifdef ENABLE_FLASH_ERASE_WRITE
                        Flash_ProgramPage(tempaddress, buffer, 1);
                        #else
                        Flash_ProgramPage(tempaddress, buffer, 0);
                        #endif
                }
      
                return REPLY_ACK; // Report programming OK
        }
        
        // Invalid memory type?
        else
        {
                return REPLY_ERROR;
        }
}



void BlockRead(unsigned int size, unsigned char mem, ADDR_T *address)
{
        int offset = 0;
        int size2 = size;
        
        // EEPROM memory type.
        
        if (mem == MEM_EEPROM) // Read EEPROM
        {
                EEPROM_read_block(*address, buffer, size);
                (*address) += size;
        }
        
        // Flash memory type.
        else if (mem == MEM_FLASH  || mem == MEM_PRODSIG)
        {
                (*address) <<= 1; // Convert address to bytes temporarily.
                
                do
                {
                        if (mem == MEM_FLASH)
                        {
                                buffer[offset++] = Flash_ReadByte(*address);
                        }
                        else if (mem == MEM_PRODSIG)
                        {
                                buffer[offset++] = SP_ReadCalibrationByte(*address);
                        }

                        
                        Flash_WaitForSPM();
                        
                        (*address)++;    // Select next word in memory.
                        size--;          // Subtract two bytes from number of bytes to read
                } while (size);         // Repeat until all block has been read
                
                (*address) >>= 1;       // Convert address back to Flash words again.
        }
        else
        {
                // bad memory type
                return;
        }
        
        // code protection
       /* if (
        #ifdef ENABLE_CODE_PROTECTION
                (protected && mem == MEM_FLASH) ||
        #endif // ENABLE_CODE_PROTECTION
        #ifdef ENABLE_EEPROM_PROTECTION
                (protected && mem == MEM_EEPROM) ||
        #endif // ENABLE_EEPROM_PROTECTION
        #ifdef ENABLE_BOOTLOADER_PROTECTION
                (*address >= (BOOT_SECTION_START >> 1) && mem == MEM_FLASH) ||
        #endif // ENABLE_BOOTLOADER_PROTECTION
                0
        )
                clear_buffer();*/
        
        // send bytes
        for (int i = 0; i < size2; i++)
        {
			send_char(buffer[i]);
        }
        
}

uint16_t crc16_block(uint32_t start, uint32_t length)
{
        uint16_t crc = 0;
        
        int bc = SPM_PAGESIZE;
        
        for ( ; length > 0; length--)
        {
                if (bc == SPM_PAGESIZE)
                {
                        Flash_ReadFlashPage(buffer, start);
                        start += SPM_PAGESIZE;
                        bc = 0;
                }
                
                crc = _crc16_update(crc, buffer[bc]);
                
                bc++;
        }
        
        return crc;
}

void install_firmware()
{
        uint16_t crc;
        uint16_t crc2;
        
        // read last block
        Flash_ReadFlashPage(buffer, XB_APP_TEMP_START + XB_APP_TEMP_SIZE - SPM_PAGESIZE);
        
        // check for install command
        if (buffer[SPM_PAGESIZE-6] == 'X' && buffer[SPM_PAGESIZE-5] == 'B' &&
                buffer[SPM_PAGESIZE-4] == 'I' && buffer[SPM_PAGESIZE-3] == 'F')
        {
                crc = (buffer[SPM_PAGESIZE-2] << 8) | buffer[SPM_PAGESIZE-1];
                
                // skip last 6 bytes as they are the install command
                crc2 = crc16_block(XB_APP_TEMP_START, XB_APP_TEMP_SIZE - 6);
                
                // crc last 6 bytes as empty
                for (int i = 0; i < 6; i++)
                        crc2 = _crc16_update(crc2, 0xff);
                
                if (crc == crc2)
                {
                        for (uint32_t ptr = 0; ptr < XB_APP_SIZE; ptr += SPM_PAGESIZE)
                        {
                                LED_PORT.OUTTGL = (1 << LED_PIN);
                                
                                Flash_ReadFlashPage(buffer, ptr + XB_APP_TEMP_START);
                                // if it's the last page, clear out the last 6 bytes
                                if (ptr >= XB_APP_SIZE - SPM_PAGESIZE)
                                {
                                        for (int i = SPM_PAGESIZE-6; i < SPM_PAGESIZE; i++)
                                                buffer[i] = 0xff;
                                }
                                Flash_ProgramPage(ptr, buffer, 1);
                        }
                }
                
                xboot_app_temp_erase();
        }
}



