   1               		.file	"fifo.c"
   2               	__SP_H__ = 0x3e
   3               	__SP_L__ = 0x3d
   4               	__SREG__ = 0x3f
   5               	__CCP__ = 0x34
   6               	__tmp_reg__ = 0
   7               	__zero_reg__ = 1
   9               		.text
  10               	.Ltext0:
 530               		.section	.text.fifo_init,"ax",@progbits
 532               	.global	fifo_init
 534               	fifo_init:
   1:fifo.c        **** /************************************************************************/
   2:fifo.c        **** /* XBoot Extensible AVR Bootloader                                      */
   3:fifo.c        **** /*                                                                      */
   4:fifo.c        **** /* FT245/2232 asynchronous Fifo Module                                  */
   5:fifo.c        **** /*                                                                      */
   6:fifo.c        **** /* fifo.c                                                               */
   7:fifo.c        **** /*                                                                      */
   8:fifo.c        **** /* Uwe Bonnes bon@elektron.ikp.physik.tu-darmstadt.de                   */
   9:fifo.c        **** /*                                                                      */
  10:fifo.c        **** /* Copyright (c) 2011 Uwe Bonnes                                        */
  11:fifo.c        **** /*                                                                      */
  12:fifo.c        **** /* Permission is hereby granted, free of charge, to any person          */
  13:fifo.c        **** /* obtaining a copy of this software and associated documentation       */
  14:fifo.c        **** /* files(the "Software"), to deal in the Software without restriction,  */
  15:fifo.c        **** /* including without limitation the rights to use, copy, modify, merge, */
  16:fifo.c        **** /* publish, distribute, sublicense, and/or sell copies of the Software, */
  17:fifo.c        **** /* and to permit persons to whom the Software is furnished to do so,    */
  18:fifo.c        **** /* subject to the following conditions:                                 */
  19:fifo.c        **** /*                                                                      */
  20:fifo.c        **** /* The above copyright notice and this permission notice shall be       */
  21:fifo.c        **** /* included in all copies or substantial portions of the Software.      */
  22:fifo.c        **** /*                                                                      */
  23:fifo.c        **** /* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
  24:fifo.c        **** /* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
  25:fifo.c        **** /* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
  26:fifo.c        **** /* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
  27:fifo.c        **** /* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
  28:fifo.c        **** /* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
  29:fifo.c        **** /* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
  30:fifo.c        **** /* SOFTWARE.                                                            */
  31:fifo.c        **** /*                                                                      */
  32:fifo.c        **** /************************************************************************/
  33:fifo.c        **** 
  34:fifo.c        **** #include "fifo.h"
  35:fifo.c        **** /* As discussed in
  36:fifo.c        ****  * http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&t=41613
  37:fifo.c        ****  * Accessing a bitrev table in bootloader flash will not be faster, as 
  38:fifo.c        ****  * a our character needs to be added to the table converted to a far address
  39:fifo.c        ****  * and probaly also the NVM needs to be cared about
  40:fifo.c        ****  */ 
  41:fifo.c        **** 
  42:fifo.c        **** #define REVERSE(a) do                     \
  43:fifo.c        **** {                                         \
  44:fifo.c        ****   a=((a>>1)&0x55)|((a<<1)&0xaa);          \
  45:fifo.c        ****   a=((a>>2)&0x33)|((a<<2)&0xcc);          \
  46:fifo.c        ****   asm volatile("swap %0":"=r"(a):"0"(a)); \
  47:fifo.c        **** } while(0)
  48:fifo.c        **** 
  49:fifo.c        **** // Initialize FIFO
  50:fifo.c        **** void fifo_init(void)
  51:fifo.c        **** {
 536               	.LM0:
 537               	.LFBB1:
 538               	/* prologue: function */
 539               	/* frame size = 0 */
 540               	/* stack size = 0 */
 541               	.L__stack_usage = 0
  52:fifo.c        **** #ifdef __AVR_XMEGA__
  53:fifo.c        ****         FIFO_DATA_PORT.DIR = 0;
 543               	.LM1:
 544 0000 1092 4006 		sts 1600,__zero_reg__
  54:fifo.c        ****         FIFO_CTL_PORT.OUTSET = _BV(FIFO_RD_N) | _BV(FIFO_WR_N);
 546               	.LM2:
 547 0004 E0E6      		ldi r30,lo8(96)
 548 0006 F6E0      		ldi r31,lo8(6)
 549 0008 83E0      		ldi r24,lo8(3)
 550 000a 8583      		std Z+5,r24
  55:fifo.c        ****         FIFO_CTL_PORT.DIRSET = _BV(FIFO_RD_N) | _BV(FIFO_WR_N);
 552               	.LM3:
 553 000c 8183      		std Z+1,r24
 554 000e 0895      		ret
 556               	.Lscope1:
 557               		.section	.text.fifo_deinit,"ax",@progbits
 559               	.global	fifo_deinit
 561               	fifo_deinit:
  56:fifo.c        **** #else // __AVR_XMEGA__
  57:fifo.c        ****         FIFO_DATA_PORT_DDR = 0;
  58:fifo.c        ****         FIFO_DATA_PORT |= (_BV(FIFO_RD_N) | _BV(FIFO_WR_N));
  59:fifo.c        ****         FIFO_CTL_PORT_DDR |= (_BV(FIFO_RD_N) | _BV(FIFO_WR_N));
  60:fifo.c        **** #endif // __AVR_XMEGA__
  61:fifo.c        **** }
  62:fifo.c        **** 
  63:fifo.c        **** // Shut down FIFO
  64:fifo.c        **** void fifo_deinit(void)
  65:fifo.c        **** {
 563               	.LM4:
 564               	.LFBB2:
 565               	/* prologue: function */
 566               	/* frame size = 0 */
 567               	/* stack size = 0 */
 568               	.L__stack_usage = 0
  66:fifo.c        **** #ifdef __AVR_XMEGA__
  67:fifo.c        ****         FIFO_DATA_PORT.DIR = 0xff;
 570               	.LM5:
 571 0000 E0E4      		ldi r30,lo8(64)
 572 0002 F6E0      		ldi r31,lo8(6)
 573 0004 8FEF      		ldi r24,lo8(-1)
 574 0006 8083      		st Z,r24
  68:fifo.c        ****         FIFO_DATA_PORT.OUTCLR = 0xff;
 576               	.LM6:
 577 0008 8683      		std Z+6,r24
  69:fifo.c        ****         FIFO_CTL_PORT.OUTCLR = _BV(FIFO_RD_N) | _BV(FIFO_WR_N);
 579               	.LM7:
 580 000a E0E6      		ldi r30,lo8(96)
 581 000c F6E0      		ldi r31,lo8(6)
 582 000e 83E0      		ldi r24,lo8(3)
 583 0010 8683      		std Z+6,r24
  70:fifo.c        ****         FIFO_CTL_PORT.DIRCLR = _BV(FIFO_RD_N) | _BV(FIFO_WR_N);
 585               	.LM8:
 586 0012 8283      		std Z+2,r24
 587 0014 0895      		ret
 589               	.Lscope2:
 590               		.section	.text.fifo_cur_char,"ax",@progbits
 592               	.global	fifo_cur_char
 594               	fifo_cur_char:
  71:fifo.c        **** #else // __AVR_XMEGA__
  72:fifo.c        ****         FIFO_DATA_PORT_DDR = 0xff;
  73:fifo.c        ****         FIFO_DATA_PORT= 0x00;
  74:fifo.c        ****         FIFO_DATA_PORT &= ~(_BV(FIFO_RD_N) | _BV(FIFO_WR_N));
  75:fifo.c        ****         FIFO_CTL_PORT_DDR &= ~(_BV(FIFO_RD_N) | _BV(FIFO_WR_N));
  76:fifo.c        **** #endif // __AVR_XMEGA__
  77:fifo.c        **** }
  78:fifo.c        **** 
  79:fifo.c        **** uint8_t fifo_cur_char(void)
  80:fifo.c        **** {
 596               	.LM9:
 597               	.LFBB3:
 598               	/* prologue: function */
 599               	/* frame size = 0 */
 600               	/* stack size = 0 */
 601               	.L__stack_usage = 0
  81:fifo.c        ****         uint8_t ret;
  82:fifo.c        **** #ifdef __AVR_XMEGA__
  83:fifo.c        ****         FIFO_CTL_PORT.OUTCLR = _BV(FIFO_RD_N);
 603               	.LM10:
 604 0000 82E0      		ldi r24,lo8(2)
 605 0002 E0E6      		ldi r30,lo8(96)
 606 0004 F6E0      		ldi r31,lo8(6)
 607 0006 8683      		std Z+6,r24
  84:fifo.c        ****         ret = FIFO_DATA_PORT.IN;
 609               	.LM11:
 610 0008 E0E4      		ldi r30,lo8(64)
 611 000a F6E0      		ldi r31,lo8(6)
 612 000c 8085      		ldd r24,Z+8
  85:fifo.c        ****         #ifdef  FIFO_BIT_REVERSE
  86:fifo.c        ****         REVERSE(ret);
 614               	.LM12:
 615 000e 282F      		mov r18,r24
 616 0010 220F      		lsl r18
 617 0012 2A7A      		andi r18,lo8(-86)
 618 0014 8695      		lsr r24
 619 0016 8575      		andi r24,lo8(85)
 620 0018 282B      		or r18,r24
 621 001a 34E0      		ldi r19,lo8(4)
 622 001c 239F      		mul r18,r19
 623 001e C001      		movw r24,r0
 624 0020 1124      		clr __zero_reg__
 625 0022 8C7C      		andi r24,lo8(-52)
 626 0024 2695      		lsr r18
 627 0026 2695      		lsr r18
 628 0028 2373      		andi r18,lo8(51)
 629 002a 822B      		or r24,r18
 630               	/* #APP */
 631               	 ;  86 "fifo.c" 1
 632 002c 8295      		swap r24
 633               	 ;  0 "" 2
  87:fifo.c        ****         #endif
  88:fifo.c        ****         FIFO_CTL_PORT.OUTSET = _BV(FIFO_RD_N);
 635               	.LM13:
 636               	/* #NOAPP */
 637 002e 92E0      		ldi r25,lo8(2)
 638 0030 E0E6      		ldi r30,lo8(96)
 639 0032 F6E0      		ldi r31,lo8(6)
 640 0034 9583      		std Z+5,r25
  89:fifo.c        **** #else // __AVR_XMEGA__
  90:fifo.c        ****         FIFO_CTL_PORT &= ~_BV(FIFO_RD_N);
  91:fifo.c        ****         ret = FIFO_DATA_PORT_PIN;
  92:fifo.c        ****         #ifdef  FIFO_BIT_REVERSE
  93:fifo.c        ****         REVERSE(ret);
  94:fifo.c        ****         #endif
  95:fifo.c        ****         FIFO_CTL_PORT |= _BV(FIFO_RD_N);
  96:fifo.c        **** #endif // __AVR_XMEGA__
  97:fifo.c        ****         return ret;
  98:fifo.c        **** }
 642               	.LM14:
 643 0036 0895      		ret
 645               	.Lscope3:
 646               		.section	.text.fifo_send_char,"ax",@progbits
 649               	.global	fifo_send_char
 651               	fifo_send_char:
  99:fifo.c        **** 
 100:fifo.c        **** void fifo_send_char(uint8_t c)
 101:fifo.c        **** {
 653               	.LM15:
 654               	.LFBB4:
 655               	/* prologue: function */
 656               	/* frame size = 0 */
 657               	/* stack size = 0 */
 658               	.L__stack_usage = 0
 102:fifo.c        **** #ifdef __AVR_XMEGA__
 103:fifo.c        ****         if ((FIFO_CTL_PORT.IN & _BV(FIFO_TXE_N)) !=  _BV(FIFO_TXE_N))
 660               	.LM16:
 661 0000 9091 6806 		lds r25,1640
 662 0004 92FD      		sbrc r25,2
 663 0006 00C0      		rjmp .L4
 104:fifo.c        ****         {
 105:fifo.c        ****                 FIFO_DATA_PORT.DIR = 0xff;
 665               	.LM17:
 666 0008 9FEF      		ldi r25,lo8(-1)
 667 000a 9093 4006 		sts 1600,r25
 106:fifo.c        ****                 #ifdef  FIFO_BIT_REVERSE
 107:fifo.c        ****                 REVERSE(c);
 669               	.LM18:
 670 000e 282F      		mov r18,r24
 671 0010 220F      		lsl r18
 672 0012 2A7A      		andi r18,lo8(-86)
 673 0014 8695      		lsr r24
 674 0016 8575      		andi r24,lo8(85)
 675 0018 282B      		or r18,r24
 676 001a 34E0      		ldi r19,lo8(4)
 677 001c 239F      		mul r18,r19
 678 001e C001      		movw r24,r0
 679 0020 1124      		clr __zero_reg__
 680 0022 8C7C      		andi r24,lo8(-52)
 681 0024 2695      		lsr r18
 682 0026 2695      		lsr r18
 683 0028 2373      		andi r18,lo8(51)
 684 002a 822B      		or r24,r18
 685               	/* #APP */
 686               	 ;  107 "fifo.c" 1
 687 002c 8295      		swap r24
 688               	 ;  0 "" 2
 108:fifo.c        ****                 #endif
 109:fifo.c        ****                 FIFO_DATA_PORT.OUT = c;
 690               	.LM19:
 691               	/* #NOAPP */
 692 002e 8093 4406 		sts 1604,r24
 110:fifo.c        ****                 FIFO_DATA_PORT.DIR = 0xff;
 694               	.LM20:
 695 0032 8FEF      		ldi r24,lo8(-1)
 696 0034 8093 4006 		sts 1600,r24
 111:fifo.c        ****                 FIFO_CTL_PORT.OUTCLR = _BV(FIFO_WR_N);
 698               	.LM21:
 699 0038 81E0      		ldi r24,lo8(1)
 700 003a 8093 6606 		sts 1638,r24
 112:fifo.c        ****                 asm volatile("nop");
 702               	.LM22:
 703               	/* #APP */
 704               	 ;  112 "fifo.c" 1
 705 003e 0000      		nop
 706               	 ;  0 "" 2
 113:fifo.c        ****                 asm volatile("nop");
 708               	.LM23:
 709               	 ;  113 "fifo.c" 1
 710 0040 0000      		nop
 711               	 ;  0 "" 2
 114:fifo.c        ****                 FIFO_CTL_PORT.OUTSET = _BV(FIFO_WR_N);
 713               	.LM24:
 714               	/* #NOAPP */
 715 0042 8093 6506 		sts 1637,r24
 115:fifo.c        ****                 FIFO_DATA_PORT.DIR = 0;
 717               	.LM25:
 718 0046 1092 4006 		sts 1600,__zero_reg__
 719               	.L4:
 720 004a 0895      		ret
 722               	.Lscope4:
 723               		.section	.text.fifo_send_char_blocking,"ax",@progbits
 726               	.global	fifo_send_char_blocking
 728               	fifo_send_char_blocking:
 116:fifo.c        ****         }
 117:fifo.c        **** #else // __AVR_XMEGA__
 118:fifo.c        ****         if ((FIFO_CTL_PORT_PIN & _BV(FIFO_TXE_N)) !=  _BV(FIFO_TXE_N))
 119:fifo.c        ****         {
 120:fifo.c        ****                 FIFO_DATA_PORT_DDR = 0xff;
 121:fifo.c        ****                 #ifdef  FIFO_BIT_REVERSE
 122:fifo.c        ****                 REVERSE(c);
 123:fifo.c        ****                 #endif
 124:fifo.c        ****                 FIFO_DATA_PORT = c;
 125:fifo.c        ****                 FIFO_DATA_PORT_DDR = 0xff;
 126:fifo.c        ****                 FIFO_CTL_PORT &= ~_BV(FIFO_WR_N);
 127:fifo.c        ****                 asm volatile("nop");
 128:fifo.c        ****                 asm volatile("nop");
 129:fifo.c        ****                 FIFO_CTL_PORT |= _BV(FIFO_WR_N);
 130:fifo.c        ****                 FIFO_DATA_PORT_DDR = 0;
 131:fifo.c        ****         }
 132:fifo.c        **** #endif // __AVR_XMEGA__
 133:fifo.c        **** }
 134:fifo.c        **** 
 135:fifo.c        **** void fifo_send_char_blocking(uint8_t c)
 136:fifo.c        **** {
 730               	.LM26:
 731               	.LFBB5:
 732               	/* prologue: function */
 733               	/* frame size = 0 */
 734               	/* stack size = 0 */
 735               	.L__stack_usage = 0
 736               	.L9:
 137:fifo.c        **** #ifdef __AVR_XMEGA__
 138:fifo.c        ****         while (FIFO_CTL_PORT.IN & _BV(FIFO_TXE_N))
 738               	.LM27:
 739 0000 9091 6806 		lds r25,1640
 740 0004 92FD      		sbrc r25,2
 741 0006 00C0      		rjmp .L9
 139:fifo.c        **** #else // __AVR_XMEGA__
 140:fifo.c        ****         while (FIFO_CTL_PORT_PIN & _BV(FIFO_TXE_N))
 141:fifo.c        **** #endif // __AVR_XMEGA__
 142:fifo.c        ****         {
 143:fifo.c        ****         };
 144:fifo.c        ****         fifo_send_char(c);
 743               	.LM28:
 744 0008 0C94 0000 		jmp fifo_send_char
 746               	.Lscope5:
 747               		.text
 749               	.Letext0:
 750               		.ident	"GCC: (GNU) 4.8.1"
DEFINED SYMBOLS
                            *ABS*:0000000000000000 fifo.c
     /tmp/ccmR3QK5.s:2      *ABS*:000000000000003e __SP_H__
     /tmp/ccmR3QK5.s:3      *ABS*:000000000000003d __SP_L__
     /tmp/ccmR3QK5.s:4      *ABS*:000000000000003f __SREG__
     /tmp/ccmR3QK5.s:5      *ABS*:0000000000000034 __CCP__
     /tmp/ccmR3QK5.s:6      *ABS*:0000000000000000 __tmp_reg__
     /tmp/ccmR3QK5.s:7      *ABS*:0000000000000001 __zero_reg__
     /tmp/ccmR3QK5.s:534    .text.fifo_init:0000000000000000 fifo_init
     /tmp/ccmR3QK5.s:561    .text.fifo_deinit:0000000000000000 fifo_deinit
     /tmp/ccmR3QK5.s:594    .text.fifo_cur_char:0000000000000000 fifo_cur_char
     /tmp/ccmR3QK5.s:651    .text.fifo_send_char:0000000000000000 fifo_send_char
     /tmp/ccmR3QK5.s:728    .text.fifo_send_char_blocking:0000000000000000 fifo_send_char_blocking

NO UNDEFINED SYMBOLS
