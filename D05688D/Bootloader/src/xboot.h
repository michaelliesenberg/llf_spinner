/************************************************************************/
/* XBoot Extensible AVR Bootloader                                      */
/************************************************************************/

#ifndef __XBOOT_H
#define __XBOOT_H

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/crc16.h>

#include <asf.h>


// token pasting
#define token_paste2_int(x, y) x ## y
#define token_paste2(x, y) token_paste2_int(x, y)
#define token_paste3_int(x, y, z) x ## y ## z
#define token_paste3(x, y, z) token_paste3_int(x, y, z)

// Version
#define XBOOT_VERSION_MAJOR 1
#define XBOOT_VERSION_MINOR 7

#include "config.h"

// Configuration

// clock config
#ifdef __AVR_XMEGA__

#define F_CPU                   32000000UL

// use 32MHz osc if makefile calls for it
#if (F_CPU == 32000000L)
// defaults to 2MHz RC oscillator
// define USE_32MHZ_RC to override
#define USE_32MHZ_RC
#endif // F_CPU

#endif // __AVR_XMEGA__



// LED
#define LED_PORT                token_paste2(PORT, LED_PORT_NAME)
#define LED_PORT_DDR            token_paste2(DDR, LED_PORT_NAME)
#define LED_PORT_PIN            token_paste2(PIN, LED_PORT_NAME)

//GlueLogic
#define GLUE_PORT               token_paste2(PORT, C)
// UART RS485 Enable Output
#define UART_EN_PORT            token_paste2(PORT, UART_EN_PORT_NAME)


#define UART_EN_PORT_DDR        token_paste2(DDR, UART_EN_PORT_NAME)
#define UART_EN_PORT_PIN        token_paste2(PIN, UART_EN_PORT_NAME)

#define UART_RX_PIN             2
#define UART_TX_PIN             3

#define UART_PORT               token_paste2(PORT, UART_PORT_NAME)
#define UART_DEVICE_PORT        token_paste2(UART_PORT_NAME, UART_NUMBER)
#define UART_DEVICE             token_paste2(USART, UART_DEVICE_PORT)
#define UART_DEVICE_RXC_ISR     token_paste3(USART, UART_DEVICE_PORT, _RXC_vect)
#define UART_DEVICE_DRE_ISR     token_paste3(USART, UART_DEVICE_PORT, _DRE_vect)
#define UART_DEVICE_TXC_ISR     token_paste3(USART, UART_DEVICE_PORT, _TXC_vect)
#define UART_RX_PIN_CTRL        token_paste3(UART_PORT.PIN, UART_RX_PIN, CTRL)
#define UART_TX_PIN_CTRL        token_paste3(UART_PORT.PIN, UART_TX_PIN, CTRL)


#if (F_CPU == 32000000L) && (UART_BAUD_RATE == 115200)
#define UART_BSEL_VALUE         1047
#define UART_BSCALE_VALUE       -6
#define UART_CLK2X              0
#else
#if (F_CPU == 2000000L)
#define UART_BSEL_VALUE         ((F_CPU) / ((uint32_t)UART_BAUD_RATE * 8) - 1)
#define UART_BSCALE_VALUE       0
#define UART_CLK2X              1
#else
#define UART_BSEL_VALUE         ((F_CPU) / ((uint32_t)UART_BAUD_RATE * 16) - 1)
#define UART_BSCALE_VALUE       0
#define UART_CLK2X              0
#endif
#endif


// FIFO
#define FIFO_DATA_PORT          token_paste2(PORT, FIFO_DATA_PORT_NAME)
#define FIFO_CTL_PORT           token_paste2(PORT, FIFO_CTL_PORT_NAME)

#ifndef __AVR_XMEGA__
#define FIFO_DATA_PORT_DDR      token_paste2(DDR, FIFO_DATA_PORT_NAME)
#define FIFO_DATA_PORT_PIN      token_paste2(PIN, FIFO_DATA_PORT_NAME)
#define FIFO_CTL_PORT_DDR       token_paste2(DDR, FIFO_CTL_PORT_NAME)
#define FIFO_CTL_PORT_PIN       token_paste2(PIN, FIFO_CTL_PORT_NAME)
#endif // __AVR_XMEGA__

// I2C
#define I2C_DEVICE              token_paste2(TWI, I2C_DEVICE_PORT)
#define I2C_DEVICE_ISR          token_paste3(TWI, I2C_DEVICE_PORT, _TWIS_vect)

// I2C Address Autonegotiation
#define I2C_AUTONEG_PORT                token_paste2(PORT, I2C_AUTONEG_PORT_NAME);

// Attach LED
#define ATTACH_LED_PORT                 token_paste2(PORT, ATTACH_LED_PORT_NAME)


#ifndef EEPROM_PAGE_SIZE
#define EEPROM_PAGE_SIZE E2PAGESIZE
#endif

#ifndef EEPROM_BYTE_ADDRESS_MASK
#if EEPROM_PAGE_SIZE == 32
#define EEPROM_BYTE_ADDRESS_MASK 0x1f
#elif EEPROM_PAGE_SIZE == 16
#define EEPROM_BYTE_ADDRESS_MASK = 0x0f
#elif EEPROM_PAGE_SIZE == 8
#define EEPROM_BYTE_ADDRESS_MASK = 0x07
#elif EEPROM_PAGE_SIZE == 4
#define EEPROM_BYTE_ADDRESS_MASK = 0x03
#else
#error Unknown EEPROM page size!  Please add new byte address value!
#endif
#endif

#ifdef USE_INTERRUPTS
#ifndef NEED_INTERRUPTS
#define NEED_INTERRUPTS
#endif // NEED_INTERRUPTS
#endif // USE_INTERRUPTS

#ifdef USE_AVR1008_EEPROM
#ifndef NEED_INTERRUPTS
#define NEED_INTERRUPTS
#endif // NEED_INTERRUPTS
#endif // USE_AVR1008_EEPROM

#ifdef ENABLE_CODE_PROTECTION
#ifndef NEED_CODE_PROTECTION
#define NEED_CODE_PROTECTION
#endif // NEED_CODE_PROTECTION
#endif // ENABLE_CODE_PROTECTION

#ifdef ENABLE_EEPROM_PROTECTION
#ifndef NEED_CODE_PROTECTION
#define NEED_CODE_PROTECTION
#endif // NEED_CODE_PROTECTION
#endif // ENABLE_EEPROM_PROTECTION

// communication modes
#define MODE_UNDEF              0
#define MODE_UART               1
#define MODE_I2C                2
#define MODE_FIFO               3

// types
typedef uint32_t ADDR_T;

// Includes
#include "protocol.h"
#include "flash.h"
#include "eeprom_driver.h"
#include "uart.h"
#include "watchdog.h"
#include "api.h"

#ifndef __AVR_XMEGA__
#include <avr/wdt.h>
#endif // __AVR_XMEGA__

// globals
#ifdef USE_INTERRUPTS
extern volatile unsigned char comm_mode;

extern volatile unsigned char rx_buff0;
extern volatile unsigned char rx_buff1;
extern volatile unsigned char rx_char_cnt;

extern volatile unsigned char tx_buff0;
extern volatile unsigned char tx_char_cnt;
#else
extern unsigned char comm_mode;
#endif // USE_INTERRUPTS

// Functions
unsigned char __attribute__ ((noinline)) ow_slave_read_bit(void);
void __attribute__ ((noinline)) ow_slave_write_bit(unsigned char b);
void ow_slave_wait_bit(void);

unsigned char __attribute__ ((noinline)) get_char(void);
void __attribute__ ((noinline)) send_char(unsigned char c);
unsigned int __attribute__ ((noinline)) get_2bytes(void);

void clear_buffer(void);

unsigned char BlockLoad(unsigned int size, unsigned char mem, ADDR_T *address);
void BlockRead(unsigned int size, unsigned char mem, ADDR_T *address);

uint16_t crc16_block(uint32_t start, uint32_t length);
void install_firmware(void);


#endif // __XBOOT_H
