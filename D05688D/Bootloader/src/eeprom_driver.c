#include "eeprom_driver.h"
#include "string.h"

#ifdef __AVR_XMEGA__
#ifdef USE_AVR1008_EEPROM

#include <avr/sleep.h>

#endif // USE_AVR1008_EEPROM
#else
#include <avr/boot.h>
#include <avr/io.h>
#endif // __AVR_XMEGA__

#ifdef __AVR_XMEGA__

// NVM call
static inline void NVM_EXEC(void)
{
        void *z = (void *)&NVM_CTRLA;
        
        __asm__ volatile("out %[ccp], %[ioreg]"  "\n\t"
        "st z, %[cmdex]"
        :
        : [ccp] "I" (_SFR_IO_ADDR(CCP)),
        [ioreg] "d" (CCP_IOREG_gc),
                     [cmdex] "r" (NVM_CMDEX_bm),
                     [z] "z" (z)
                     );
}


#define NVM_EXEC_WRAPPER NVM_EXEC



void wait_for_nvm(void)
{
        while (NVM.STATUS & NVM_NVMBUSY_bm) { };
}

void EEPROM_erase_all(void)
{
        wait_for_nvm();
        
        NVM.CMD = NVM_CMD_ERASE_EEPROM_gc;
        NVM_EXEC_WRAPPER();
}


#endif // __AVR_XMEGA__