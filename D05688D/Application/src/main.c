/**
 * \file
 *
 * \brief Firmware f�r Fast Ethernet IRT
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * 20180618:
 * HEY
 * for D05688 (2017 05 E20586-01 IRT FPGA)
 * adjustment for HW changes:
 * ADC3: from 1.8V --> 1.1V
 * PC0 VCXO_SDA --> nFPGA
 * PC1 VCXO_SCL --> Not connected
 * PC5 SCK -> not connected
 * PC6 MISO -> not connected
 * PC7 MOSI -> not connected
 * PD6 nRESET_PHY -> PHY_RESET_MC (inverted logic)
 * PD7 ADC15: nReset_CPLD --> +3.3V (ADC input; was on ADC0) 
 *
 * 20180112:
 * HEY
 * Umsetzung von BASCOM (A99080) in C unter Benutzung der ASF bibliothek
 * �nderungen im Vergleich zum urspr�nglichen Code:
 * -5V: wird jetzt �ber den ADC(7) gelesen und mit einem Schwellwert (~2V) verglichen.
 *      bisher wurde der Pin als digitaler Eingang verwendet.
 * DIAG_TX: im Betrieb ist DIAG_TX jetzt LOW, und wird erst f�r die Kommunkikation freigeschaltet
 * N_DIAG_TX wird jetzt aus DIAG_TX �ber die XMEGA Custom Logic (XCL) generiert
 *           dadurch ist das Sendesignal gr��er
 * +3V3: jetzt wird die +3V3 Analogspannung �ber ADC1 eingelesen.
 *       bisher wurde die Spannung �ber den ADC0 gelesen. Dies hat zum schalten des
 *       Analogkomperators gef�hrt. Dadurch konnte der USART das darauffolgende Zeichen nicht richtig einlesen.
 *       Durch Verwendung von ADC1 wird der Effekt minimiert.

 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>
#include <stdio.h>
#include <util/atomic.h>

#include "main.h"
#include "phy.h"
#include "led.h"
#include "externalclock.h"
#include "serialport1_PC.h"
#include "serialport2_PLD.h"
#include "boardAdc.h"
#include "gluelogic.h"
#include "fuses.h"
#include "nvmManagement.h"
#include "protokollparser.h"
#include "crcCalc.h"


#define MAIN_TIME_GAIN                  5
#define MAIN_TIME_TOGGLE_FAST           142
#define MAIN_POSITION_ENTERDIAGNOSTIC   0
#define MAIN_POSITION_SENDTOOTHERSIEDE  600
#define MAIN_POSITION_ADC               150
#define MAIN_POSITION_END               999

uint8_t SW_VERSION [8]= "D05688D ";// Version can be 8Bytes D05688XX
uint16_t SW_VERSION_HEADER = 2;

uint16_t uiMsMainLoopCounter = 0;

bool bInitialized = false;
bool bClockOk = false;
bool bPhyOk = false;
bool bLLFOk=false;
bool bIsBody = false;
bool bFlagUART_MODE=false;
uint8_t is_FPGA_Board=0;

void mainInit(void);
void mainLoop(void);


void mainInit(void) 
{
	uint8_t ucPhyInitCounter = 0;

    sysclk_init();
    sleepmgr_init();

	wdt_reset();
 
    irq_initialize_vectors();
	
    vBoardInit();
    vUART1Init();//Setup UART for PC
    vUART2Init();//Setup UART for PLD
    irq_initialize_vectors();
    cpu_irq_enable();
   // wdt_set_timeout_period(WDT_TIMEOUT_PERIOD_8KCLK);
	
	bIsBody = ioport_get_pin_level(BODY_SELECT) == IS_BODY;

    bInitialized = false;
    bClockOk = false;
    bPhyOk = false;
	bLLFOk=false;
	
	 wdt_reset();
	 
    _delay_ms(10);
	if(is_FPGA_Board)
	{
	    ioport_set_pin_level(N_FPGA_RESET, 0);
    	ioport_set_pin_level(N_RESET_PHY, 1);
	}
	else
		ioport_set_pin_level(N_RESET_PHY, 0);

    _delay_ms(100);
    ioport_set_pin_level(LED, LED_ON);

	if(bNvmConfigIsEmpty())
	{
		PC_body.BODY.PHY0_DUPPLEX=FULL;
		PC_body.BODY.PHY1_DUPPLEX=FULL;
		PC_body.BODY.LLF_PHY0=DEACTIVE;
		PC_body.BODY.LLF_PHY1=DEACTIVE;
		PC_body.HOLLOW.PHY0_DUPPLEX=FULL;
		PC_body.HOLLOW.PHY1_DUPPLEX=FULL;
		PC_body.HOLLOW.LLF_PHY0=ACTIVE;
		PC_body.HOLLOW.LLF_PHY1=ACTIVE;
		PC_body.CRC_Checksum =uiCalculateCrc(&PC_body, ((sizeof(PC_body)*sizeof(uint8_t)) -2));
		bWriteConfigToNvm();
	}
	 wdt_reset();
	_delay_ms(1800);

    while (!bInitialized) 	
	{
		// wdt_reset();
		// set FPGA & PHY in reset
		if(is_FPGA_Board)
		{
			if(!bClockOk)
			{
        		ioport_set_pin_level(N_FPGA_RESET, 0); 
			    if (! (ucPhyInitCounter & 0x03))
					ioport_set_pin_level(N_RESET_PHY, 1);
				_delay_ms(200);
				// release FPGA from reset
				ioport_set_pin_level(N_FPGA_RESET, 1);
				_delay_ms(200);
				// release PHY from reset
				if (! (ucPhyInitCounter & 0x03)) {
				 ioport_set_pin_level(N_RESET_PHY, 0);
				}            
				_delay_ms(200); 
				bClockOk=true;
			}
		}       
		else if(is_FPGA_Board==0)
		{
        	if (!bClockOk) 
			{
				bClockOk = bExternalClockChipSetup();
        	}
			if (bClockOk) 
			{
	            _delay_ms(2000);
	            ioport_set_pin_level(N_RESET_PHY, 1);
	            _delay_ms(100);
	        }
		} 
		bPhyOk = bInitializePhy();
			
		if (bClockOk && bPhyOk) 
		{
            bInitialized = true;
            vLedBlink3();
        }
    }
}


void mainLoop(void) {
    uint16_t uiMsMainLoopCounter = 0;
    uint8_t ucDiagTxCounter = 0;
    uint8_t ucAdcIndex = 0;

    if(is_FPGA_Board==0)
		ioport_set_pin_level(N_RESET_CPLD, 1);
    wdt_enable();

    while(true) 
	{
        if(tc45_is_cc_interrupt (&TCC4, TC45_CCA)) //Timer Interrupt
		{
            tc45_clear_cc_interrupt(&TCC4, TC45_CCA);

            if ((uiMsMainLoopCounter % MAIN_TIME_GAIN) == 0) 
			{
                vReadAdc(ADC_INDEX_GAIN);
                vScanCommandLine();
            }

            if ((uiMsMainLoopCounter % MAIN_TIME_TOGGLE_FAST) == 0) 
			{
                if (uiMsMainLoopCounter == 0) 
				{
                    if (bFlagUART_MODE) 
					{
                        ioport_toggle_pin_level(LED);
                    }
                 } 
				 else 
				 {
                    if (!bInitialized) 
					{
                        ioport_toggle_pin_level(LED);
                    }
                }
            }

            if (uiMsMainLoopCounter == MAIN_POSITION_ENTERDIAGNOSTIC && (bFlagUART_MODE)) 
			{
				if(is_FPGA_Board==0)
				{
					ioport_set_pin_level(N_RESET_PHY, 0);
					//ioport_set_pin_level(N_RESET_CPLD, 0);
				}
				else
				{
					ioport_set_pin_level(N_RESET_PHY, 1);
					//ioport_set_pin_level(N_FPGA_RESET, 1);
				}
            }
            if (uiMsMainLoopCounter > MAIN_POSITION_ADC && ucAdcIndex < ADC_INDEX_END) 
			{
                if (ucAdcIndex != ADC_INDEX_GAIN) 
				{
                    vReadAdc(ucAdcIndex);
                }
                ucAdcIndex++;
            }
            if (uiMsMainLoopCounter == MAIN_POSITION_SENDTOOTHERSIEDE) 
			{
				bPhyCheckStatus();//Get Status connection
                vSendDiagnosticToOtherSide();
            }

            if (uiMsMainLoopCounter == MAIN_POSITION_END) 
			{
                uiMsMainLoopCounter = 0;
                ucAdcIndex = 0;

                if (ucEnableMinMax < 3) 
				{
                    ucEnableMinMax++;
                }
                wdt_reset();
            } 
			else 
			{
                ATOMIC_BLOCK(ATOMIC_RESTORESTATE) 
				{
                    uiMsMainLoopCounter++;
                }
            }
        }
    }
}

int main (void)
{	
	mainInit();
    mainLoop();
}
