

#ifndef ADC_H_
#define ADC_H_


uint8_t ucEnableMinMax;

void vAdcInit(void);
void vReadAdc(uint8_t ucIndex);

#endif /* ADC_H_ */