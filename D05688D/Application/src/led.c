/*
 * led.c
 *
 * Created: 04.01.2018 07:52:56
 *  Author: Eyermann
 *
 * This file contains routines to drive the LED
 */ 

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>

#include "led.h"

void vLedInit(void) {
    ioport_set_pin_dir(LED, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(LED, LED_OFF);
}

void vLedBlink3(void) {
    uint8_t ucCounter = 0;
    ioport_set_pin_level(LED, LED_OFF);
    
    for(ucCounter = 0; ucCounter < 6; ucCounter++) {
        ioport_toggle_pin_level(LED);
        _delay_ms(200);
    }
}