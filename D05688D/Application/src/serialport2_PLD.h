/*
 * serialport_1.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */ 

#ifndef serialport_2_h
#define serialport_2_h

extern	uint16_t uiVPHYODupplestherSide;
extern	uint16_t uiVPHY1DupplestherSide;
extern Bool bOtherSideInBootloader;
extern Bool bSendResettoOtherSide;
extern void vUART2Init(void);
void vSendDiagnosticToOtherSide(void);

#endif