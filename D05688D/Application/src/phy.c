/*
 * phy.c
 *
 * Created: 04.01.2018 07:52:56
 *  Author: Eyermann
 *
 * This file contains routines to access the PHY's (TLK105)
 *
 */ 

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>
#include <stdio.h>

#include "phy.h"
#include "nvmManagement.h"
#include "serialport2_PLD.h"
#include "serialport1_PC.h"
#include "main.h"

#define CLOCK_POSITION_IDLE         (0)
#define CLOCK_COUNT_IDLE            (2 * 4)
#define CLOCK_POSITION_START        (CLOCK_POSITION_IDLE + CLOCK_COUNT_IDLE)
#define CLOCK_COUNT_START           (2 * 2)
#define CLOCK_POSITION_OPCODE       (CLOCK_POSITION_START + CLOCK_COUNT_START)
#define CLOCK_COUNT_OPCODE          (2 * 2)
#define CLOCK_POSITION_PHY_ADDRESS  (CLOCK_POSITION_OPCODE + CLOCK_COUNT_OPCODE)
#define CLOCK_COUNT_PHY_ADDRESS     (2 * 5)
#define CLOCK_POSITION_REG_ADDRESS  (CLOCK_POSITION_PHY_ADDRESS + CLOCK_COUNT_PHY_ADDRESS)
#define CLOCK_COUNT_REG_ADDRESS     (2 * 5)
#define CLOCK_POSITION_TA           (CLOCK_POSITION_REG_ADDRESS + CLOCK_COUNT_REG_ADDRESS)
#define CLOCK_COUNT_TA              (2 * 2)
#define CLOCK_POSITION_DATA         (CLOCK_POSITION_TA + CLOCK_COUNT_TA)
#define CLOCK_COUNT_DATA            (2 * 16)
#define CLOCK_POSITION_END          (CLOCK_POSITION_DATA + CLOCK_COUNT_DATA)
#define CLOCK_COUNT_END             (2 * 2)
#define CLOCK_POSITION_FINISHED     (CLOCK_POSITION_END + CLOCK_COUNT_END)





enum eAccess {
    Write = 0, 
    Read = 1
};


bool bDisableAutoNegotiation(uint16_t phy);
bool bInitializePhyDefault(uint8_t ucPhy);

uint16_t uiMdioTransfer(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData, enum eAccess eAccessType);

bool bLLFPHY0 =0;
bool bLLFPHY1 =0;
bool bLLFPHY0_OtherSide=0;
bool bLLFPHY1_OtherSide=0;

bool bLLF_PHY0_Active=0;
bool bLLF_PHY0_ActiveOtherSide=0;
bool bLLF_PHY1_Active=0;
bool bLLF_PHY1_ActiveOtherSide=0;



uint16_t uiMdioTransfer(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData, enum eAccess eAccessType) 
{
    uint8_t ucClockCounter = 0;
    volatile uint8_t ucClockPosition;
    uint16_t uiTempData = 0;
    uint16_t uiReadData = 0;
    bool bLevel = 0;
    
    
    if (ucPhy > MAX_PHY_ADDR) 
	{
        return (0xFFFF);
    }
    while (ucClockCounter < CLOCK_POSITION_FINISHED) 
	{
        while (!tc45_is_cc_interrupt (&TCC4, TC45_CCA))
        {}
            
        tc45_clear_cc_interrupt(&TCC4, TC45_CCA);
        ioport_set_pin_level(MDIO_CLK, ucClockCounter & 0x01);
            
        if ((ucClockCounter & 0x01) == 0x00) 
		{
            if ( ucClockCounter < CLOCK_POSITION_IDLE + CLOCK_COUNT_IDLE) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_IDLE) / 2;
                    
            } 
			else if (ucClockCounter < CLOCK_POSITION_START + CLOCK_COUNT_START) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_START) / 2;
                if (ucClockPosition == 0) 
				{
                    ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_OUTPUT);
                }
                bLevel = ((ucClockPosition & 0x01) == 0x01);
                
            } 
			else if (ucClockCounter < CLOCK_POSITION_OPCODE + CLOCK_COUNT_OPCODE) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_OPCODE) / 2;                    
                bLevel = ((ucClockPosition & 0x01) == 0x01);
                if (eAccessType == Read) 
				{
                    bLevel = !bLevel;
                }
                
            } 
			else if (ucClockCounter < CLOCK_POSITION_PHY_ADDRESS + CLOCK_COUNT_PHY_ADDRESS) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_PHY_ADDRESS) / 2;
                if (ucClockPosition == 0) 
				{
                    uiTempData = ucPhy;
                }
                bLevel = ((uiTempData & 0x10) == 0x10);
                uiTempData <<= 1;                    
            } 
			else if (ucClockCounter < CLOCK_POSITION_REG_ADDRESS + CLOCK_COUNT_REG_ADDRESS) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_REG_ADDRESS) / 2;
                if (ucClockPosition == 0) 
				{
                    uiTempData = ucAddress;
                }
                bLevel = ((uiTempData & 0x10) == 0x10);
                uiTempData <<= 1;
            } 
			else if (ucClockCounter < CLOCK_POSITION_TA + CLOCK_COUNT_TA) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_TA) / 2;
                if (ucClockPosition == 0 && eAccessType == Read) 
				{
                    ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_INPUT);
                }
                bLevel = ((ucClockPosition & 0x01) == 0x00);
            } 
			else if (ucClockCounter < CLOCK_POSITION_DATA + CLOCK_COUNT_DATA) 
			{
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_DATA) / 2;
                if (ucClockPosition == 0) 
				{
                    uiTempData = uiData;
                }
                bLevel = ((uiTempData & 0x8000) == 0x8000);
                uiTempData <<= 1;
                uiReadData <<= 1;
                if (ioport_get_pin_level(MDIO_DATA) == true) 
				{
                    uiReadData |= 0x0001;
                }
            } 
			else 
			{
                bLevel = true;
                ioport_set_pin_level(MDIO_DATA, bLevel);
                ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_INPUT);
                break;
            }
            ioport_set_pin_level(MDIO_DATA, bLevel);                
        }            
        ucClockCounter++;
    }
    return uiReadData;
}

uint16_t uiMdioReadRegister(uint8_t ucPhy, uint8_t ucAddress) 
{
    return uiMdioTransfer(ucPhy, ucAddress, 0, Read);    
}


void vMdioWriteRegister(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData) 
{
    uiMdioTransfer(ucPhy, ucAddress, uiData, Write);    
}

bool bInitializePhyDefault(uint8_t ucPhy) 
{
    uint16_t uiData;

    vMdioWriteRegister(ucPhy, PHY_ADDR_ANAR, PHY_ANAR_100BT_ONLY);
    _delay_ms(10);
    uiData = uiMdioReadRegister(ucPhy, PHY_ADDR_ANAR);
    _delay_ms(10);
    if (uiData != PHY_ANAR_100BT_ONLY) 
	{
        return false;
    }
    return true;
}    
bool bPhyPowerDown(uint8_t ucPhy)
{
	uint16_t uiData;
	uint16_t uiDataRead;
	
	uiDataRead = uiMdioReadRegister(PHY0_ADDR, PHY_ADDR_BMCR);

	vMdioWriteRegister(ucPhy, PHY_ADDR_BMCR, uiDataRead | PHY_BMCR_POWER_DOWN);
	_delay_ms(10);
	uiData = uiMdioReadRegister(ucPhy, PHY_ADDR_BMCR);
	_delay_ms(10);
	if (uiData != (uiDataRead|PHY_BMCR_POWER_DOWN))
	{
		return false;
	}
	return true;
}    
bool bPhyPowerUp(uint8_t ucPhy)
{
	uint16_t uiData;
	uint16_t uiDataRead;
	
	uiDataRead = uiMdioReadRegister(PHY0_ADDR, PHY_ADDR_BMCR);

	vMdioWriteRegister(ucPhy, PHY_ADDR_BMCR, uiDataRead & PHY_BMCR_POWER_UP);
	_delay_ms(10);
	uiData = uiMdioReadRegister(ucPhy, PHY_ADDR_BMCR);
	_delay_ms(10);
	if (uiData != (uiDataRead & PHY_BMCR_POWER_UP))
	{
		return false;
	}
	return true;
}
void bPhyCheckStatus(void)
{
	uint16_t uiData;
	
	if(IRT_body.BODY.LLF_PHY0==ACTIVE && !bIsBody || IRT_body.HOLLOW.LLF_PHY0==ACTIVE && bIsBody)
	{
		bLLF_PHY0_Active = true;
	}
	else
	{
		bLLF_PHY0_Active = false;
	}
	if(IRT_body.BODY.LLF_PHY1==ACTIVE && !bIsBody || IRT_body.HOLLOW.LLF_PHY1==ACTIVE && bIsBody)
	{
		bLLF_PHY1_Active = true;
	}
	else
	{
		bLLF_PHY1_Active = false;
	}

	uiData = uiMdioReadRegister(PHY0_ADDR, PHY_ADDR_BMSR);
	
	if(uiData & 0x0004)
	{
		if(bIsBody)
			IRT_body.BODY.LLF_PHY0_LINKUP=1;
		else
			IRT_body.HOLLOW.LLF_PHY0_LINKUP=1;
	}
	else
	{
		if(bIsBody)
			IRT_body.BODY.LLF_PHY0_LINKUP=0;
		else
			IRT_body.HOLLOW.LLF_PHY0_LINKUP=0;
	}
		
	_delay_ms(10);
	
	uiData = uiMdioReadRegister(PHY1_ADDR, PHY_ADDR_BMSR);
		
	if(uiData & 0x0004)//connected
	{
		if(bIsBody)
			IRT_body.BODY.LLF_PHY1_LINKUP=1;
		else
			IRT_body.HOLLOW.LLF_PHY1_LINKUP=1;
	}
	else
	{
		if(bIsBody)
			IRT_body.BODY.LLF_PHY1_LINKUP=0;
		else
			IRT_body.HOLLOW.LLF_PHY1_LINKUP=0;
	}
	
	if((bIsBody && IRT_body.HOLLOW.LLF_PHY0_LINKUP==1) || (!bIsBody && IRT_body.BODY.LLF_PHY0_LINKUP==1))
	{
		bLLFPHY0_OtherSide=1;
	}
	else
	{
		bLLFPHY0_OtherSide=0;
	}
	if((bIsBody && IRT_body.HOLLOW.LLF_PHY1_LINKUP==1) || (!bIsBody && IRT_body.BODY.LLF_PHY1_LINKUP==1))
	{
		bLLFPHY1_OtherSide=1;
	}
	else
	{
		bLLFPHY1_OtherSide=0;
	}
	_delay_ms(10);
	

	if(bLLF_PHY0_Active==1 && bLLFPHY0_OtherSide==0)
	{
		bPhyPowerDown(PHY0_ADDR);//Link down 
	}
	else if(bLLF_PHY0_Active==1 && bLLFPHY0_OtherSide==1)
	{
		bPhyPowerUp(PHY0_ADDR);//Link up
	}
	
	if(bLLF_PHY1_Active==1 && bLLFPHY1_OtherSide==0)
	{
		bPhyPowerDown(PHY1_ADDR);//Link down
	}
	else if(bLLF_PHY1_Active==1  && bLLFPHY1_OtherSide==1)
	{
		bPhyPowerUp(PHY1_ADDR);//Link up
	}
	
	if((PC_body.BODY.PHY0_DUPPLEX==HALF && bIsBody) || (PC_body.HOLLOW.PHY0_DUPPLEX==HALF && !bIsBody))
	{
		uiData = uiMdioReadRegister(PHY0_ADDR, 0x0004);
		
		if(uiData & 0x0080 != 0x0080)
		{
			_delay_ms(10);
			vMdioWriteRegister(PHY0_ADDR, 0x0004, 0x0081);
		}
	}
	if((PC_body.BODY.PHY1_DUPPLEX==HALF && bIsBody) || (PC_body.HOLLOW.PHY1_DUPPLEX==HALF && !bIsBody))
	{
		uiData = uiMdioReadRegister(PHY1_ADDR, 0x0004);
		
		if(uiData & 0x0080 != 0x0080)
		{
			_delay_ms(10);
			vMdioWriteRegister(PHY1_ADDR, 0x0004, 0x0081);
		}
	}

}
bool bInitializePhy(void) 
{
    uint8_t ucIndex = 0;
    uint8_t ucPhyAddress;
    uint16_t uiData;
    
  
  if(bIsBody)
  {
		if(PC_body.BODY.PHY0_DUPPLEX==FULL)
		{
			vMdioWriteRegister(PHY0_ADDR, 0x0004, 0x0101);
			_delay_ms(10);
			uiData = uiMdioReadRegister(PHY0_ADDR, 0x0004);

			if (uiData != 0x0101)
			return false;
		}
		else
		{
			vMdioWriteRegister(PHY0_ADDR, 0x0004, 0x0081);
			_delay_ms(10);
			uiData = uiMdioReadRegister(PHY0_ADDR, 0x0004);
			
			if (uiData != 0x0081)
			return false;
		}
		if(PC_body.BODY.PHY1_DUPPLEX==FULL)
		{
			vMdioWriteRegister(PHY1_ADDR, 0x0004, 0x0101);
			_delay_ms(10);
			uiData = uiMdioReadRegister(PHY1_ADDR, 0x0004);
			
			if (uiData != 0x0101)
			return false;
		}
		else
		{
			vMdioWriteRegister(PHY1_ADDR, 0x0004, 0x0081);
			_delay_ms(10);
			uiData = uiMdioReadRegister(PHY1_ADDR, 0x0004);
			
			if (uiData != 0x0081)
			return false;
		}
  }
  else
  {
	  if(PC_body.HOLLOW.PHY0_DUPPLEX==0x46)
	  {
		  vMdioWriteRegister(PHY0_ADDR, 0x0004, 0x0101);
		  _delay_ms(10);
		  uiData = uiMdioReadRegister(PHY0_ADDR, 0x0004);
		
		  if (uiData != 0x0101)
			return false;
	  }
	  else
	  {
		  vMdioWriteRegister(PHY0_ADDR, 0x0004, 0x0081);
		  _delay_ms(10);
		  uiData = uiMdioReadRegister(PHY0_ADDR, 0x0004);
	  
		  if (uiData != 0x0081)
			return false;
	  }
	  if(PC_body.HOLLOW.PHY1_DUPPLEX==0x46)
	  {
		  vMdioWriteRegister(PHY1_ADDR, 0x0004, 0x0101);
		  _delay_ms(10);
		  uiData = uiMdioReadRegister(PHY1_ADDR, 0x0004);
	  
		  if (uiData != 0x0101)
		  return false;
	  }
	  else
	  {
		  vMdioWriteRegister(PHY1_ADDR, 0x0004, 0x0081);
		  _delay_ms(10);
		  uiData = uiMdioReadRegister(PHY1_ADDR, 0x0004);
	  
		  if (uiData != 0x0081)
		  return false;
	  }
  }
		
    return true;  
}
  