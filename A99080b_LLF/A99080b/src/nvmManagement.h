

#ifndef NVM_MANAGEMENT_H_
#define NVM_MANAGEMENT_H_

#define NVM_BLOCK_SIZE          5
#define NVM_BLOCK_MAXENTRIES    20

#define NVM_BLOCK_CHIP_ID       0
#define NVM_BLOCK_ADDRESS       1
#define NVM_BLOCK_DATA          3

#define NVM_ID_PHY0             0x00
#define NVM_ID_PHY1             0x01
#define NVM_ID_LLF				0x02
#define NVM_ID_END              0xF0

struct stConfigData {
    uint8_t ucChip;
    uint16_t uiAddress;
    uint16_t uiData;
};


bool bNvmConfigIsEmpty(void);
bool bNvmHasConfigForPhy(uint8_t ucPhy);
bool bNvmConfigCrcIsOk(void);
bool bWriteConfigToNvm(char * pcBuffer, uint8_t ucSize,bool isString);
void vGetNvmConfig(struct stConfigData *stStructureConfigData, uint8_t ucIndex);
bool bNvmWriteDefaultConfig(void);
bool bNvmGetLLF(void);
#endif /* NVM_MANAGEMENT_H_ */

