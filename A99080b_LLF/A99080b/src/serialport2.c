/*
 * serialport_2.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <stdbool.h>
#include <string.h>
#include <avr/interrupt.h>
#include <nvm.h>
#include <limits.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "serialport2.h"
#include "serialport1.h"
#include "boardAdc.h"
#include "main.h"

bool bIsSending = false;

uint8_t aucRcvBuffer[BUFFER_SIZE];

uint8_t ucOtherSideReceivedCounter = 0;

uint16_t uiV50vOtherSide = 0;
uint16_t uiV33vOtherSide = 0;
uint16_t uiV18vOtherSide = 0;
uint16_t uiV25vOtherSide = 0;
uint16_t uiI50vOtherSide = 0;
uint16_t uiI33vOtherSide = 0;
uint16_t uiTemperatureOtherSide = 0;
uint16_t uiVGainOtherSide = 0;
uint16_t uiVGainMinOtherSide = USHRT_MAX;
uint16_t uiVGainMaxOtherSide = 0;
int16_t iMV50vOtherSide = 0;

void vSendWordToOtherSide(uint16_t uiData);
void vReceiveDataFromOtherSide(uint32_t ulReceivedByte);


void vUART2Init(void) {

    static usart_rs232_options_t usart_2_options = {
        .baudrate = USART_2_BAUDRATE,
        .charlength = USART_2_CHAR_LENGTH,
        .paritytype = USART_2_PARITY,
        .stopbits = USART_2_STOP_BIT,
    };
    ioport_set_pin_dir(USART_2_TX, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(USART_2_RX, IOPORT_DIR_INPUT);
    usart_init_rs232(USART_2, &usart_2_options);
	usart_set_rx_interrupt_level(USART_2, USART_INT_LVL_MED);
}

void vSendWordToOtherSide(uint16_t uiData) {
    usart_putchar(USART_2, uiData >> 8);
    usart_putchar(USART_2, uiData & 0xFF);
}

void vClearSendBuffer(void) {
    memset(cSendBuffer, 0, SEND_BUFFER_SIZE);
    ucBytesInSendBuffer = 0;
}

void vSendConfigurationToOtherSide(char * pcBuffer, uint8_t ucLength) {
    uint8_t ucIndex;
    if (bIsSending) {
        _delay_us(100);
        if (bIsSending) {
            return;
        }
    }
    bIsSending = true;
    usart_putchar(USART_2, CHAR_SOH);
    usart_putchar(USART_2, CHAR_ENQ);
    for(ucIndex = 0; ucIndex < ucLength; ucIndex++) {
        usart_putchar(USART_2, pcBuffer[ucIndex]);
    }
    usart_putchar(USART_2, CHAR_END);
    _delay_ms(1);
    bIsSending = false;
    vClearSendBuffer();
}

void vSendDiagnosticToOtherSide(void) {
    if (bIsSending) {
        _delay_us(100);
        if (bIsSending) {
            return;
        }
    }
    bIsSending = true;
    usart_putchar(USART_2, CHAR_SOH);
    usart_putchar(USART_2, CHAR_STX);

    vSendWordToOtherSide(uiV50v);
    vSendWordToOtherSide(uiV33v);
    vSendWordToOtherSide(uiV18v);
    vSendWordToOtherSide(uiV25v);
    vSendWordToOtherSide(uiI50v);
    vSendWordToOtherSide(uiI33v);
    vSendWordToOtherSide(uiTemperature);
    vSendWordToOtherSide(uiVGain);
    vSendWordToOtherSide(uiVGainMin);
    vSendWordToOtherSide(uiVGainMax);
    vSendWordToOtherSide(iMV50v);

    usart_putchar(USART_2, CHAR_END);
    _delay_ms(1);
    bIsSending = false;

}

// ---------------------------------------------------------------
// Receives diagnostic data from the other side
// ---------------------------------------------------------------
void vReceiveDiagnosticDataFromOtherSide(void) {
    uiV50vOtherSide = aucRcvBuffer[ 0] << 8 | aucRcvBuffer[ 1];         // convert 2 bytes to word
    uiV33vOtherSide = aucRcvBuffer[ 2] << 8 | aucRcvBuffer[ 3];
    uiV18vOtherSide = aucRcvBuffer[ 4] << 8 | aucRcvBuffer[ 5];
    uiV25vOtherSide = aucRcvBuffer[ 6] << 8 | aucRcvBuffer[ 7];
    uiI50vOtherSide = aucRcvBuffer[ 8] << 8 | aucRcvBuffer[ 9];
    uiI33vOtherSide = aucRcvBuffer[10] << 8 | aucRcvBuffer[11];
    uiTemperatureOtherSide = aucRcvBuffer[12] << 8 | aucRcvBuffer[13];
    uiVGainOtherSide = aucRcvBuffer[14] << 8 | aucRcvBuffer[15];
    uiVGainMinOtherSide = aucRcvBuffer[16] << 8 | aucRcvBuffer[17];
    uiVGainMaxOtherSide = aucRcvBuffer[18] << 8 | aucRcvBuffer[19];
    iMV50vOtherSide = aucRcvBuffer[20] << 8 | aucRcvBuffer[21];
    memset(aucRcvBuffer, 0, BUFFER_SIZE);
};


void vReceiveDataFromOtherSide(uint32_t ulReceivedByte) {
    static bool bIsConfigData = false;
    static uint8_t ucOtherSideReceivedByteIndex;
    static volatile uint16_t uiErrorCount = 0;
    static uint16_t uiLastMainLoopCounter;

    if (uiMsMainLoopCounter - uiLastMainLoopCounter > 10) {
        ucOtherSideReceivedByteIndex = 0;
    }
    uiLastMainLoopCounter = uiMsMainLoopCounter;

    if (ucOtherSideReceivedByteIndex == 0) {
        if (ulReceivedByte == CHAR_SOH) {
            ucOtherSideReceivedByteIndex++;
        }
    } else if (ucOtherSideReceivedByteIndex == 1) {
        if (ulReceivedByte == CHAR_STX) {
            // Diagnostic data
            ucOtherSideReceivedByteIndex++;
            bIsConfigData = false;
        } else if (ulReceivedByte == CHAR_ENQ) {
            // Configuration data
            ucOtherSideReceivedByteIndex++;
            bIsConfigData = true;
        } else {
            ucOtherSideReceivedByteIndex = 0;
        }
    } else if (bIsConfigData) {
        // config data
        if (ulReceivedByte == CHAR_END) {
            if (ucBytesInCommandBuffer == 0) {
                memcpy(cCommandBuffer, aucRcvBuffer, (ucOtherSideReceivedByteIndex - 2) * sizeof(uint8_t));
                ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                    ucBytesInCommandBuffer = ucOtherSideReceivedByteIndex - 2;
                    bCommandFromLocalSide = false;
                }
            }
            ucOtherSideReceivedByteIndex = 0;
        } else {
            if (ucOtherSideReceivedByteIndex < (BUFFER_SIZE + 2)) {
                aucRcvBuffer[ucOtherSideReceivedByteIndex - 2] = ulReceivedByte;
                ucOtherSideReceivedByteIndex++;
            } else {
                memset(aucRcvBuffer, 0, BUFFER_SIZE);
                ucOtherSideReceivedByteIndex = 0;
            }
        }
    } else {
        // diagnostic data
        if (ucOtherSideReceivedByteIndex == UART_2_DIAGNOSTIC_DATA_SIZE + 2) {
            if (ulReceivedByte == CHAR_END) {
                vReceiveDiagnosticDataFromOtherSide();
                ucOtherSideReceivedCounter++;
            } else {
                uiErrorCount++;
            }
            memset(aucRcvBuffer, 0, BUFFER_SIZE);
            ucOtherSideReceivedByteIndex = 0;
        } else {
            aucRcvBuffer[ucOtherSideReceivedByteIndex - 2] = ulReceivedByte;
            ucOtherSideReceivedByteIndex++;
        }
    }
}

ISR(USART_2_RX_ISR_HANDLER)
{
    if (usart_rx_is_complete(USART_2)) {
        uint32_t ulReceivedByte;
        ulReceivedByte = usart_getchar(USART_2);
        vReceiveDataFromOtherSide(ulReceivedByte);
    }
}


ISR(USART_2_DRE_ISR_HANDLER)
{
    // USART_Dre(&USART_C0);
}