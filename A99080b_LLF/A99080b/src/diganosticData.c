


#include <asf.h>
#include <board.h>
#include <conf_board.h>


#include "diagnosticData.h"
#include "serialport1_PC.h"
#include "serialport2_PLD.h"
#include "boardAdc.h"
#include "main.h"
#include "phy.h"
void vPrintNumber(int16_t iData, bool bInsertDecimal);
void vSendStringNoCrc(const char *cBuffer);


void vPrintNumber(int16_t iData, bool bInsertDecimal) {
	char cBuffer[10];
	uint8_t ucIndex;
	itoa(iData, cBuffer, 10);
	ucIndex = 0;
	while (cBuffer[ucIndex] != 0) {
		usart_serial_putchar(USART_1, cBuffer[ucIndex]);
		if (bInsertDecimal && (ucIndex == 0 && cBuffer[0] != '-') ) {
			usart_serial_putchar(USART_1, '.');
		}
		if (bInsertDecimal && (ucIndex == 1 && cBuffer[0] == '-') ) {
    		usart_serial_putchar(USART_1, '.');
		}
		if (bInsertDecimal && ucIndex > 1) {
			break;
		}
		ucIndex++;
	}
}

void vSendStringNoCrc(const char *cBuffer) {
    vAddStringToSendBuffer(cBuffer, false);
}


void vDiagPrintText(uint8_t ucIndex) {
    bool bIsBody;
    uint16_t uiData1, uiData2;
    bIsBody = ioport_get_pin_level(BODY_SELECT) == IS_BODY;
	
		
    vDisableSendBuffer();
    switch (ucIndex)
    {
    case PRINT_INDEX_5V:
        uiData1 = uiV50v;
        uiData2 = uiV50vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_5V_START);
        vPrintNumber(uiData1, true);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, true);
        vSendStringNoCrc(S_VOLTAGE_END);
        
    	break;
    case PRINT_INDEX_3V3:
        uiData1 = uiV33v;
        uiData2 = uiV33vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_3V3_START);
        vPrintNumber(uiData1, true);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, true);
        vSendStringNoCrc(S_VOLTAGE_END);

        break;
    case PRINT_INDEX_1V8:
        uiData1 = uiV18v;
        uiData2 = uiV18vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        if(is_FPGA_Board==0)
            vSendStringNoCrc(S_1V8_START);
        else
            vSendStringNoCrc(S_1V1_START);
        
        vPrintNumber(uiData1, true);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, true);
        vSendStringNoCrc(S_VOLTAGE_END);

        break;
    case PRINT_INDEX_2V5:
        uiData1 = uiV25v;
        uiData2 = uiV25vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_2V5_START);
        vPrintNumber(uiData1, true);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, true);
        vSendStringNoCrc(S_VOLTAGE_END);
        break;
    case PRINT_INDEX_M5V:
        uiData1 = iMV50v;
        uiData2 = iMV50vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_M5V_START);       
        vPrintNumber(uiData1, true);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, true);
        vSendStringNoCrc(S_VOLTAGE_END);
        break;
    case PRINT_INDEX_I3V3:
        uiData1 = uiI33v;
        uiData2 = uiI33vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_I33V_START);
        vPrintNumber(uiData1, false);
        vSendStringNoCrc(S_CURRENT_MID);
        vPrintNumber(uiData2, false);
        vSendStringNoCrc(S_CURRENT_END);
        break;
    case PRINT_INDEX_I5V:
        uiData1 = uiI50v;
        uiData2 = uiI50vOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_I5V_START);
        vPrintNumber(uiData1, false);
        vSendStringNoCrc(S_CURRENT_MID);
        vPrintNumber(uiData2, false);
        vSendStringNoCrc(S_CURRENT_END);
        break;
    case PRINT_INDEX_VGAIN:
        uiData1 = uiVGain;
        uiData2 = uiVGainOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_VGAIN_START);
        vPrintNumber(uiData1, false);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, false);
        vSendStringNoCrc(S_VOLTAGE_END);
        break;
    case PRINT_INDEX_VGAIN_MIN:
        uiData1 = uiVGainMin;
        uiData2 = uiVGainMinOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_VGAIN_MIN_START);
        vPrintNumber(uiData1, false);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, false);
        vSendStringNoCrc(S_VOLTAGE_END);
        break;
    case PRINT_INDEX_VGAIN_MAX:
        uiData1 = uiVGainMax;
        uiData2 = uiVGainMaxOtherSide;
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }
        vSendStringNoCrc(S_VGAIN_MAX_START);
        vPrintNumber(uiData1, false);
        vSendStringNoCrc(S_VOLTAGE_MID);
        vPrintNumber(uiData2, false);
        vSendStringNoCrc(S_VOLTAGE_END);
        break;
    case PRINT_INDEX_TEMPERATURE:
        uiData1 = uiTemperature;
        uiData2 = uiTemperatureOtherSide;
        
        if (!bIsBody) {
            SWAP(uiData1, uiData2);
        }

        vSendStringNoCrc(S_TEMPERATURE_START);
        vPrintNumber(uiData1, false);
        vSendStringNoCrc(S_TEMPERATURE_MID);
        vPrintNumber(uiData2, false);
        vSendStringNoCrc(S_TEMPERATURE_END);
        break;
	case PRINT_INDEX_LLF:

			vSendStringNoCrc(S_LLF_START);

	        if(bIsBody && bLLFActive)
				vSendStringNoCrc(S_LLF_ACTIVE);
	        else if(bIsBody && !bLLFActive)
				vSendStringNoCrc(S_LLF_DEACTIVE);
			else 
			{
				if(bLLFActiveOtherSide)
					vSendStringNoCrc(S_LLF_ACTIVE);
				else
					vSendStringNoCrc(S_LLF_DEACTIVE);
			}
	   
	        vSendStringNoCrc(S_LLF_MID);
			
	        if(!bIsBody && bLLFActive)
				vSendStringNoCrc(S_LLF_ACTIVE);
	        else if(!bIsBody && !bLLFActive)
				vSendStringNoCrc(S_LLF_DEACTIVE);
			else
			{
				if(bLLFActiveOtherSide)
					vSendStringNoCrc(S_LLF_ACTIVE);
				else
					vSendStringNoCrc(S_LLF_DEACTIVE);
			}
			
	        vSendStringNoCrc(S_LLF_END);
		break;
	case PRINT_INDEX_DUPLEX:
		vSendStringNoCrc(S_DUPLEX_START);

		if(bIsBody)
		{
			if(PHY0_DUPLEX_EEPROM==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
				
			vSendStringNoCrc(S_DUPLEX_PHY1);
			
			if(PHY1_DUPLEX_EEPROM==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
		}
		else
		{
			if(uiVPHYODupplestherSide==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
			
			vSendStringNoCrc(S_DUPLEX_PHY1);
			
			if(uiVPHYODupplestherSide==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
		}
			
		vSendStringNoCrc(S_LLF_MID);
		
		if(!bIsBody)
		{
			if(PHY0_DUPLEX_EEPROM==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
			
			vSendStringNoCrc(S_DUPLEX_PHY1);
			
			if(PHY0_DUPLEX_EEPROM==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
		}
		else
		{
			if(uiVPHYODupplestherSide==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
			
			vSendStringNoCrc(S_DUPLEX_PHY1);
			
			if(uiVPHYODupplestherSide==0x0081)
				vSendStringNoCrc(S_DUPLEX_HALF);
			else
				vSendStringNoCrc(S_DUPLEX_FULL);
		}

				
		vSendStringNoCrc(S_DUPLEX_END);
	break;
    case PRINT_INDEX_CONNECTEDTO:
        vSendStringNoCrc(S_CONNECTEDTO_START);
        if (bIsBody) {
            vSendStringNoCrc(S_CONNECTEDTO_BODY);
        } else {
            vSendStringNoCrc(S_CONNECTEDTO_HOLLOWSHAFT);
        }
        vSendStringNoCrc(S_CONNECTEDTO_END);
        break;
    }
    vEnableSendBuffer();
}

