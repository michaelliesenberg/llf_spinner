/**
 * \file
 *
 * \brief Firmware f�r Fast Ethernet IRT
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * 20180112:
 * HEY
 * Umsetzung von BASCOM (A99080) in C unter Benutzung der ASF bibliothek
 * �nderungen im Vergleich zum urspr�nglichen Code:
 * -5V: wird jetzt �ber den ADC(7) gelesen und mit einem Schwellwert (~2V) verglichen.
 *      bisher wurde der Pin als digitaler Eingang verwendet.
 * DIAG_TX: im Betrieb ist DIAG_TX jetzt LOW, und wird erst f�r die Kommunkikation freigeschaltet
 * N_DIAG_TX wird jetzt aus DIAG_TX �ber die XMEGA Custom Logic (XCL) generiert
 *           dadurch ist das Sendesignal gr��er
 * +3V3: jetzt wird die +3V3 Analogspannung �ber ADC1 eingelesen.
 *       bisher wurde die Spannung �ber den ADC0 gelesen. Dies hat zum schalten des
 *       Analogkomperators gef�hrt. Dadurch konnte der USART das darauffolgende Zeichen nicht richtig einlesen.
 *       Durch Verwendung von ADC1 wird der Effekt minimiert.

 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>
#include <stdio.h>
#include <util/atomic.h>

#include "main.h"
#include "phy.h"
#include "led.h"
#include "externalclock.h"
#include "serialport1_PC.h"
#include "serialport2_PLD.h"
#include "boardAdc.h"
#include "gluelogic.h"
#include "fuses.h"
#include "nvmManagement.h"
#include "diagnosticData.h"
#include "protokollparser.h"


#define MAIN_TIME_GAIN                  5
#define MAIN_TIME_TOGGLE_FAST           142

#define MAIN_POSITION_ENTERDIAGNOSTIC   0
#define MAIN_POSITION_SENDTOOTHERSIEDE  600
#define MAIN_POSITION_ADC               150
#define MAIN_POSITION_END               999

uint16_t uiOthersideReceivedCounterOld;
uint16_t uiMsMainLoopCounter = 0;

bool bInitialized = false;
bool bClockOk = false;
bool bPhyOk = false;
bool bNvmContainsData = false;
bool bNvmWriteDefaultData = false;
bool bLLFOk=false;
extern uint8_t LLF_EEPROM = 0x7E;//Default is not defined
extern uint8_t is_FPGA_Board=0;
extern uint16_t PHY0_DUPLEX_EEPROM = 0x0000;//Default is not defined
extern uint16_t PHY1_DUPLEX_EEPROM = 0x0000;//Default is not defined
void mainInit(void);
void mainLoop(void);


void mainInit(void) 
{

    sysclk_init();
    sleepmgr_init();

    irq_initialize_vectors();
    vBoardInit();
    vUART1Init();//Setup UART for PC
    vUART2Init();//Setup UART for PLD
    irq_initialize_vectors();
    cpu_irq_enable();
    wdt_set_timeout_period(WDT_TIMEOUT_PERIOD_4KCLK);

    bInitialized = false;
    bClockOk = false;
    bPhyOk = false;
	bLLFOk=false;

    _delay_ms(10);
    ioport_set_pin_level(N_RESET_PHY, 0);
    _delay_ms(100);
    ioport_set_pin_level(LED, LED_ON);

    bNvmContainsData = bNvmConfigCrcIsOk();

    if(bNvmConfigIsEmpty() && !bNvmContainsData) 
	{
		bNvmWriteDefaultData=bNvmWriteDefaultConfig();
		bNvmContainsData = bNvmConfigCrcIsOk();
    }
	
	if(bNvmConfigIsEmpty() && !bNvmContainsData)
	{
		 vLedBlink3();
		return;
	}

    while (!bInitialized) 
	{

        if (!bClockOk) 
		{
            bClockOk = bExternalClockChipSetup();
        }
        if (bClockOk) 
		{
            _delay_ms(2000);
            ioport_set_pin_level(N_RESET_PHY, 1);
            _delay_ms(100);
        }

        if (!bPhyOk) 
		{
            bPhyOk = bInitializePhy();
        }
		
		if(!bLLFOk)
		{
			bLLFOk=bNvmGetLLF();
		}

        if (bPhyOk && bClockOk && bLLFOk) 
		{
            bInitialized = true;
            vLedBlink3();
        }
    }
}


void mainLoop(void) {
    uint16_t uiMsMainLoopCounter = 0;
    uint16_t uiSecondaryCounter = 0;
    uint8_t ucDiagTxCounter = 0;
    uint8_t ucAdcIndex = 0;

    ioport_set_pin_level(N_RESET_CPLD, 1);
    wdt_enable();

    while(true) 
	{
        if(tc45_is_cc_interrupt (&TCC4, TC45_CCA)) //Timer Interrupt
		{
            tc45_clear_cc_interrupt(&TCC4, TC45_CCA);

            if ((uiMsMainLoopCounter % MAIN_TIME_GAIN) == 0) 
			{
                vReadAdc(ADC_INDEX_GAIN);
                vScanCommandLine();
            }

            if ((uiMsMainLoopCounter % MAIN_TIME_TOGGLE_FAST) == 0) 
			{
                if (uiMsMainLoopCounter == 0) 
				{
                    if (bFlagDiagMode || bConfigurationMode) 
					{
                        ioport_toggle_pin_level(LED);
                    }
                 } 
				 else 
				 {
                    if (!bInitialized) 
					{
                        ioport_toggle_pin_level(LED);
                    }
                }
            }

            if (uiMsMainLoopCounter == MAIN_POSITION_ENTERDIAGNOSTIC && (bFlagEnableDiagMode || bFlagEnableConfigurationMode || bFlagEnableOtherSideConfigurationMode)) 
			{
                ioport_set_pin_level(N_RESET_CPLD, 0);
                ioport_set_pin_level(N_RESET_PHY, 0);
                vDiagEnableDiagMode();
            }
            if (uiMsMainLoopCounter > MAIN_POSITION_ADC && ucAdcIndex < ADC_INDEX_END) 
			{
                if (ucAdcIndex != ADC_INDEX_GAIN) 
				{
                    vReadAdc(ucAdcIndex);
                }
                ucAdcIndex++;
            }
            if (uiMsMainLoopCounter == MAIN_POSITION_SENDTOOTHERSIEDE) 
			{
				bPhyCheckStatus();//Get Status connection
                vSendDiagnosticToOtherSide();
            }


            if (uiMsMainLoopCounter == MAIN_POSITION_END) 
			{
                uiMsMainLoopCounter = 0;
                ucAdcIndex = 0;

                if (bFlagDiagMode) 
				{
					bFlagDiagMode=false; //TODO FIXME
                    for(ucDiagTxCounter = 0; ucDiagTxCounter < PRINT_INDEX_END; ucDiagTxCounter++) 
					{
                        vDiagPrintText(ucDiagTxCounter);
                    }
                }
                if (uiSecondaryCounter == 3) 
				{
                    uiSecondaryCounter = 0;
                    bool bResetOtherSideValues;
                    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) 
					{
                        bResetOtherSideValues = (ucOtherSideReceivedCounter == uiOthersideReceivedCounterOld);
                    }
                    if (bResetOtherSideValues)
					{
                        uiV50vOtherSide = 0;
                        uiV33vOtherSide = 0;
                        uiV18vOtherSide = 0;
                        uiV25vOtherSide = 0;
                        uiI50vOtherSide = 0;
                        uiI33vOtherSide = 0;
                        uiVGainOtherSide = 0;
                        uiVGainMinOtherSide = 0;
                        uiVGainMaxOtherSide = 0;
                        iMV50vOtherSide = 0;
                        uiTemperatureOtherSide = 0;
						bLLFPHY0OtherSide = 0;
						bLLFPHY1OtherSide = 0;
                    }
                    uiSecondaryCounter = 0;
                    uiOthersideReceivedCounterOld = ucOtherSideReceivedCounter;

                } 
				else 
				{
                    uiSecondaryCounter++;
                }

                if (ucEnableMinMax < 3) 
				{
                    ucEnableMinMax++;
                }
                wdt_reset()
            } 
			else 
			{
                ATOMIC_BLOCK(ATOMIC_RESTORESTATE) 
				{
                    uiMsMainLoopCounter++;
                }
            }
        }
    }
}

int main (void)
{
    mainInit();
    mainLoop();
}
