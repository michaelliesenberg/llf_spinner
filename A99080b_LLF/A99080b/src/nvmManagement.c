
#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <string.h>
#include <stdio.h>

#include "nvmManagement.h"
#include "crcCalc.h"
#include "serialport1_PC.h"
#include "main.h"

bool bCrcVerifyEEPROM(eeprom_addr_t uiIndex);

bool bCrcVerifyEEPROM(eeprom_addr_t uiIndex) 
{
    uint16_t uiCalculatedCrc;
    uint16_t uiStoredCrc;
    
    uiStoredCrc = 0;
    uiStoredCrc |= nvm_eeprom_read_byte(uiIndex + 1);
    uiStoredCrc <<= 8;
    uiStoredCrc |= nvm_eeprom_read_byte(uiIndex + 2);
    
    uiCalculatedCrc = uiCalculateEepromCrc(uiIndex);
    if (uiCalculatedCrc == uiStoredCrc) 
	{
        return true;
    }
    return false;
}

bool bNvmConfigIsEmpty(void) 
{
    eeprom_addr_t uiIndex;    

    for(uiIndex = NVM_CONFIG_START; uiIndex < NVM_CONFIG_START + NVM_BLOCK_SIZE; uiIndex++) 
	{
		uint8_t ucByte;
        ucByte = nvm_eeprom_read_byte(uiIndex);
        if (ucByte != 0xFF) 
		{
            return false;
        }
    }
    return true;
}


bool bNvmHasConfigForPhy(uint8_t ucPhy) 
{
    eeprom_addr_t uiIndex;
    uint8_t ucByte;
    
    if (!bNvmConfigCrcIsOk()) 
	{
        return false;
    }
    
    for(uiIndex = NVM_CONFIG_START; uiIndex < (NVM_CONFIG_END + 1); uiIndex += NVM_BLOCK_SIZE) 
	{
        ucByte = nvm_eeprom_read_byte(uiIndex);
        if (ucByte == ucPhy) 
		{
            return true;
        }
        if (ucByte == NVM_ID_END) 
		{
            break;
        }
    }
    return false;    
}

bool bNvmConfigCrcIsOk(void) 
{
    eeprom_addr_t uiIndex;
    uint8_t ucByte;
    
    for(uiIndex = NVM_CONFIG_START; uiIndex < (NVM_CONFIG_END); uiIndex += NVM_BLOCK_SIZE) 
	{
        ucByte = nvm_eeprom_read_byte(uiIndex);
        if (ucByte != NVM_ID_PHY0 && 
            ucByte != NVM_ID_PHY1 &&
			ucByte != NVM_ID_LLF &&
            ucByte != NVM_ID_END) 
		{
			return (false);
        }
        if (ucByte == NVM_ID_END) 
		{
            break;
        }
    }
    return bCrcVerifyEEPROM(uiIndex);
}

void vGetNvmConfig(struct stConfigData *stStructureConfigData, uint8_t ucIndex)
{
    uint16_t uiValue;    
    uint8_t ucEepromAddress;
    ucEepromAddress = NVM_CONFIG_START + ucIndex * NVM_BLOCK_SIZE;
    stStructureConfigData->ucChip = nvm_eeprom_read_byte(ucEepromAddress + NVM_BLOCK_CHIP_ID);
    uiValue = nvm_eeprom_read_byte(ucEepromAddress + NVM_BLOCK_ADDRESS) << 8;
    uiValue |= nvm_eeprom_read_byte(ucEepromAddress + NVM_BLOCK_ADDRESS + 1);
    stStructureConfigData->uiAddress = uiValue;    
    uiValue = nvm_eeprom_read_byte(ucEepromAddress + NVM_BLOCK_DATA) << 8;
    uiValue |= nvm_eeprom_read_byte(ucEepromAddress + NVM_BLOCK_DATA + 1);
    stStructureConfigData->uiData = uiValue;    
}

bool bWriteConfigToNvm(char * pcBuffer, uint8_t ucSize, bool isString) 
{
    eeprom_addr_t uiEepromAddress = NVM_CONFIG_START;
    uint8_t ucIndex;
    uint8_t ucValue;
    uint16_t uiChecksum;
    
    if (!bWaitForCrcFree()) 
	{
        return false;
    }        
    crc_set_initial_value(0xFFFF);
    crc_io_checksum_byte_start(CRC_16BIT);
	
    for(ucIndex = 0; ucIndex < ucSize; ucIndex += 2) 
	{
		if(isString)
			ucValue = uiHexStringToDec(pcBuffer + ucIndex, 2);
		else
		{
			ucValue = *pcBuffer;
			pcBuffer++;
		}
			
        nvm_eeprom_write_byte(uiEepromAddress, ucValue);
        uiEepromAddress++;
        crc_io_checksum_byte_add(ucValue);
    }
    uiChecksum = (uint16_t) crc_io_checksum_byte_stop();
    vReleaseCrc();
    nvm_eeprom_write_byte(uiEepromAddress, NVM_ID_END);
    uiEepromAddress++;
    nvm_eeprom_write_byte(uiEepromAddress, uiChecksum >> 8);
    uiEepromAddress++;
    nvm_eeprom_write_byte(uiEepromAddress, uiChecksum & 0x00FF);
    return true;
}
bool bNvmWriteDefaultConfig(void)
{
	for(int i=NVM_CONFIG_START ; i<NVM_CONFIG_END; i++)//ERASE EEPROM
	{
		nvm_eeprom_write_byte(i, 0x00);
	}
	/*char buf1[100]={NVM_ID_LLF,0x00,0x00,0x00,0x44,
					NVM_ID_PHY0,0x00,0x04,0x01,0x01,
					NVM_ID_PHY1,0x00,0x04,0x01,0x01,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00,
					0x00,0x00,0x00,0x00,0x00}; 

	bWriteConfigToNvm(buf1,200,false);*/
}

bool bNvmGetLLF(void)
{
	eeprom_addr_t uiIndex;
	uint8_t ucByte;
	bool ret=false;
	
    for(uiIndex = NVM_CONFIG_START; uiIndex < (NVM_CONFIG_END + 1); uiIndex += NVM_BLOCK_SIZE)
    {
	    ucByte = nvm_eeprom_read_byte(uiIndex);
	    if (ucByte == NVM_ID_LLF)
	    {
			LLF_EEPROM = nvm_eeprom_read_byte(uiIndex+4);
		    ret = true;
			//return ret;
	    }
    }
	if(ret == false)
	{
		LLF_EEPROM = 0x7F;
		ret = true;	
	}
	return ret;
}