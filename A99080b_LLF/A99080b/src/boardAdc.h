

#ifndef ADC_H_
#define ADC_H_

extern uint16_t uiV50v;
extern uint16_t uiV33v;
extern uint16_t uiV18v;
extern uint16_t uiV25v;
extern uint16_t uiI50v;
extern uint16_t uiI33v;
extern uint16_t uiTemperature;
extern uint16_t uiVGain;
extern uint16_t uiVGainMin;
extern uint16_t uiVGainMax;
extern int16_t iMV50v;

uint8_t ucEnableMinMax;

void vAdcInit(void);
void vReadAdc(uint8_t ucIndex);

#endif /* ADC_H_ */