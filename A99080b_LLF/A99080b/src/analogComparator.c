

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <string.h>
#include "main.h"

#include "analogComparator.h"


void vAnalogComparatorInit(void) {
    // ---------------------------------------------------------------
    // config ACA
    // ---------------------------------------------------------------
    // setup comparator pin 0 and pin 1 are the input of portA.
    
    struct ac_config aca_config;
    
           

    memset(&aca_config, 0, sizeof(struct ac_config));

    ac_set_mode(&aca_config, AC_MODE);
    ac_set_voltage_scaler(&aca_config, AC_SCALE);
    ac_set_hysteresis(&aca_config, AC_HYSTERESIS);
    
    if(is_FPGA_Board==0)
    {
        ac_set_positive_reference(&aca_config, AC_POS_INPUT);
        ac_set_negative_reference(&aca_config, AC_NEG_INPUT);
    }
    else
    {
        ac_set_negative_reference(&aca_config, AC_MUXNEG_PIN0_gc);
        ac_set_positive_reference(&aca_config, AC_MUXPOS_PIN1_gc);
    }

    ac_set_interrupt_mode(&aca_config, AC_INTERRUPT_MODE);
    ac_set_interrupt_level(&aca_config, AC_INTERRUPT_LEVEL);

    ac_write_config(&ACA, 0, &aca_config);

    ac_enable(&ACA, 0);
    
    // enable output of analog compartor (to Port R1)
    PORTCFG.ACEVOUT = PORTCFG_ACOUT_PR_gc;
    ACA.CTRLA = 1;
    ioport_set_pin_dir(AC_OUTPUT_PIN, IOPORT_DIR_OUTPUT);
}