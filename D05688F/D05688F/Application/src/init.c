/**
 * \file
 *
 * \brief User board initialization template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <tc45.h>
#include <twi_master.h>
#include <string.h>
#include "main.h"
#include "led.h"
#include "boardAdc.h"
#include "gluelogic.h"
#include "analogComparator.h"
#include "externalclock.h"

void vTimerInit(void);

void vTimerInit(void) {
    // config TCC4 as 1ms MDIO clk timer and main loop timer
    tc45_enable(&TCC4);
    tc45_set_wgm(&TCC4, TC45_WG_NORMAL);
    tc45_write_period(&TCC4, TIMER_4_PERIOD);
    tc45_write_clock_source(&TCC4, TC_CLKSEL_DIV64_gc);
    // TCC5 not used
}    



void vBoardInit(void) {
	// IO-Ports
	ioport_init();

	ioport_set_pin_dir(TEST_M5V, IOPORT_DIR_INPUT);

    ioport_set_pin_dir(N_RESET_PHY, IOPORT_DIR_OUTPUT);
    
    vAdcInit();
    
	if(is_FPGA_Board)
	{
		ioport_set_pin_level(N_RESET_PHY, 0);
	    ioport_set_pin_dir(N_FPGA_RESET, IOPORT_DIR_OUTPUT);
    	ioport_set_pin_level(N_FPGA_RESET, 0);
	}
	else
	{
		ioport_set_pin_level(N_RESET_PHY, 1);
		ioport_set_pin_dir(N_RESET_CPLD, IOPORT_DIR_OUTPUT);
    	ioport_set_pin_level(N_RESET_CPLD, 0);
	}

	ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(MDIO_DATA, IOPORT_MODE_PULLUP);
	
    ioport_set_pin_dir(MDIO_CLK, IOPORT_DIR_OUTPUT);

    ioport_set_pin_dir(DIAG_OUT, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(DIAG_OUT_INV, IOPORT_DIR_OUTPUT);

    ioport_set_pin_dir(BODY_SELECT, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(BODY_SELECT, IOPORT_MODE_PULLUP);

    vLedInit();
    
    vAnalogComparatorInit();
    
    vTimerInit();
    
	if(is_FPGA_Board==0)
    	vClockChipBusInit();
       
    vGlueLogicInit();
}
