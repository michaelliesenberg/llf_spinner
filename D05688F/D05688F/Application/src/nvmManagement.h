

#ifndef NVM_MANAGEMENT_H_
#define NVM_MANAGEMENT_H_

#define NVM_BLOCK_SIZE          5
#define NVM_BLOCK_MAXENTRIES    20
#define NVM_BLOCK_CHIP_ID       0
#define NVM_BLOCK_ADDRESS       1
#define NVM_BLOCK_DATA          3

bool bWrite_To_Nvm(uint16_t address, uint8_t value);
bool bNvmConfigIsEmpty(void);
bool bWriteConfigToNvm(void) ;
#endif /* NVM_MANAGEMENT_H_ */

