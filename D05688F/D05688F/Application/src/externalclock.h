/*
 * clock.h
 *
 * Created: 04.01.2018 07:58:42
 *  Author: Eyermann
 */ 


#ifndef CLOCK_H_
#define CLOCK_H_


bool bExternalClockChipSetup(void);
void vClockChipBusInit(void);

#endif /* CLOCK_H_ */


