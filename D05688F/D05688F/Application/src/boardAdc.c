

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <limits.h>
#include <stdio.h>
#include <util/delay.h>
#include "main.h"
#include "boardAdc.h"
#include "nvmManagement.h"
#include "serialport1_PC.h"
struct adc_config adc_conf;
struct adc_channel_config adcch_conf;

uint8_t ucEnableMinMax = 0;

float fTempKPerBit;
float fTempYInt;
float fTempHot;
float fTempRoom;

int16_t iGetAdcValue(enum adcch_positive_input pos, enum adcch_negative_input neg);
int16_t iReadTemperature(void);


void vAdcInit(void)
{
	irt_data.GAIN_MIN = USHRT_MAX;
    adc_read_configuration(&ADC, &adc_conf);
    adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_VCC);
    adc_set_conversion_trigger(&adc_conf, ADC_TRIG_EVENT_SINGLE, 1, 0);
    adc_set_clock_rate(&adc_conf, ADC_CLOCK_RATE);    
    adc_write_configuration(&ADC, &adc_conf);

    adcch_read_configuration(&ADC, ADC_CH, &adcch_conf);
    adcch_set_input(&adcch_conf, ADCCH_POS_PIN0, ADCCH_NEG_NONE, 1);
    adcch_set_interrupt_mode(&adcch_conf, ADCCH_MODE_COMPLETE);
    adcch_disable_interrupt(&adcch_conf);
    adcch_write_configuration(&ADC, ADC_CH, &adcch_conf);    
    adc_enable(&ADC);
    
    uint16_t uiHotTemperature;
    uint16_t uiRoomTemperature;
    uint16_t uiAdcHot;
    uint16_t uiAdcRoom;
    
    uiHotTemperature = adc_get_calibration_data(ADC_CAL_HOTTEMP);
    uiRoomTemperature= adc_get_calibration_data(ADC_CAL_ROOMTEMP);
    uiAdcHot = adc_get_calibration_data(ADC_CAL_TEMPSENSE);
    uiAdcRoom = adc_get_calibration_data(ADC_CAL_TEMPSENSE2);
    
    fTempKPerBit = ((float)uiHotTemperature - (float)uiRoomTemperature) / ((float)uiAdcHot - (float)uiAdcRoom);
    fTempYInt = (float)uiHotTemperature - ((float)uiAdcHot * fTempKPerBit);
    
    vReadAdc(ADC_INDEX_1V8_1V1);
    
    if(irt_data.VALUE_1V1_1V8 >= 1500)//Bigger than 1500mv
	{
        is_FPGA_Board=0;//Not FPGA Board
		_delay_ms(100);
		float lResult = iGetAdcValue(ADC_MUX_3V3_TLK, ADC_NEG_GND);
		irt_data.VALUE_3V3 = (lResult * 3906) / 20480;
		adc_disable_internal_input(&adcch_conf, ADC_MUX_3V3_TLK);
	}
    else
        is_FPGA_Board=1;//Is FPGA Board
    
	bWrite_To_Nvm(NVM_BootLoader_BOARD_Type, is_FPGA_Board);//Tell Bootloader board type
	bWrite_To_Nvm(NVM_BootLoader_UART, 0);//Tell Bootloader update UART from PC
	bWrite_To_Nvm(NVM_BootLoader_ENTRY_CODE, 0xFF);
}


int16_t iGetAdcValue(enum adcch_positive_input pos, enum adcch_negative_input neg) {
    int16_t iResult;

    adcch_set_input(&adcch_conf, pos, neg, 1);
    adcch_write_configuration(&ADC, ADC_CH, &adcch_conf);    
    adc_start_conversion(&ADC, ADC_CH);
    adc_wait_for_interrupt_flag(&ADC, ADC_CH);

    iResult = adc_get_signed_result(&ADC, ADC_CH);    
    return (iResult);
    
}


int16_t iReadTemperature(void) {
    int16_t iTemperature;
    adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_BANDGAP);
    
    adc_enable_internal_input(&adc_conf, ADC_INT_TEMPSENSE);    
    adc_write_configuration(&ADC, &adc_conf); 
    adcch_set_input(&adcch_conf, ADCCH_POS_TEMPSENSE, ADCCH_NEG_NONE, 1);
    adcch_write_configuration(&ADC, ADC_CH0, &adcch_conf);
    adc_start_conversion(&ADC, ADC_CH);
    adc_wait_for_interrupt_flag(&ADC, ADC_CH);
    
    iTemperature = adc_get_signed_result(&ADC, ADC_CH0); 
    
    adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_VCC);
    adc_disable_internal_input(&adc_conf, ADC_INT_TEMPSENSE);
    adc_write_configuration(&ADC, &adc_conf);   
    
    return (iTemperature);
}



void vReadAdc(uint8_t ucIndex) 
{   
    volatile int32_t lResult;   
	
    switch (ucIndex)
    {
    case ADC_INDEX_5V:
        lResult = iGetAdcValue(ADC_MUX_5V0, ADC_NEG_GND);
        irt_data.VALUE_5V = (lResult * 625) / 2048;
    	break;
    case ADC_INDEX_3V3:
        
		if(is_FPGA_Board)
		{
			lResult = iGetAdcValue(ADC_MUX_3V3, ADC_NEG_GND);
        	irt_data.VALUE_3V3 = (lResult * 625) / 2048;
		}
		//TLK only measures 3V3 on init
        break;
    case ADC_INDEX_1V8_1V1:
        lResult = iGetAdcValue(ADC_MUX_1V8_1V1, ADC_NEG_GND);
        irt_data.VALUE_1V1_1V8 = (lResult * 3125) / 2048;
        break;
    case ADC_INDEX_2V5:
        lResult = iGetAdcValue(ADC_MUX_2V5, ADC_NEG_GND);
        irt_data.VALUE_2V5 = (lResult * 3125) / 2048;
        break;
    case ADC_INDEX_I5V:
        lResult = iGetAdcValue(ADC_MUX_I5V0, ADC_NEG_GND);
		if(is_FPGA_Board)
			irt_data.VALUE_I_5V = (uint16_t)(((float)(lResult - 748) / 320) * 100);
		else
        	irt_data.VALUE_I_5V = (lResult * 781) / 2048;
        break;
    case ADC_INDEX_I3V3:
        lResult = iGetAdcValue(ADC_MUX_I3V3, ADC_NEG_GND);
		if(is_FPGA_Board)
			irt_data.VALUE_I_3V3 = (uint16_t)(((float)(lResult - 472) / 315) * 100);
		else
        	irt_data.VALUE_I_3V3 = (lResult * 781) / 2048;
        break;
    case ADC_INDEX_TEMPERATURE:        
        lResult = iReadTemperature();
        float fTemp;
        fTemp = ((float) lResult * fTempKPerBit) + fTempYInt;		
		irt_data.TEMPERATURE = (uint16_t) fTemp;
        break;    
    case ADC_INDEX_M5V0:
        lResult = iGetAdcValue(ADC_MUX_N5V0, ADC_NEG_GND);
        // in 10mV
        lResult = (lResult * 156) / 2048;                
        // for 10k / 15k Version: 
        //iMV50v = (5* lResult - 3 * uiV50v) / 2;
        // for 15k / 15k Version:
        //iMV50v = 2* lResult - uiV50v;
		
		irt_data.VALUE_n_5V =2* lResult - irt_data.VALUE_5V;
        break;
    case ADC_INDEX_GAIN:    
	
        lResult = iGetAdcValue(ADC_MUX_VGAIN, ADC_NEG_GND);
		irt_data.GAIN = (uint16_t) ((lResult * 1563) / 2048);
    
        if (ucEnableMinMax > 2) 
		{
            if (irt_data.GAIN_MIN > irt_data.GAIN) 
                irt_data.GAIN_MIN = irt_data.GAIN;
            if (irt_data.GAIN_MAX < irt_data.GAIN) 
                irt_data.GAIN_MAX = irt_data.GAIN;
        }
        break;
    }
}