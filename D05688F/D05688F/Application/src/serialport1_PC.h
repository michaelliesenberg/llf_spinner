/*
 * serialport_1.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#ifndef serialport_1_h
#define serialport_1_h

#include "nvmManagement.h"
#include "boardAdc.h"

// buffer size: Command(2) + Length(2) + 20 * Phy[0/1](10) + 4 bytes for internal communication
#define BUFFER_SIZE   (2 + 2 + 2 * NVM_BLOCK_SIZE * NVM_BLOCK_MAXENTRIES + 4)

#define INPUT_BUFFER_SIZE   BUFFER_SIZE
#define SEND_BUFFER_SIZE    BUFFER_SIZE


//#define SWAP(x, y) do { typeof(x) SWAP = x; x = y; y = SWAP; } while (0)


#define CMD_EXIT_BOOTLOADER     'E'
#define REPLY_ACK               '\r'


extern bool bErrorReceivingStruct;
extern bool bCommandHeader;
extern bool bCommandBody;
extern bool bCommandUpdate_Otherside;
extern bool bCommandSend_Bootloader_Otherside;
extern bool bCommandReceive_Bootloader_Otherside;
extern char cCommandBuffer[];
extern volatile uint8_t ucBytesInCommandBuffer;
extern uint8_t ucReceiverIndex;
extern char cSendBuffer[];

void vUART1Init(void);

typedef enum 
{
	FULL='F',
	HALF='H'
}DUPPLEX;
typedef enum 
{
	ACTIVE='A',
	DEACTIVE='D'
}ENABLE_TYPE;

typedef enum
{
	RECEIVE_HEADER='H',
	RECEIVE_BODY='B'
}RECEIVE_TYPE;

typedef enum
{
	START_UPDATE ='U',
	START_UPDATE_OTHERSIDE ='O',
	START_UPDATE_OTHERSIDE_ACKNOWLEDGE ='A',
	GET_BODY ='B',
	SEND_BODY ='S',
	SET_CONFIG ='C',
	RESET_IRT = 'R',
	NORMAL_OPERATION = 'N',
	START_UPDATE_MAKE_BRIDGE = 'M',
	IN_BOOTLOADER = 'X'
}COMMAND_TYPE;

typedef enum
{
	BAD_STRUCT ='B',
	OK_STRUCT ='O',
	OtherSide_In_Bootloader ='L'
}RESPONSE_TYPE;
typedef enum
{
	FPGA ='F',
	TLK ='T',
}BOARD_TYPE;
typedef enum
{
	BODY ='B',
	HOLLOW ='H',
}IRT_TYPE;
typedef struct
{
	COMMAND_TYPE COMMAND		:8;
	RESPONSE_TYPE RESPONSE		:8;
	uint16_t VERSION			:16;

	uint16_t SIZEBYTES	:16;
	uint16_t CRC_CHECK	:16;
} HEADER;

typedef struct
{
	int16_t VALUE_1V1_1V8	:16;
	int16_t VALUE_3V3		:16;
	
	int16_t VALUE_5V		:16;
	int16_t VALUE_n_5V		:16;
	
	int16_t VALUE_2V5		:16;
	int16_t VALUE_I_3V3		:16;
	
	int16_t VALUE_I_5V			:16;
	uint8_t UPDATE_OTHERSIDE	:8;
	uint8_t						:8;
	
	uint16_t GAIN			:16;
	uint16_t GAIN_MIN		:16;
	
	uint16_t GAIN_MAX		:16;
	uint16_t TEMPERATURE	:16;
	
	DUPPLEX PHY0_DUPPLEX	:8;
	DUPPLEX PHY1_DUPPLEX	:8;
	ENABLE_TYPE LLF_PHY0	:8;
	ENABLE_TYPE LLF_PHY1	:8;
	
	uint8_t SW_RESET			:8;
	uint8_t NEW_CONFIGURATION	:8;
	uint8_t  LLF_PHY0_LINKUP	:8;
	uint8_t  LLF_PHY1_LINKUP	:8;
	
	uint8_t SW_VERSION_1		:8;
	uint8_t SW_VERSION_2		:8;
	uint8_t SW_VERSION_3		:8;
	uint8_t SW_VERSION_4		:8;
	
	uint8_t SW_VERSION_5		:8;
	uint8_t SW_VERSION_6		:8;
	uint8_t SW_VERSION_7		:8;
	uint8_t SW_VERSION_8		:8;
} IRT_DATA;


typedef struct
{
	uint8_t irt_status		:8;
	BOARD_TYPE board_type	:8;
	IRT_TYPE irt_type		:8;
	uint8_t 				:8;
	
	IRT_DATA BODY;
	IRT_DATA HOLLOW;
	
	uint8_t 				:8;
	uint8_t 				:8;
	uint16_t CRCSUM			:16;
} IRT_BODY;

typedef struct
{
	DUPPLEX PHY0_DUPPLEX		:8;
	DUPPLEX PHY1_DUPPLEX		:8;
	ENABLE_TYPE LLF_PHY0		:8;
	ENABLE_TYPE LLF_PHY1		:8;
	
} SETTINGS;
	
	
typedef struct
{
	SETTINGS HOLLOW;
	SETTINGS BODY;
	
	uint8_t 				:8;
	uint8_t 				:8;
	uint16_t CRC_Checksum	:16;
} PC_BODY;   
	   
HEADER IRT_header;
HEADER PC_header;
IRT_BODY IRT_body;	
PC_BODY PC_body;	
PC_BODY PC_body_UART;	

IRT_DATA irt_data;
extern void USART_1_ISR_HANDLER(void);

#endif