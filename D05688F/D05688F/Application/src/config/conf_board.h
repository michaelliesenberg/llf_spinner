/**
 * \file
 *
 * \brief User board configuration template
 *
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H

#define LINE_ENDING             "\n"

#define F_CPU                   8000000UL


// IO Ports
#define TEST_M5V        IOPORT_CREATE_PIN(PORTA, 7)
#define N_RESET_PHY     IOPORT_CREATE_PIN(PORTD, 6)
#define N_FPGA_RESET    IOPORT_CREATE_PIN(PORTC, 0)
#define N_RESET_CPLD    IOPORT_CREATE_PIN(PORTD, 7)
#define LED             IOPORT_CREATE_PIN(PORTR, 0)
#define MDIO_DATA       IOPORT_CREATE_PIN(PORTD, 1)
#define MDIO_CLK        IOPORT_CREATE_PIN(PORTD, 4)

#define DIAG_OUT        IOPORT_CREATE_PIN(PORTC, 3)
#define DIAG_OUT_INV    IOPORT_CREATE_PIN(PORTC, 4)
#define BODY_SELECT     IOPORT_CREATE_PIN(PORTD, 5)

#define IS_HOLLOWSHAFT  0
#define IS_BODY         1


#define TIMER_4_PERIOD  125

// ADC Ports
#define ADC             ADCA
#define ADC_CH          ADC_CH0
#define ADC_GAIN        1
#define ADC_CLOCK_RATE  200000UL

#define ADC_MUX_3V3_TLK  ADCCH_POS_PIN0             
#define ADC_MUX_VGAIN    ADCCH_POS_PIN2
#define ADC_MUX_1V8_1V1  ADCCH_POS_PIN3
#define ADC_MUX_2V5      ADCCH_POS_PIN4
#define ADC_MUX_5V0      ADCCH_POS_PIN5
#define ADC_MUX_I3V3     ADCCH_POS_PIN6
#define ADC_MUX_N5V0     ADCCH_POS_PIN7
#define ADC_MUX_I5V0     ADCCH_POS_PIN8
#define ADC_MUX_3V3      ADCCH_POS_PIN15

#define ADC_NEG_GND      ADCCH_NEG_PAD_GND


// threshold for -5V ~ 2V on ADC7
#define ADC_N5V0_THRESHOLD  1850

#define ADC_INDEX_5V            0
#define ADC_INDEX_3V3           1
#define ADC_INDEX_1V8_1V1       2
#define ADC_INDEX_2V5           3
#define ADC_INDEX_I5V           4
#define ADC_INDEX_I3V3          5
#define ADC_INDEX_TEMPERATURE   6
#define ADC_INDEX_M5V0          7
#define ADC_INDEX_GAIN          8
#define ADC_INDEX_END           9

// AC

#define AC_MODE                 AC_MODE_SINGLE
#define AC_SCALE                1
#define AC_POS_INPUT            AC_MUXNEG_PIN0_gc
#define AC_NEG_INPUT            AC_MUXNEG_PIN1_gc
#define AC_HYSTERESIS           AC_HYSMODE_LARGE_gc
#define AC_INTERRUPT_MODE       AC_INT_MODE_BOTH_EDGES
#define AC_INTERRUPT_LEVEL      AC_INT_LVL_OFF
#define AC_OUTPUT_PIN           IOPORT_CREATE_PIN(PORTR, 1)

// USART

#define USART_1                     &USARTC0
#define USART_1_RX                  IOPORT_CREATE_PIN(PORTC, 2)
#define USART_1_TX                  IOPORT_CREATE_PIN(PORTC, 3)
#define USART_1_RX_ISR_HANDLER      USARTC0_RXC_vect
#define USART_1_DRE_ISR_HANDLER     USARTC0_DRE_vect
#define USART_1_BAUDRATE            9600
#define USART_1_CHAR_LENGTH         USART_CHSIZE_8BIT_gc
#define USART_1_PARITY              USART_PMODE_EVEN_gc//USART_PMODE_DISABLED_gc
#define USART_1_STOP_BIT            false


#define USART_2                     &USARTD0
#define USART_2_RX                  IOPORT_CREATE_PIN(PORTD, 2)
#define USART_2_TX                  IOPORT_CREATE_PIN(PORTD, 3)
#define USART_2_RX_ISR_HANDLER      USARTD0_RXC_vect
#define USART_2_DRE_ISR_HANDLER     USARTD0_DRE_vect
#define USART_2_BAUDRATE            9600
#define USART_2_CHAR_LENGTH         USART_CHSIZE_8BIT_gc
#define USART_2_PARITY              USART_PMODE_EVEN_gc//USART_PMODE_DISABLED_gc
#define USART_2_STOP_BIT            false


// TWI (I2C)
#define CDCE_PORT                   &TWIC
#define CDCE_SPEED                  100000
#define CDCE_PIN_SDA                IOPORT_CREATE_PIN(PORTC, 0)
#define CDCE_PIN_SCL                IOPORT_CREATE_PIN(PORTC, 1)
#define CDCE_ADDR                   0x65
#define CDCE_OFFSET_GENERIC         0x00
#define CDCE_OFFSET_PLL1            0x10
#define CDCE_GENERIC_01             (CDCE_OFFSET_GENERIC + 0x01)
#define CDCE_GENERIC_06             (CDCE_OFFSET_GENERIC + 0x06)

#define CDCE_GENERIC_06_EEWRITE         0x41        // from BASCOM code
#define CDCE_GENERIC_06_EEWRITE_RESET   0x40


// NVM (EEPROM)

#define NVM_CONFIG_START        EEPROM_PAGE_SIZE + 4
#define NVM_BootLoader_UART			0x0000
#define NVM_BootLoader_BOARD_Type	0x0001
#define NVM_BootLoader_ENTRY_CODE	0x0002
// size: 20 entries + End entry, including CRC
//#define NVM_CONFIG_SIZE         (5 * (20 + 1))
//#define NVM_CONFIG_END          (NVM_CONFIG_START + NVM_CONFIG_SIZE - 1)


#endif // CONF_BOARD_H
