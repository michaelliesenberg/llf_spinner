/************************************************************************/
/* XBoot Extensible AVR Bootloader                                      */
/*                                                                      */
/* UART Module                                                          */
/*                                                                      */
/* uart.h                                                               */
/*                                                                      */
/* Alex Forencich <alex@alexforencich.com>                              */
/*                                                                      */
/* Copyright (c) 2010 Alex Forencich                                    */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files(the "Software"), to deal in the Software without restriction,  */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef __UART_H
#define __UART_H

#include "xboot.h"

// Globals
enum sysclk_port_id {
	SYSCLK_PORT_GEN,   //!< Devices not associated with a specific port.
	SYSCLK_PORT_A,     //!< Devices on PORTA
	SYSCLK_PORT_B,     //!< Devices on PORTB
	SYSCLK_PORT_C,     //!< Devices on PORTC
	SYSCLK_PORT_D,     //!< Devices on PORTD
	SYSCLK_PORT_E,     //!< Devices on PORTE
	SYSCLK_PORT_F,     //!< Devices on PORTF
};
// Defines

// nonzero if character has been received
#define uart_char_received(b) ( (b) == 0 ?  (USARTC0.STATUS & USART_RXCIF_bm) : (USARTD0.STATUS & USART_RXCIF_bm))
// current character in UART receive buffer
#define uart_cur_char(b) ( (b) == 0 ? USARTC0.DATA : USARTD0.DATA)
// send character
#define uart_send_char(c, b) ( (b) == 0 ? (USARTC0.DATA = (c)) : (USARTD0.DATA = (c))) 
// send character, block until it is completely sent
#define uart_send_char_blocking(c,b)								\
	({																\
		if((b)==0)													\
		{															\
			do {													\
				uart_send_char(c,b);								\
				while (!(USARTC0.STATUS & USART_TXCIF_bm)) { }		\
				USARTC0.STATUS |= USART_TXCIF_bm;					\
			} while (0);											\
		}															\
		else														\
		{															\
			do {													\
				uart_send_char(c,b);								\
				while (!(USARTD0.STATUS & USART_TXCIF_bm)) { }		\
				USARTD0.STATUS |= USART_TXCIF_bm;					\
			} while (0);											\
		}															\
	})


// Prototypes
extern void uart_init(void);
extern void uart_deinit(void);
extern uint8_t is_FPGA_Board;
extern uint8_t UART_OtherSide;
#endif // __UART_H

