/************************************************************************/
/* XBoot Extensible AVR Bootloader                                      */
/*                                                                      */
/* UART Module                                                          */
/*                                                                      */
/* uart.c                                                               */
/*                                                                      */
/* Alex Forencich <alex@alexforencich.com>                              */
/*                                                                      */
/* Copyright (c) 2010 Alex Forencich                                    */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files(the "Software"), to deal in the Software without restriction,  */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "uart.h"

extern uint8_t is_FPGA_Board=0;
extern uint8_t UART_OtherSide=false;
#define SpinnerBoard
//#define Use_ADC
#ifdef SpinnerBoard
void vAnalogComparatorInit_Bootloader(void);
void vGlueLogicInit_Bootloader(void);
void adc_init();

struct ac_config {
	//! Comparator control register
	uint8_t         acctrl;
	//! Comparator mux control register
	uint8_t         acmuxctrl;
	//! Control register B register
	uint8_t         ctrlb;
	//! Window mode control register
	uint8_t         winctrl;
};
#endif

// Interrupts
#ifdef USE_INTERRUPTS
#ifdef USE_UART
ISR(UART_DEVICE_RXC_ISR)
{
        if (comm_mode == MODE_UNDEF)
        {
                comm_mode = MODE_UART;        
		}
        if (rx_char_cnt == 0)
        {
                rx_buff0 = UART_DEVICE.DATA;
                rx_char_cnt = 1;
        }
        else
        {
                rx_buff1 = UART_DEVICE.DATA;
                rx_char_cnt = 2;
        }
}




ISR(UART_DEVICE_TXC_ISR)
{
        tx_char_cnt = 0;
}
#endif // USE_UART
#endif // USE_INTERRUPTS

// Initialize UART
void uart_init(void)
{
	#ifdef Use_ADC
	//adc_init();
	#endif
	
	#ifdef SpinnerBoard

	//RESET PHY FPGA
	PORTD.DIRSET = (1 << 6);
	
	if(is_FPGA_Board==0)
	{
		PORTD.DIRSET = (1 << 7);
		PORTD.OUTCLR = (1 << 6);//Phy TLK Reset
		PORTD.OUTSET = (1 << 7);//TLK Enable
	}
	else
	{
		PORTC.DIRSET = (1 << 0);
		PORTD.OUTSET = (1 << 6);//Phy FPGA Reset
		PORTC.OUTSET = (1 << 0);//FPGA Enable
	}
	//LED
	PORTR.DIRSET = (1 << LED_PIN); 
	PORTR.OUTCLR = (1 << LED_PIN);
	
	if (UART_OtherSide)
    {
		//UARTD from otherside init
		PORTD.DIRSET = (1 << UART_TX_PIN);
		PORTD.DIRSET = (0 << UART_RX_PIN);
		USARTD0.BAUDCTRLA = (UART_BSEL_VALUE & USART_BSEL_gm);
		USARTD0.BAUDCTRLB = ((UART_BSCALE_VALUE << USART_BSCALE_gp) & USART_BSCALE_gm) | ((UART_BSEL_VALUE >> 8) & ~USART_BSCALE_gm);
		USARTD0.CTRLC |= (uint8_t)USART_CHSIZE_8BIT_gc | USART_PMODE_EVEN_gc | USART_CMODE_ASYNCHRONOUS_gc;
		USARTD0.CTRLB = USART_RXEN_bm | USART_TXEN_bm;	
		
		//USARTD0.CTRLC = USART_PMODE_EVEN_gc;
		//PORTD.PIN2CTRL = PORT_OPC_PULLUP_gc;
    }
	else
	{
		//Comparator
		vAnalogComparatorInit_Bootloader();//From differential UARTRX to single ended 
		//Gluelogic invert TX positive
		vGlueLogicInit_Bootloader();
		//UARTC to PC init
		PORTC.DIRSET = (1 << UART_TX_PIN);
		PORTC.DIRSET = (0 << UART_RX_PIN);
		USARTC0.BAUDCTRLA = (UART_BSEL_VALUE & USART_BSEL_gm);
		USARTC0.BAUDCTRLB = ((UART_BSCALE_VALUE << USART_BSCALE_gp) & USART_BSCALE_gm) | ((UART_BSEL_VALUE >> 8) & ~USART_BSCALE_gm);
		USARTC0.CTRLC |= (uint8_t)USART_CHSIZE_8BIT_gc | USART_PMODE_EVEN_gc | USART_CMODE_ASYNCHRONOUS_gc;
		USARTC0.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
		//USARTC0.CTRLA = USART_RXCINTLVL0_bm | USART_TXCINTLVL0_bm; //USE_INTERRUPTS
		        // Initialize RX pin pull-up
		
        UART_RX_PIN_CTRL = 0x18;     
	}
	#endif
}

// Shut down UART
void uart_deinit(void)
{
    UART_DEVICE.CTRLB = 0;
    #ifdef USE_INTERRUPTS
    UART_DEVICE.CTRLA = 0;
    #endif // USE_INTERRUPTS
    UART_DEVICE.BAUDCTRLA = 0;
    UART_DEVICE.BAUDCTRLB = 0;
    #ifdef UART_REMAP
    UART_PORT.REMAP &= ~PORT_USART0_bm;
    #endif // UART_REMAP
    UART_PORT.DIRCLR = (1 << UART_TX_PIN);
}

#ifdef Use_ADC
void adc_init()
{
	ADCA.CTRLB = 0x00;
	ADCA.REFCTRL = ADC_REFSEL_INTVCC_gc;
	ADCA.PRESCALER = ADC_PRESCALER_DIV32_gc;
	ADCA.CTRLA = ADC_ENABLE_bm;
	
	
	//	Calibrate the ADC
	
	/*uint8_t CalibrationByteL;
	uint8_t CalibrationByteH;
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	CalibrationByteL = pgm_read_byte(offsetof(NVM_PROD_SIGNATURES_t, ADCACAL0));
	CalibrationByteH = pgm_read_byte(offsetof(NVM_PROD_SIGNATURES_t, ADCACAL1));
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	ADCA.CAL = CalibrationByteH<<8 | CalibrationByteL;*/

	//
	//	Initialize the ADC channel
	//		-> Channel 3 (PA3 - 1V1 or 1V8)
	//		-> Gain 1x
	//		-> Single-ended
	
	PORTA.DIR &= ~(0x01 << 0x03);
	PR.PRPA  &= ~PR_ADC_bm;
	ADCA.CH0.CTRL = ADC_CH_INPUTMODE0_bm;
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN3_gc | ADC_CH_MUXNEG_INTGND_MODE3_gc;
	// Start a new measurement
	ADCA.CH0.CTRL |= ADC_CH_START_bm;
	while(!(ADCA.CH0.INTFLAGS & 0x01));
	ADCA.CH0.INTFLAGS = 0x01;
	uint16_t ADC_Result = ADCA.CH0.RES;
	ADC_Result = (ADC_Result * 3125) / 2048;
	//if(ADC_Result >= 1500)//Bigger than 1500mv
	    is_FPGA_Board=0;//Not FPGA Board
	//else
	//    is_FPGA_Board=1;//Is FPGA Board
}
#endif
#ifdef SpinnerBoard
void vGlueLogicInit_Bootloader(void)
{
	//Glue Logic
	*((uint8_t *)&PR.PRGEN + 0x00) &= ~PR_XCL_bm;

	XCL.CTRLA &= ~XCL_PORTSEL_gm;
	XCL.CTRLA |= XCL_PORTSEL_PC_gc;

	XCL.CTRLA &= ~XCL_LUTCONF_gm;
	XCL.CTRLA |= XCL_LUTCONF_1LUT3IN_gc;

	XCL.CTRLB &= ~XCL_IN0SEL_gm;
	XCL.CTRLB |= 0x02 << XCL_IN0SEL_gp;

	XCL.CTRLB &= ~XCL_IN1SEL_gm;
	XCL.CTRLB |= 0x02 << XCL_IN1SEL_gp;

	XCL.CTRLB &= ~XCL_IN2SEL_gm;
	XCL.CTRLB |= 0x02 << XCL_IN2SEL_gp;

	XCL.CTRLB &= ~XCL_IN3SEL_gm;
	XCL.CTRLB |= 0x02 << XCL_IN3SEL_gp;

	//Disable TX
	XCL.CTRLA &= ~XCL_LUT0OUTEN_gm;
	XCL.CTRLA |= XCL_LUT0OUTEN_DISABLE_gc;

	XCL.CTRLC = 0;
	XCL.CTRLC |= (uint8_t)XCL_DLYSEL_DLY11_gc | (uint8_t)0x00 | (uint8_t)0x00 << XCL_DLY1CONF_gp;

	XCL.CTRLD &= ~XCL_TRUTH0_gm;
	XCL.CTRLD |= 0xC << XCL_TRUTH0_gp;

	XCL.CTRLD &= ~XCL_TRUTH1_gm;
	XCL.CTRLD |= 0x3 << XCL_TRUTH1_gp;

	PORTC.DIRSET = (1 << 4);//ioport_set_pin_mode(IOPORT_CREATE_PIN(PORTC, 4), 1);
	PORTC.OUTCLR = (1 << 4);//ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 4), 0);

	//Enable TX
	XCL.CTRLA &= ~XCL_LUT0OUTEN_gm;
	XCL.CTRLA |= XCL_LUT0OUTEN_PIN4_gc;
}




void vAnalogComparatorInit_Bootloader(void) {
    // ---------------------------------------------------------------
    // config ACA
    // ---------------------------------------------------------------
    // setup comparator pin 0 and pin 1 are the input of portA.
    

	
    struct ac_config aca_config;
	
    //memset(&aca_config, 0, sizeof(struct ac_config));

	aca_config.winctrl =0;
	aca_config.ctrlb = 1;
	aca_config.acctrl &= ~AC_HYSMODE_gm;
	aca_config.acctrl |= AC_HYSMODE_LARGE_gc;
	

		
	 if(is_FPGA_Board==0) 
	 {
		aca_config.acmuxctrl &= ~AC_MUXNEG_gm;
		aca_config.acmuxctrl |= AC_MUXNEG_PIN1_gc;
		
		aca_config.acmuxctrl &= ~AC_MUXPOS_gm;
		aca_config.acmuxctrl |= AC_MUXNEG_PIN0_gc;
	 }
	 else
	 {
		aca_config.acmuxctrl &= ~AC_MUXNEG_gm;
		aca_config.acmuxctrl |= AC_MUXNEG_PIN0_gc;
		
		aca_config.acmuxctrl &= ~AC_MUXPOS_gm;
		aca_config.acmuxctrl |= AC_MUXPOS_PIN1_gc;
	 }


	aca_config.winctrl &= ~AC_WINTMODE_gm;
	aca_config.winctrl |= 0;
	
	aca_config.acctrl &= ~AC_INTLVL_gm;
	aca_config.acctrl |= (0 << AC_INTLVL_gp);
	
	*((uint8_t *)&PR.PRGEN + 1) &= ~0x01;
	ACA.CTRLB       = aca_config.ctrlb;
	ACA.WINCTRL     = aca_config.winctrl;
	ACA.AC0MUXCTRL  = aca_config.acmuxctrl;
	ACA.AC0CTRL     = aca_config.acctrl;
	ACA.AC0CTRL |= AC_ENABLE_bm;
	
	
    // enable output of analog compartor (to Port R1)
    PORTCFG.ACEVOUT = PORTCFG_ACOUT_PR_gc;
    ACA.CTRLA = 1;
	
	PORTR.DIRSET = (1 << 1);
}
#endif