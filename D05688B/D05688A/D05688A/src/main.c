/**
 * \file
 *
 * \brief Firmware f�r Fast Ethernet IRT
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * 20180618:
 * HEY
 * for D05688 (2017 05 E20586-01 IRT FPGA)
 * adjustment for HW changes:
 * ADC3: from 1.8V --> 1.1V
 * PC0 VCXO_SDA --> nFPGA
 * PC1 VCXO_SCL --> Not connected
 * PC5 SCK -> not connected
 * PC6 MISO -> not connected
 * PC7 MOSI -> not connected
 * PD6 nRESET_PHY -> PHY_RESET_MC (inverted logic)
 * PD7 ADC15: nReset_CPLD --> +3.3V (ADC input; was on ADC0) 
 *
 * 20180112:
 * HEY
 * Umsetzung von BASCOM (A99080) in C unter Benutzung der ASF bibliothek
 * �nderungen im Vergleich zum urspr�nglichen Code:
 * -5V: wird jetzt �ber den ADC(7) gelesen und mit einem Schwellwert (~2V) verglichen.
 *      bisher wurde der Pin als digitaler Eingang verwendet.
 * DIAG_TX: im Betrieb ist DIAG_TX jetzt LOW, und wird erst f�r die Kommunkikation freigeschaltet
 * N_DIAG_TX wird jetzt aus DIAG_TX �ber die XMEGA Custom Logic (XCL) generiert
 *           dadurch ist das Sendesignal gr��er
 * +3V3: jetzt wird die +3V3 Analogspannung �ber ADC1 eingelesen.
 *       bisher wurde die Spannung �ber den ADC0 gelesen. Dies hat zum schalten des
 *       Analogkomperators gef�hrt. Dadurch konnte der USART das darauffolgende Zeichen nicht richtig einlesen.
 *       Durch Verwendung von ADC1 wird der Effekt minimiert.
 * 
 *
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>
#include <stdio.h>
#include <util/atomic.h>

#include "main.h"
#include "phy.h"
#include "led.h"
#include "serialport1.h"
#include "serialport2.h"
#include "boardAdc.h"
#include "gluelogic.h"
#include "fuses.h"
#include "nvmManagement.h"
#include "diagnosticData.h"
#include "protokollparser.h"


#define MAIN_TIME_GAIN                  5
#define MAIN_TIME_TOGGLE_FAST           142

#define MAIN_POSITION_ENTERDIAGNOSTIC   0
#define MAIN_POSITION_SENDTOOTHERSIEDE  600
#define MAIN_POSITION_ADC               150
#define MAIN_POSITION_END               999

uint16_t uiOthersideReceivedCounterOld;
uint16_t uiMsMainLoopCounter = 0;

bool bInitialized = false;
bool bPhyOk = false;
bool bNvmContainsData = false;

void mainInit(void);
void mainLoop(void);

uint16_t uiDataHelp=0;

void mainInit(void) {
    uint8_t ucPhyInitCounter = 0;

    sysclk_init();
    sleepmgr_init();

    irq_initialize_vectors();
    vBoardInit();
    vUART1Init();
    vUART2Init();
    irq_initialize_vectors();
    cpu_irq_enable();
    wdt_set_timeout_period(WDT_TIMEOUT_PERIOD_4KCLK);

    bInitialized = false;
    bPhyOk = false;

    _delay_ms(10);
    // phy and FPGA in reset
    ioport_set_pin_level(N_FPGA_RESET, 0);
    ioport_set_pin_level(PHY_RESET_MC, 1);
    _delay_ms(100);
    ioport_set_pin_level(LED, LED_ON);

    bNvmContainsData = bNvmConfigCrcIsOk();

    if(!bNvmConfigIsEmpty() && !bNvmContainsData) {
        vLedBlink3();
        //return;
    }

    _delay_ms(1800);    
    
    while (!bInitialized) {
        // set FPGA & PHY in reset
        ioport_set_pin_level(N_FPGA_RESET, 0); 
        if (! (ucPhyInitCounter & 0x03)) {
            ioport_set_pin_level(PHY_RESET_MC, 1);
        }        
        _delay_ms(200);
        // release FPGA from reset
        ioport_set_pin_level(N_FPGA_RESET, 1);
        _delay_ms(200);
        // release PHY from reset
        if (! (ucPhyInitCounter & 0x03)) {
            ioport_set_pin_level(PHY_RESET_MC, 0);
        }            
        _delay_ms(200);

        bPhyOk = bInitializePhy();
        if (bPhyOk) {
			vMdioWriteRegister(0x03, PHY_ADDR_ANAR, 0x0081);
			_delay_ms(10);
			uiDataHelp = uiMdioReadRegister(0x03, PHY_ADDR_ANAR);
            bInitialized = true;
            vLedBlink3();
        }
    }
}


void mainLoop(void) {
    uint16_t uiMsMainLoopCounter = 0;
    uint16_t uiSecondaryCounter = 0;
    uint8_t ucDiagTxCounter = 0;
    uint8_t ucAdcIndex = 0;

    wdt_enable();

    while(true) {
        if(tc45_is_cc_interrupt (&TCC4, TC45_CCA)) {
            tc45_clear_cc_interrupt(&TCC4, TC45_CCA);

            if ((uiMsMainLoopCounter % MAIN_TIME_GAIN) == 0) {
                vReadAdc(ADC_INDEX_GAIN);
                vScanCommandLine();
            }

            if ((uiMsMainLoopCounter % MAIN_TIME_TOGGLE_FAST) == 0) {
                if (uiMsMainLoopCounter == 0) {
                    if (bFlagDiagMode || bConfigurationMode) {
                        ioport_toggle_pin_level(LED);
                    }
                    } else {
                    if (!bInitialized) {
                        ioport_toggle_pin_level(LED);
                    }
                }
            }

            if (uiMsMainLoopCounter == MAIN_POSITION_ENTERDIAGNOSTIC && (bFlagEnableDiagMode || bFlagEnableConfigurationMode || bFlagEnableOtherSideConfigurationMode)) {
                ioport_set_pin_level(PHY_RESET_MC, 1);
                vDiagEnableDiagMode();
            }
            if (uiMsMainLoopCounter > MAIN_POSITION_ADC && ucAdcIndex < ADC_INDEX_END) {
                if (ucAdcIndex != ADC_INDEX_GAIN) {
                    vReadAdc(ucAdcIndex);
                }
                ucAdcIndex++;
            }
            if (uiMsMainLoopCounter == MAIN_POSITION_SENDTOOTHERSIEDE) {
                vSendDiagnosticToOtherSide();
            }


            if (uiMsMainLoopCounter == MAIN_POSITION_END) {
                uiMsMainLoopCounter = 0;
                ucAdcIndex = 0;

                if (bFlagDiagMode) {
                    for(ucDiagTxCounter = 0; ucDiagTxCounter < PRINT_INDEX_END; ucDiagTxCounter++) {
                        vDiagPrintText(ucDiagTxCounter);
                    }
                }
                if (uiSecondaryCounter == 3) {
                    uiSecondaryCounter = 0;
                    bool bResetOtherSideValues;
                    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                        bResetOtherSideValues = (ucOtherSideReceivedCounter == uiOthersideReceivedCounterOld);
                    }
                    if (bResetOtherSideValues) {
                        uiV50vOtherSide = 0;
                        uiV33vOtherSide = 0;
                        uiV11vOtherSide = 0;
                        uiV25vOtherSide = 0;
                        uiI50vOtherSide = 0;
                        uiI33vOtherSide = 0;
                        uiVGainOtherSide = 0;
                        uiVGainMinOtherSide = 0;
                        uiVGainMaxOtherSide = 0;
                        iMV50vOtherSide = 0;
                        uiTemperatureOtherSide = 0;
                    }
                    uiSecondaryCounter = 0;
                    uiOthersideReceivedCounterOld = ucOtherSideReceivedCounter;

                } else {
                    uiSecondaryCounter++;
                }

                if (ucEnableMinMax < 3) {
                    ucEnableMinMax++;
                }
                wdt_reset()
            } else {
                ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                    uiMsMainLoopCounter++;
                }
            }
        }
    }
}

int main (void)
{
    mainInit();
    mainLoop();
}
