/*
 * CFile1.c
 *
 * Created: 17.01.2018 13:36:20
 *  Author: eyermann
 */ 

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>

#include "crcCalc.h"
#include "serialport1.h"

bool bCrcInUse = false;


bool bWaitForCrcFree(void) {
    if (bCrcInUse) {
        _delay_us(100);
        if (bCrcInUse) {
            return false;
        }
    }
    bCrcInUse = true;    
    return true;
}

void vReleaseCrc(void) {
    bCrcInUse = false;
}


bool bReceivedCrcIsCorrect(char * pcBuffer, uint8_t ucCRCIndex) {
    // calculates the CRC, then compares the calculated CRC with the received CRC
    uint16_t uiCalculatedCRC;
    uint16_t uiRecievedCRC;
    
    if (!bWaitForCrcFree()) {
        return false;
    }
    
    crc_set_initial_value(0xFFFF);
    uiCalculatedCRC = (uint16_t) crc_io_checksum(pcBuffer, ucCRCIndex, CRC_16BIT);

    vReleaseCrc();
    
    char * pcCrcStart = pcBuffer + ucCRCIndex;    
    uiRecievedCRC = uiHexStringToDec(pcCrcStart, 4);
    if (uiCalculatedCRC == uiRecievedCRC) {
        return true;
    }
    return false;
}


uint16_t uiCalculateEepromCrc(eeprom_addr_t uiEndMarker) {
    uint8_t ucByte;
    uint8_t ucIndex;
    uint16_t uiChecksum;
    
    if (!bWaitForCrcFree()) {
        return 0x000;
    }
    crc_set_initial_value(0xFFFF);
    crc_io_checksum_byte_start(CRC_16BIT);
    
    for(ucIndex = NVM_CONFIG_START; ucIndex < (uiEndMarker); ucIndex++) {
        ucByte = nvm_eeprom_read_byte(ucIndex);
        crc_io_checksum_byte_add(ucByte);        
    }    
    uiChecksum = (uint16_t) crc_io_checksum_byte_stop();
    vReleaseCrc();
    return uiChecksum;
}