/*
 * serialport_1.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#ifndef serialport_1_h
#define serialport_1_h

#include "nvmManagement.h"


// buffer size: Command(2) + Length(2) + 20 * Phy[0/1](10) + 4 bytes for internal communication
#define BUFFER_SIZE   (2 + 2 + 2 * NVM_BLOCK_SIZE * NVM_BLOCK_MAXENTRIES + 4)

#define INPUT_BUFFER_SIZE   BUFFER_SIZE
#define SEND_BUFFER_SIZE    BUFFER_SIZE


#define SWAP(x, y) do { typeof(x) SWAP = x; x = y; y = SWAP; } while (0)

extern bool bFlagDiagMode;
extern bool bConfigurationMode;
extern bool bFlagEnableDiagMode;
extern bool bFlagEnableConfigurationMode;
extern bool bFlagEnableOtherSideConfigurationMode;
extern bool bOtherSideInConfigurationMode;
extern bool bCommandFromLocalSide;

extern char cCommandBuffer[];
extern volatile uint8_t ucBytesInCommandBuffer;

extern char cSendBuffer[];
extern volatile uint8_t ucBytesInSendBuffer;

void vEnableSendBuffer(void);
void vDisableSendBuffer(void);
void vUART1EnableTx(void);
void vUART1Init(void);
void vDiagCheckStatus(void);
void vDiagPrintText(uint8_t ucIndex);
void vDiagEnableDiagMode(void);
uint16_t uiHexStringToDec(char * pcBuffer, uint8_t ucLength);
void vAddCharToSendBuffer(char cChar, bool bAddCrc);

void vAddStringToSendBuffer(const char *cBuffer, bool bAddCrc);

extern void USART_1_ISR_HANDLER(void);

#endif