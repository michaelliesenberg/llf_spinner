/*
 * xcl.c
 *
 * Created: 09.01.2018 10:43:47
 *  Author: Eyermann
 */ 

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <xcl.h>
#include <xcl_glue_logic.h>

#include "gluelogic.h"

void vEnableNDiagTx(void) {
    xcl_lut0_output(LUT0_OUT_PIN4);    // output to PC Pin 4
}


void vDisableNDiagTx(void) {
    xcl_lut0_output(LUT0_OUT_DISABLE);
}


void vGlueLogicInit(void) {
    xcl_enable(XCL_ASYNCHRONOUS);
    xcl_port(PC);
    
     xcl_lut_type(LUT_1LUT3IN);
     xcl_lut_in0(LUT_IN_PINL);
     xcl_lut_in1(LUT_IN_PINL);
     xcl_lut_in2(LUT_IN_PINL);
     xcl_lut_in3(LUT_IN_PINL);     
     vDisableNDiagTx();
     xcl_lut_config_delay(DLY11, LUT_DLY_DISABLE, LUT_DLY_DISABLE); // no delay
     xcl_lut0_truth(IN1);               // LUT1 (not DIAG_TX)
     xcl_lut1_truth(NOT_IN3);           // DIAG_TX   
     
     ioport_set_pin_mode(DIAG_OUT_INV, IOPORT_DIR_OUTPUT);
     ioport_set_pin_level(DIAG_OUT_INV, 0);
}