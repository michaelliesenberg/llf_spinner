/*
 * led.h
 *
 * Created: 04.01.2018 07:58:42
 *  Author: Eyermann
 */ 


#ifndef LED_H_
#define LED_H_

#define LED_ON  0
#define LED_OFF 1

void vLedInit(void);
void vLedBlink3(void);

#endif   /* LED_H_ */