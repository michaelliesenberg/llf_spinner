


#ifndef PROTOKOLLPARSER_H_
#define PROTOKOLLPARSER_H_


#define FROM_LOCAL_SIDE     true
#define FROM_REMOTE_SIDE    false

#define DIAG_START_CHAR         'U'
#define DIAG_START_COUNT        10
#define DIAG_COMMAND_INDEX      0
#define DIAG_LENGTH_INDEX       1


#define DIAG_CMD_ENABLE_DIAG    'E'
#define DIAG_CMD_WRITE          'W'
#define DIAG_CMD_READ           'R'
#define DIAG_CMD_STATUS         'S'
#define DIAG_CMD_END            'X'

#define DIAG_RESPONSE_ENABLED               LINE_ENDING "enabled" LINE_ENDING "connected to: "
#define DIAG_RESPONSE_ENABLED_OTHERSIDE     "enabled remote" LINE_ENDING
#define DIAG_RESPONSE_ENABLED_BODY          "body" LINE_ENDING
#define DIAG_RESPONSE_ENABLED_HOLLOWSHAFT   "hollow shaft" LINE_ENDING
#define DIAG_RESPONSE_CRC_ERROR             " CRC error in EEPROM" LINE_ENDING
#define DIAG_RESPONSE_WRITE                 'w'
#define DIAG_RESPONSE_READ                  'r'
#define DIAG_REMOTE_RESPONSE_ERROR          '?'
#define DIAG_RESPONSE_ERROR     "?" LINE_ENDING

#define DIAG_RESPONSE_BODY          'B'
#define DIAG_RESPONSE_HOLLOWSHAFT   'H'


void vScanCommandLine(void);


#endif /* PROTOKOLLPARSER_H_ */

