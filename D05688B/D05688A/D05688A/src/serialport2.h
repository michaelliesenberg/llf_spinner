/*
 * serialport_1.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */ 

#ifndef serialport_2_h
#define serialport_2_h

#define CHAR_SOH    0x01
#define CHAR_STX    0x02
#define CHAR_END    0x04
#define CHAR_ENQ    0x05

#define UART_2_DIAGNOSTIC_DATA_SIZE      (11 * 2)

extern uint8_t ucOtherSideReceivedCounter;

extern uint16_t uiV50vOtherSide;
extern uint16_t uiV33vOtherSide;
extern uint16_t uiV11vOtherSide;
extern uint16_t uiV25vOtherSide;
extern uint16_t uiI50vOtherSide;
extern uint16_t uiI33vOtherSide;
extern uint16_t uiTemperatureOtherSide;
extern uint16_t uiVGainOtherSide;
extern uint16_t uiVGainMinOtherSide;
extern uint16_t uiVGainMaxOtherSide;
extern int16_t iMV50vOtherSide;

extern void vUART2Init(void);
void vReceiveDiagnosticDataFromOtherSide(void);
void vClearSendBuffer(void);
void vSendDiagnosticToOtherSide(void);
void vSendConfigurationToOtherSide(char * pcBuffer, uint8_t ucLength);


#endif