/*
 * phy.h
 *
 * Created: 04.01.2018 07:58:42
 *  Author: Eyermann
 */ 


#ifndef PHY_H_
#define PHY_H_

#define MAX_PHY_ADDR            0x1F
#define MAX_PHY_REGISTER_ADDR   0x1F

#define PHY0_ADDR               0x01
#define PHY1_ADDR               0x03


#define MDIO_FRAME_READY_READ   0x6000
#define MDIO_FRAME_READY_WRITE  0x5002

#define PHY_ADDR_ANAR           0x04


#define PHY_ANAR_100BT_ONLY     0x0101

bool bInitializePhy(void);
uint16_t uiMdioReadRegister(uint8_t ucPhy, uint8_t ucAddress);
void vMdioWriteRegister(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData);

#endif /* PHY_H_ */