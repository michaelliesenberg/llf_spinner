

#ifndef CRC_CALC_H_
#define CRC_CALC_H_

bool bWaitForCrcFree(void);
void vReleaseCrc(void);


bool bReceivedCrcIsCorrect(char * pcBuffer, uint8_t ucCRCIndex);
uint16_t uiCalculateEepromCrc(eeprom_addr_t uiIndex);



#endif /* CRC_CALC_H_ */