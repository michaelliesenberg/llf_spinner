/*
 * serialport_1.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <usart.h>
#include <string.h>
#include <stdio.h>
#include <util/atomic.h>

#include "serialport1.h"
#include "serialport2.h"
#include "boardAdc.h"
#include "gluelogic.h"
#include "crcCalc.h"
#include "nvmManagement.h"
#include "diagnosticData.h"
#include "protokollparser.h"


bool bFlagDiagMode = false;
bool bFlagEnableDiagMode = false;
bool bFlagEnableOtherSideConfigurationMode = false;

bool bConfigurationMode = false;
bool bOtherSideInConfigurationMode = false;
bool bFlagEnableConfigurationMode = false;

char cCommandBuffer[INPUT_BUFFER_SIZE];
volatile uint8_t ucBytesInCommandBuffer;

char cSendBuffer[BUFFER_SIZE];
volatile uint8_t ucBytesInSendBuffer = 0;

bool bCommandFromLocalSide = true;

bool bSendBufferEnabled = true;

void vEnableSendBuffer(void) {
    bSendBufferEnabled = true;
}

void vDisableSendBuffer(void) {
    bSendBufferEnabled = false;
}


void vAddCharToSendBuffer(char cChar, bool bAddCrc) {
    usart_serial_putchar(USART_1, cChar);
    if (!bSendBufferEnabled || ucBytesInSendBuffer >= BUFFER_SIZE) {
        return;
    }
    cSendBuffer[ucBytesInSendBuffer] = cChar;
    ucBytesInSendBuffer++;
    if (bAddCrc && cChar != ' ') {
        crc_io_checksum_byte_add(cChar);
    }
}





void vDiagEnableDiagMode(void) {
    bool bIsBody;
    bIsBody = ioport_get_pin_level(BODY_SELECT) == IS_BODY;
    vEnableNDiagTx();
	vUART1EnableTx();
    if (bFlagEnableDiagMode) {
        bFlagDiagMode = true;
    }
    if (bFlagEnableConfigurationMode) {
        bFlagDiagMode = false;
        bConfigurationMode = true;
        bOtherSideInConfigurationMode = false;
        vAddStringToSendBuffer(DIAG_RESPONSE_ENABLED, false);
        if (bIsBody) {
            vAddStringToSendBuffer(DIAG_RESPONSE_ENABLED_BODY, false);
        } else {
            vAddStringToSendBuffer(DIAG_RESPONSE_ENABLED_HOLLOWSHAFT, false);
        }
        memset(cSendBuffer, 0, SEND_BUFFER_SIZE);
        ucBytesInSendBuffer = 0;

    }
    if (bFlagEnableOtherSideConfigurationMode) {
        bFlagDiagMode = false;
        bConfigurationMode = false;
        bOtherSideInConfigurationMode = true;
    }
	bFlagEnableDiagMode = false;
    bFlagEnableConfigurationMode = false;
    bFlagEnableOtherSideConfigurationMode = false;
}









void vUART1EnableTx(void) {
    ioport_set_pin_dir(USART_1_TX, IOPORT_DIR_OUTPUT);
}

void vUART1Init(void) {
	// ---------------------------------------------------------------
	// config UART1
	// ---------------------------------------------------------------
	static usart_serial_options_t usart_1_options = {
    	.baudrate = USART_1_BAUDRATE,
    	.charlength = USART_1_CHAR_LENGTH,
    	.paritytype = USART_1_PARITY,
    	.stopbits = USART_1_STOP_BIT
	};

    ioport_set_pin_dir(USART_1_TX, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(USART_1_RX, IOPORT_DIR_INPUT);
	usart_init_rs232(USART_1, &usart_1_options);
	usart_set_rx_interrupt_level(USART_1, USART_INT_LVL_LO);
}



ISR(USART_1_RX_ISR_HANDLER)
{
	static uint8_t ucDiagEnableCounter = 0;
    static uint8_t ucReceiverIndex = 0;
    static uint8_t ucErrorCounter = 0;
    static char cInputBuffer[INPUT_BUFFER_SIZE];

    if (usart_rx_is_complete(USART_1)) {
        volatile uint32_t ulReceivedByte;
        ulReceivedByte = usart_getchar(USART_1);

		if (!bConfigurationMode &&
            !bFlagDiagMode &&
            !bOtherSideInConfigurationMode &&
            ulReceivedByte == DIAG_START_CHAR) {
			    ucDiagEnableCounter++;
			    if (ucDiagEnableCounter >= DIAG_START_COUNT) {
		            bFlagEnableDiagMode = true;
		            ucDiagEnableCounter = 0;
	            }
		} else {
            ucDiagEnableCounter = 0;
        }

        if (ucReceiverIndex < INPUT_BUFFER_SIZE &&
            ulReceivedByte != ' ' ) {
                cInputBuffer[ucReceiverIndex] = ulReceivedByte;
                ucReceiverIndex++;
        }

        if (ulReceivedByte == (uint8_t) LINE_ENDING[0] || ucReceiverIndex == INPUT_BUFFER_SIZE) {
            if (ucBytesInCommandBuffer == 0) {
                if (ucReceiverIndex > 0) {
                    memcpy(cCommandBuffer, cInputBuffer, ucReceiverIndex * sizeof(uint8_t));
                    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                        ucBytesInCommandBuffer = ucReceiverIndex;
                        bCommandFromLocalSide = true;
                    }
                }
            } else {
                ucErrorCounter++;
            }
            if (ucErrorCounter > 5) {
                ucErrorCounter = 0;
                ucBytesInCommandBuffer = 0;
            }
            ucReceiverIndex = 0;
        }
	}
}


ISR(USART_1_DRE_ISR_HANDLER)
{
    // USART_Dre(&USART_C0);
}