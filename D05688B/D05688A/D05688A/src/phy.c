/*
 * phy.c
 *
 * Created: 04.01.2018 07:52:56
 *  Author: Eyermann
 *
 * This file contains routines to access the PHY's (TLK105)
 *
 */ 

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>
#include <stdio.h>

#include "phy.h"
#include "nvmManagement.h"

#define CLOCK_POSITION_IDLE         (0)
#define CLOCK_COUNT_IDLE            (2 * 4)
#define CLOCK_POSITION_START        (CLOCK_POSITION_IDLE + CLOCK_COUNT_IDLE)
#define CLOCK_COUNT_START           (2 * 2)
#define CLOCK_POSITION_OPCODE       (CLOCK_POSITION_START + CLOCK_COUNT_START)
#define CLOCK_COUNT_OPCODE          (2 * 2)
#define CLOCK_POSITION_PHY_ADDRESS  (CLOCK_POSITION_OPCODE + CLOCK_COUNT_OPCODE)
#define CLOCK_COUNT_PHY_ADDRESS     (2 * 5)
#define CLOCK_POSITION_REG_ADDRESS  (CLOCK_POSITION_PHY_ADDRESS + CLOCK_COUNT_PHY_ADDRESS)
#define CLOCK_COUNT_REG_ADDRESS     (2 * 5)
#define CLOCK_POSITION_TA           (CLOCK_POSITION_REG_ADDRESS + CLOCK_COUNT_REG_ADDRESS)
#define CLOCK_COUNT_TA              (2 * 2)
#define CLOCK_POSITION_DATA         (CLOCK_POSITION_TA + CLOCK_COUNT_TA)
#define CLOCK_COUNT_DATA            (2 * 16)
#define CLOCK_POSITION_END          (CLOCK_POSITION_DATA + CLOCK_COUNT_DATA)
#define CLOCK_COUNT_END             (2 * 2)
#define CLOCK_POSITION_FINISHED     (CLOCK_POSITION_END + CLOCK_COUNT_END)


enum eAccess {
    Write = 0, 
    Read = 1
};


bool bDisableAutoNegotiation(uint16_t phy);
bool bInitializePhyDefault(uint8_t ucPhy);
bool bInitializePhyFromEeprom(uint8_t ucPhy);
uint16_t uiMdioTransfer(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData, enum eAccess eAccessType);




uint16_t uiMdioTransfer(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData, enum eAccess eAccessType) {
    uint8_t ucClockCounter = 0;
    volatile uint8_t ucClockPosition;
    uint16_t uiTempData = 0;
    uint16_t uiReadData = 0;
    bool bLevel = 0;
    
    
    if (ucPhy > MAX_PHY_ADDR) {
        return (0xFFFF);
    }
    while (ucClockCounter < CLOCK_POSITION_FINISHED) {
        while (!tc45_is_cc_interrupt (&TCC4, TC45_CCA))
        {}
            
        tc45_clear_cc_interrupt(&TCC4, TC45_CCA);
        ioport_set_pin_level(MDIO_CLK, ucClockCounter & 0x01);
            
        if ((ucClockCounter & 0x01) == 0x00) {
            if ( ucClockCounter < CLOCK_POSITION_IDLE + CLOCK_COUNT_IDLE) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_IDLE) / 2;
                    
            } else if (ucClockCounter < CLOCK_POSITION_START + CLOCK_COUNT_START) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_START) / 2;
                if (ucClockPosition == 0) {
                    ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_OUTPUT);
                }
                bLevel = ((ucClockPosition & 0x01) == 0x01);
                
            } else if (ucClockCounter < CLOCK_POSITION_OPCODE + CLOCK_COUNT_OPCODE) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_OPCODE) / 2;                    
                bLevel = ((ucClockPosition & 0x01) == 0x01);
                if (eAccessType == Read) {
                    bLevel = !bLevel;
                }
                
            } else if (ucClockCounter < CLOCK_POSITION_PHY_ADDRESS + CLOCK_COUNT_PHY_ADDRESS) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_PHY_ADDRESS) / 2;
                if (ucClockPosition == 0) {
                    uiTempData = ucPhy;
                }
                bLevel = ((uiTempData & 0x10) == 0x10);
                uiTempData <<= 1;                    
            } else if (ucClockCounter < CLOCK_POSITION_REG_ADDRESS + CLOCK_COUNT_REG_ADDRESS) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_REG_ADDRESS) / 2;
                if (ucClockPosition == 0) {
                    uiTempData = ucAddress;
                }
                bLevel = ((uiTempData & 0x10) == 0x10);
                uiTempData <<= 1;
            } else if (ucClockCounter < CLOCK_POSITION_TA + CLOCK_COUNT_TA) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_TA) / 2;
                if (ucClockPosition == 0 && eAccessType == Read) {
                    ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_INPUT);
                }
                bLevel = ((ucClockPosition & 0x01) == 0x00);
            } else if (ucClockCounter < CLOCK_POSITION_DATA + CLOCK_COUNT_DATA) {
                ucClockPosition = (ucClockCounter - CLOCK_POSITION_DATA) / 2;
                if (ucClockPosition == 0) {
                    uiTempData = uiData;
                }
                bLevel = ((uiTempData & 0x8000) == 0x8000);
                uiTempData <<= 1;
                uiReadData <<= 1;
                if (ioport_get_pin_level(MDIO_DATA) == true) {
                    uiReadData |= 0x0001;
                }
            } else {
                bLevel = true;
                ioport_set_pin_level(MDIO_DATA, bLevel);
                ioport_set_pin_dir(MDIO_DATA, IOPORT_DIR_INPUT);
                break;
            }
            ioport_set_pin_level(MDIO_DATA, bLevel);                
        }            
        ucClockCounter++;
    }
    return uiReadData;
}

uint16_t uiMdioReadRegister(uint8_t ucPhy, uint8_t ucAddress) {
    return uiMdioTransfer(ucPhy, ucAddress, 0, Read);    
}


void vMdioWriteRegister(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData) {
    uiMdioTransfer(ucPhy, ucAddress, uiData, Write);    
}

bool bInitializePhyDefault(uint8_t ucPhy) {
    uint16_t uiData;

    vMdioWriteRegister(ucPhy, PHY_ADDR_ANAR, PHY_ANAR_100BT_ONLY);
    _delay_ms(10);
    uiData = uiMdioReadRegister(ucPhy, PHY_ADDR_ANAR);
    _delay_ms(10);
    if (uiData != PHY_ANAR_100BT_ONLY) {
        return false;
    }
    return true;
}    
    
bool bInitializePhyFromEeprom(uint8_t ucPhy) {
    uint8_t ucIndex = 0;
    uint8_t ucPhyAddress;
    uint16_t uiData;
    
    struct stConfigData stStructureConfigData;
    
    do 
    {
        vGetNvmConfig(&stStructureConfigData, ucIndex);
        ucIndex++;
        if (stStructureConfigData.ucChip == NVM_ID_END) {
            break;
        }
        if (stStructureConfigData.ucChip == ucPhy) {
            if (ucPhy == NVM_ID_PHY0) {
                ucPhyAddress = PHY0_ADDR;
            } else if (ucPhy == NVM_ID_PHY1) {
                ucPhyAddress = PHY1_ADDR;
            } else {
                return false;
            }
            vMdioWriteRegister(ucPhyAddress, stStructureConfigData.uiAddress, stStructureConfigData.uiData);
            _delay_ms(10);
            uiData = uiMdioReadRegister(ucPhyAddress, stStructureConfigData.uiAddress);
        
            if (uiData != stStructureConfigData.uiData) {
                return false;
            }
            _delay_ms(10);
        }
    } while (ucIndex <= NVM_BLOCK_SIZE);
    return true;  
}


bool bInitializePhy(void) {
    bool bConfigResult;

    if (bNvmHasConfigForPhy(NVM_ID_PHY0)) {
        bConfigResult = bInitializePhyFromEeprom(NVM_ID_PHY0);
    } else {
        bConfigResult = bInitializePhyDefault(PHY0_ADDR);
    }
    if (!bConfigResult) {
        return false;
    }
    if (bNvmHasConfigForPhy(NVM_ID_PHY1)) {
        bConfigResult = bInitializePhyFromEeprom(NVM_ID_PHY1);
    } else {
        bConfigResult = bInitializePhyDefault(PHY1_ADDR);
    }
    if (!bConfigResult) {
        return false;
    }
    return true;
}    