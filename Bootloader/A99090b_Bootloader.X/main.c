
#include "xboot.h"


unsigned char comm_mode;
unsigned char buffer[SPM_PAGESIZE];


// Main code
int main(void)
{
    ADDR_T address = 0;
    unsigned char in_bootloader = 0;
    unsigned char val = 0;
    int i = 0;


    comm_mode = MODE_UNDEF;


    OSC.CTRL |= OSC_RC32MEN_bm; // turn on 32 MHz oscillator
    while (!(OSC.STATUS & OSC_RC32MRDY_bm)) { }; // wait for it to start
    CCP = CCP_IOREG_gc;
    CLK.CTRL = CLK_SCLKSEL_RC32M_gc;

    //OSC.CTRL |= OSC_RC32KEN_bm;
    //while(!(OSC.STATUS & OSC_RC32KRDY_bm));
    //DFLLRC32M.CTRL = DFLL_ENABLE_bm;



    // Initialize UART
    uart_init();

    // Initialize RX pin pull-up

    // Enable RX pin pullup
    UART_RX_PIN_CTRL = 0x18;
    //UART_PORT |= (1 << UART_RX_PIN);

    // Initialize UART EN pin
    UART_EN_PORT.DIRSET = (1 << UART_EN_PIN);

    if (uart_char_received())
    {
            in_bootloader = 1;
            comm_mode = MODE_UART;
    }


    while (in_bootloader) 
    {

        val = get_char();

            if (val == CMD_CHECK_AUTOINCREMENT)
            {
                    send_char(REPLY_YES);
            }
            else if (val == CMD_SET_ADDRESS)
            {
                    // Read address high then low
                    address = get_2bytes();
                    // acknowledge
                    send_char(REPLY_ACK);
        }
        else if (val == CMD_SET_EXT_ADDRESS)
        {
                // Read address high then low
                //address = ((ADDR_T)get_char() << 16) | get_2bytes();
                asm volatile (
                        "call get_char"    "\n\t"
                        "mov  %C0,r24"     "\n\t"
                        "call get_2bytes"  "\n\t"
                        "clr  %D0"         "\n\t"
                        : "=r" (address)
                        :
                );

                // acknowledge
                send_char(REPLY_ACK);
        }
        // Chip erase
        else if (val == CMD_CHIP_ERASE)
        {
                    // Erase the application section
                    // XMEGA E5: ERASE_APP NVM command (0x20) erases the entire flash - as a workaround, we erase page-by-page.
                    // From Atmel Support: "The NVM controller design is such that the entire flash will get erased always when application/bootloader erase is called."

            for(uint32_t addr = APP_SECTION_START; addr < APP_SECTION_END; addr += SPM_PAGESIZE)
            {
                Flash_EraseWriteApplicationPage(addr);
            }


            // Erase EEPROM
            EEPROM_erase_all();

            // acknowledge
            send_char(REPLY_ACK);
        }

        else if (val == CMD_CHECK_BLOCK_SUPPORT )
        {
                // yes, it is supported
                send_char(REPLY_YES);
                // Send block size (page size)
                send_char((SPM_PAGESIZE >> 8) & 0xFF);
                send_char(SPM_PAGESIZE & 0xFF);
        }
        // Block load
        else if (val == CMD_BLOCK_LOAD)
        {
                // Block size
                i = get_2bytes();
                // Memory type
                val = get_char();
                // Load it
                send_char(BlockLoad(i, val, &address));
        }
        // Block read
        else if (val == CMD_BLOCK_READ)
        {
                // Block size
                i = get_2bytes();
                // Memory type
                val = get_char();
                // Read it
                BlockRead(i, val, &address);
        }

        // Read program memory byte
        else if (val == CMD_READ_BYTE)
        {
                unsigned int w = Flash_ReadWord((address << 1));

                send_char(w >> 8);
                send_char(w);

                address++;
        }
        // Write program memory low byte
        else if (val == CMD_WRITE_LOW_BYTE)
        {
                // get low byte
                i = get_char();
                send_char(REPLY_ACK);
        }
        // Write program memory high byte
        else if (val == CMD_WRITE_HIGH_BYTE)
        {
                // get high byte; combine
                i |= (get_char() << 8);
                Flash_LoadFlashWord((address << 1), i);
                address++;
                send_char(REPLY_ACK);
        }
        // Write page
        else if (val == CMD_WRITE_PAGE)
        {
                if (address >= (APP_SECTION_SIZE>>1))
                {
                        // don't allow bootloader overwrite
                        send_char(REPLY_ERROR);
                }
                else
                {
                        Flash_WriteApplicationPage( address << 1);
                        send_char(REPLY_ACK);
                }
        }

        // Write EEPROM memory
        else if (val == CMD_WRITE_EEPROM_BYTE)
        {
                EEPROM_write_byte(address, get_char());
                address++;
                send_char(REPLY_ACK);
        }
        // Read EEPROM memory
        else if (val == CMD_READ_EEPROM_BYTE)
        {
                char c = EEPROM_read_byte(address);
                send_char(c);
                address++;
        }
        // Write lockbits
        else if (val == CMD_WRITE_LOCK_BITS)
        {
                SP_WriteLockBits( get_char() );
                send_char(REPLY_ACK);
        }
        // Read lockbits
        else if (val == CMD_READ_LOCK_BITS)
        {
                send_char(SP_ReadLockBits());
        }
        // Read low fuse bits
        else if (val == CMD_READ_LOW_FUSE_BITS)
        {
                send_char(SP_ReadFuseByte(0));
        }
        // Read high fuse bits
        else if (val == CMD_READ_HIGH_FUSE_BITS)
        {
                send_char(SP_ReadFuseByte(1));
        }
        // Read extended fuse bits
        else if (val == CMD_READ_EXT_FUSE_BITS)
        {
                send_char(SP_ReadFuseByte(2));
        }
        // Enter and leave programming mode
        else if ((val == CMD_ENTER_PROG_MODE) || (val == CMD_LEAVE_PROG_MODE))
        {
                // just acknowledge
                send_char(REPLY_ACK);
        }
        // Exit bootloader
        else if (val == CMD_EXIT_BOOTLOADER)
        {
                in_bootloader = 0;
                send_char(REPLY_ACK);
        }
        // Get programmer type
        else if (val == CMD_PROGRAMMER_TYPE)
        {
                // serial
                send_char('S');
        }
        // Return supported device codes
        else if (val == CMD_DEVICE_CODE)
        {
                // send only this device
                send_char(123); // TODO
                // terminator
                send_char(0);
        }
        // Set LED, clear LED, and set device type
        else if ((val == CMD_SET_LED) || (val == CMD_CLEAR_LED) || (val == CMD_SET_TYPE))
        {
                // discard parameter
                get_char();
                send_char(REPLY_ACK);
        }
        // Return program identifier
        else if (val == CMD_PROGRAM_ID)
        {
                send_char('X');
                send_char('B');
                send_char('o');
                send_char('o');
                send_char('t');
                send_char('+');
                send_char('+');
        }
        // Read software version
        else if (val == CMD_VERSION)
        {
                send_char('0' + XBOOT_VERSION_MAJOR);
                send_char('0' + XBOOT_VERSION_MINOR);
        }
        // Read signature bytes
        else if (val == CMD_READ_SIGNATURE)
        {
                send_char(SIGNATURE_2);
                send_char(SIGNATURE_1);
                send_char(SIGNATURE_0);
        }

        else if (val == CMD_CRC)
        {
                uint32_t start = 0;
                uint32_t length = 0;
                uint16_t crc;

                val = get_char();

                switch (val)
                {
                        case SECTION_FLASH:
                                length = PROGMEM_SIZE;
                                break;
                        case SECTION_APPLICATION:
                                length = APP_SECTION_SIZE;
                                break;
                        case SECTION_BOOT:
                                start = BOOT_SECTION_START;
                                length = BOOT_SECTION_SIZE;
                                break;
                        case SECTION_APP:
                                length = XB_APP_SIZE;
                                break;
                        case SECTION_APP_TEMP:
                                start = XB_APP_TEMP_START;
                                length = XB_APP_TEMP_SIZE;
                                break;
                        default:
                                send_char(REPLY_ERROR);
                                continue;
                }

                crc = crc16_block(start, length);

                send_char((crc >> 8) & 0xff);
                send_char(crc & 0xff);
        }
        // ESC (0x1b) to sync
        // otherwise, error
        else if (val != CMD_SYNC)
        {
                send_char(REPLY_ERROR);
        }

        // Wait for any lingering SPM instructions to finish
        Flash_WaitForSPM();
        // End of bootloader main loop
    }

    // Bootloader exit section
    // Code here runs after the bootloader has exited,
    // but before the application code has started
    // --------------------------------------------------


    // Shut down UART
    uart_deinit();

    // Disable RX pin pull-up

    // Disable RX pin pullup
    UART_RX_PIN_CTRL = 0;
    UART_EN_PORT.DIRCLR = (1 << UART_EN_PIN);
    UART_EN_PORT.OUTCLR = (1 << UART_EN_PIN);

    // Jump into main code
    asm("jmp 0");
}


unsigned char __attribute__ ((noinline)) get_char(void)
{
    unsigned char ret;

    while (1)
    {
        // Get next character
        if (comm_mode == MODE_UNDEF || comm_mode == MODE_UART)
        {
                if (uart_char_received())
                {
                        comm_mode = MODE_UART;
                        return uart_cur_char();
                }
        }  
    }

    return ret;
}

void __attribute__ ((noinline)) send_char(unsigned char c)
{
    // Send character
    if (comm_mode == MODE_UNDEF || comm_mode == MODE_UART)
    {

            UART_EN_PORT.OUTSET = (1 << UART_EN_PIN);
            uart_send_char_blocking(c);
            UART_EN_PORT.OUTCLR = (1 << UART_EN_PIN);
    }
}


unsigned int __attribute__ ((noinline)) get_2bytes()
{
        // return (get_char() << 8) | get_char();
        unsigned int result;
        asm volatile (
                "call get_char"    "\n\t"
                "push r24"         "\n\t"
                "call get_char"    "\n\t"
                "pop  %B0"         "\n\t"
                "mov  %A0,r24"     "\n\t"
                : "=r" (result)
                :
        );
        return result;
}

void clear_buffer(void)
{
        unsigned char *ptr = buffer;
        for (long i = 0; i < SPM_PAGESIZE; i++)
        {
                *(ptr++) = 0xff;
        }
}

unsigned char BlockLoad(unsigned int size, unsigned char mem, ADDR_T *address)
{
        ADDR_T tempaddress;
        
        
        // fill up buffer
        for (int i = 0; i < SPM_PAGESIZE; i++)
        {
                char c = 0xff;
                
                if (i < size)
                        c = get_char();
                
                buffer[i] = c;
        }
        
        // EEPROM memory type.
        if(mem == MEM_EEPROM)
        {
                EEPROM_write_block(*address, buffer, size);
                (*address) += size;
                
                return REPLY_ACK; // Report programming OK
        } 
        else if (mem == MEM_FLASH || mem == MEM_USERSIG)
        {
            // NOTE: For flash programming, 'address' is given in words.
            tempaddress = (*address) << 1;  // Store address in page.

            (*address) += size >> 1;



            if (mem == MEM_FLASH)
            {
                    #ifdef ENABLE_FLASH_ERASE_WRITE
                    Flash_ProgramPage(tempaddress, buffer, 1);
                    #else
                    Flash_ProgramPage(tempaddress, buffer, 0);
                    #endif
            }
            else if (mem == MEM_USERSIG)
            {
                    Flash_LoadFlashPage(buffer);
                    Flash_EraseUserSignatureRow();
                    Flash_WaitForSPM();
                    Flash_WriteUserSignatureRow();
                    Flash_WaitForSPM();
            }
            return REPLY_ACK; // Report programming OK
        }
        
        // Invalid memory type?
        else
        {
                return REPLY_ERROR;
        }
}



void BlockRead(unsigned int size, unsigned char mem, ADDR_T *address)
{
        int offset = 0;
        int size2 = size;
        
        // EEPROM memory type.
        
        if (mem == MEM_EEPROM) // Read EEPROM
        {
                EEPROM_read_block(*address, buffer, size);
                (*address) += size;
        }
        // Flash memory type.
        else if (mem == MEM_FLASH || mem == MEM_USERSIG || mem == MEM_PRODSIG)
        {
            (*address) <<= 1; // Convert address to bytes temporarily.

            do
            {
                    if (mem == MEM_FLASH)
                    {
                            buffer[offset++] = Flash_ReadByte(*address);
                    }
                    else if (mem == MEM_USERSIG)
                    {
                            buffer[offset++] = SP_ReadUserSignatureByte(*address);
                    }
                    else if (mem == MEM_PRODSIG)
                    {
                            buffer[offset++] = SP_ReadCalibrationByte(*address);
                    }

                    Flash_WaitForSPM();

                    (*address)++;    // Select next word in memory.
                    size--;          // Subtract two bytes from number of bytes to read
            } while (size);         // Repeat until all block has been read

            (*address) >>= 1;       // Convert address back to Flash words again.
        }
        else
        {
                // bad memory type
                return;
        }
        
        // send bytes
        for (int i = 0; i < size2; i++)
        {
                send_char(buffer[i]);
        }
        
}

uint16_t crc16_block(uint32_t start, uint32_t length)
{
        uint16_t crc = 0;
        
        int bc = SPM_PAGESIZE;
        
        for ( ; length > 0; length--)
        {
                if (bc == SPM_PAGESIZE)
                {
                        Flash_ReadFlashPage(buffer, start);
                        start += SPM_PAGESIZE;
                        bc = 0;
                }
                
                crc = _crc16_update(crc, buffer[bc]);
                
                bc++;
        }
        
        return crc;
}

void install_firmware()
{
        uint16_t crc;
        uint16_t crc2;
        
        // read last block
        Flash_ReadFlashPage(buffer, XB_APP_TEMP_START + XB_APP_TEMP_SIZE - SPM_PAGESIZE);
        
        // check for install command
        if (buffer[SPM_PAGESIZE-6] == 'X' && buffer[SPM_PAGESIZE-5] == 'B' &&
                buffer[SPM_PAGESIZE-4] == 'I' && buffer[SPM_PAGESIZE-3] == 'F')
        {
                crc = (buffer[SPM_PAGESIZE-2] << 8) | buffer[SPM_PAGESIZE-1];
                
                // skip last 6 bytes as they are the install command
                crc2 = crc16_block(XB_APP_TEMP_START, XB_APP_TEMP_SIZE - 6);
                
                // crc last 6 bytes as empty
                for (int i = 0; i < 6; i++)
                        crc2 = _crc16_update(crc2, 0xff);
                
                if (crc == crc2)
                {
                        for (uint32_t ptr = 0; ptr < XB_APP_SIZE; ptr += SPM_PAGESIZE)
                        {
                                Flash_ReadFlashPage(buffer, ptr + XB_APP_TEMP_START);
                                // if it's the last page, clear out the last 6 bytes
                                if (ptr >= XB_APP_SIZE - SPM_PAGESIZE)
                                {
                                        for (int i = SPM_PAGESIZE-6; i < SPM_PAGESIZE; i++)
                                                buffer[i] = 0xff;
                                }
                                Flash_ProgramPage(ptr, buffer, 1);
                        }
                }
                
                xboot_app_temp_erase();
        }
}



