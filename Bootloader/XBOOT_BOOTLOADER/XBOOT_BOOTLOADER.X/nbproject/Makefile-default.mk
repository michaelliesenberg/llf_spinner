#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=/Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/fifo.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/uart.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/watchdog.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/flash.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/sp_driver.S /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/api.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/eeprom_driver.c xboot.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/2093940080/fifo.o ${OBJECTDIR}/_ext/2093940080/uart.o ${OBJECTDIR}/_ext/2093940080/watchdog.o ${OBJECTDIR}/_ext/2093940080/flash.o ${OBJECTDIR}/_ext/2093940080/sp_driver.o ${OBJECTDIR}/_ext/2093940080/api.o ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o ${OBJECTDIR}/xboot.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/2093940080/fifo.o.d ${OBJECTDIR}/_ext/2093940080/uart.o.d ${OBJECTDIR}/_ext/2093940080/watchdog.o.d ${OBJECTDIR}/_ext/2093940080/flash.o.d ${OBJECTDIR}/_ext/2093940080/sp_driver.o.d ${OBJECTDIR}/_ext/2093940080/api.o.d ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d ${OBJECTDIR}/xboot.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/2093940080/fifo.o ${OBJECTDIR}/_ext/2093940080/uart.o ${OBJECTDIR}/_ext/2093940080/watchdog.o ${OBJECTDIR}/_ext/2093940080/flash.o ${OBJECTDIR}/_ext/2093940080/sp_driver.o ${OBJECTDIR}/_ext/2093940080/api.o ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o ${OBJECTDIR}/xboot.o

# Source Files
SOURCEFILES=/Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/fifo.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/uart.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/watchdog.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/flash.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/sp_driver.S /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/api.c /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/eeprom_driver.c xboot.c

# Pack Options 
PACK_COMPILER_OPTIONS=-I "${DFP_DIR}/include"
PACK_COMMON_OPTIONS=-B "${DFP_DIR}/gcc/dev/atxmega32e5"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATxmega32E5
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2093940080/sp_driver.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/sp_driver.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/sp_driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/sp_driver.o 
	@${RM} ${OBJECTDIR}/_ext/2093940080/sp_driver.o.ok ${OBJECTDIR}/_ext/2093940080/sp_driver.o.err 
	 ${MP_CC} $(MP_EXTRA_AS_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -x assembler-with-cpp -c -D__$(MP_PROCESSOR_OPTION)__  -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/sp_driver.o.d" -MT "${OBJECTDIR}/_ext/2093940080/sp_driver.o.d" -MT ${OBJECTDIR}/_ext/2093940080/sp_driver.o -o ${OBJECTDIR}/_ext/2093940080/sp_driver.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/sp_driver.S  -DXPRJ_default=$(CND_CONF)  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2093940080/sp_driver.o.asm.d",--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_SIMULATOR=1
	
else
${OBJECTDIR}/_ext/2093940080/sp_driver.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/sp_driver.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/sp_driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/sp_driver.o 
	@${RM} ${OBJECTDIR}/_ext/2093940080/sp_driver.o.ok ${OBJECTDIR}/_ext/2093940080/sp_driver.o.err 
	 ${MP_CC} $(MP_EXTRA_AS_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x assembler-with-cpp -c -D__$(MP_PROCESSOR_OPTION)__  -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/sp_driver.o.d" -MT "${OBJECTDIR}/_ext/2093940080/sp_driver.o.d" -MT ${OBJECTDIR}/_ext/2093940080/sp_driver.o -o ${OBJECTDIR}/_ext/2093940080/sp_driver.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/sp_driver.S  -DXPRJ_default=$(CND_CONF)  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2093940080/sp_driver.o.asm.d"
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2093940080/fifo.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/fifo.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/fifo.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/fifo.o.d" -MT "${OBJECTDIR}/_ext/2093940080/fifo.o.d" -MT ${OBJECTDIR}/_ext/2093940080/fifo.o  -o ${OBJECTDIR}/_ext/2093940080/fifo.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/fifo.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/uart.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/uart.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/uart.o.d" -MT "${OBJECTDIR}/_ext/2093940080/uart.o.d" -MT ${OBJECTDIR}/_ext/2093940080/uart.o  -o ${OBJECTDIR}/_ext/2093940080/uart.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/uart.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/watchdog.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/watchdog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/watchdog.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/watchdog.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/watchdog.o.d" -MT "${OBJECTDIR}/_ext/2093940080/watchdog.o.d" -MT ${OBJECTDIR}/_ext/2093940080/watchdog.o  -o ${OBJECTDIR}/_ext/2093940080/watchdog.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/watchdog.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/flash.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/flash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/flash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/flash.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/flash.o.d" -MT "${OBJECTDIR}/_ext/2093940080/flash.o.d" -MT ${OBJECTDIR}/_ext/2093940080/flash.o  -o ${OBJECTDIR}/_ext/2093940080/flash.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/flash.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/api.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/api.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/api.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/api.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/api.o.d" -MT "${OBJECTDIR}/_ext/2093940080/api.o.d" -MT ${OBJECTDIR}/_ext/2093940080/api.o  -o ${OBJECTDIR}/_ext/2093940080/api.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/api.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/eeprom_driver.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/eeprom_driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d" -MT "${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d" -MT ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o  -o ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/eeprom_driver.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/xboot.o: xboot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/xboot.o.d 
	@${RM} ${OBJECTDIR}/xboot.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/xboot.o.d" -MT "${OBJECTDIR}/xboot.o.d" -MT ${OBJECTDIR}/xboot.o  -o ${OBJECTDIR}/xboot.o xboot.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/2093940080/fifo.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/fifo.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/fifo.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/fifo.o.d" -MT "${OBJECTDIR}/_ext/2093940080/fifo.o.d" -MT ${OBJECTDIR}/_ext/2093940080/fifo.o  -o ${OBJECTDIR}/_ext/2093940080/fifo.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/fifo.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/uart.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/uart.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/uart.o.d" -MT "${OBJECTDIR}/_ext/2093940080/uart.o.d" -MT ${OBJECTDIR}/_ext/2093940080/uart.o  -o ${OBJECTDIR}/_ext/2093940080/uart.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/uart.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/watchdog.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/watchdog.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/watchdog.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/watchdog.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/watchdog.o.d" -MT "${OBJECTDIR}/_ext/2093940080/watchdog.o.d" -MT ${OBJECTDIR}/_ext/2093940080/watchdog.o  -o ${OBJECTDIR}/_ext/2093940080/watchdog.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/watchdog.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/flash.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/flash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/flash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/flash.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/flash.o.d" -MT "${OBJECTDIR}/_ext/2093940080/flash.o.d" -MT ${OBJECTDIR}/_ext/2093940080/flash.o  -o ${OBJECTDIR}/_ext/2093940080/flash.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/flash.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/api.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/api.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/api.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/api.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/api.o.d" -MT "${OBJECTDIR}/_ext/2093940080/api.o.d" -MT ${OBJECTDIR}/_ext/2093940080/api.o  -o ${OBJECTDIR}/_ext/2093940080/api.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/api.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2093940080/eeprom_driver.o: /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/eeprom_driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2093940080" 
	@${RM} ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d" -MT "${OBJECTDIR}/_ext/2093940080/eeprom_driver.o.d" -MT ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o  -o ${OBJECTDIR}/_ext/2093940080/eeprom_driver.o /Users/michaelliesenberg/LLF_Spinner/Bootloader/XBOOT_BOOTLOADER/XBOOT_BOOTLOADER.X/eeprom_driver.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/xboot.o: xboot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/xboot.o.d 
	@${RM} ${OBJECTDIR}/xboot.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/xboot.o.d" -MT "${OBJECTDIR}/xboot.o.d" -MT ${OBJECTDIR}/xboot.o  -o ${OBJECTDIR}/xboot.o xboot.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atxmega32e5 ${PACK_COMMON_OPTIONS}  -D__MPLAB_DEBUGGER_SIMULATOR=1 -gdwarf-2 -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.map"   -Wl,-section-start=.text=0x8000  -o dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_SIMULATOR=1 -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	
	
	
	
	
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atxmega32e5 ${PACK_COMMON_OPTIONS}  -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.map"   -Wl,-section-start=.text=0x8000  -o dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION) -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	${MP_CC_DIR}/avr-objcopy -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/XBOOT_BOOTLOADER.X.${IMAGE_TYPE}.hex"
	
	
	
	
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
