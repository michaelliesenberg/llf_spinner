/*
 * serialport_2.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <stdbool.h>
#include <string.h>
#include <avr/interrupt.h>
#include <nvm.h>
#include <limits.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "serialport2_PLD.h"
#include "serialport1_PC.h"
#include "boardAdc.h"
#include "main.h"
#include "phy.h"
#include "crcCalc.h"

bool bIsSending = false;

uint8_t aucRcvBuffer[BUFFER_SIZE];

uint8_t ucOtherSideReceivedCounter = 0;

uint16_t uiV50vOtherSide = 0;
uint16_t uiV33vOtherSide = 0;
uint16_t ui_1V8_1V1_OtherSide = 0;
uint16_t uiV25vOtherSide = 0;
uint16_t uiI50vOtherSide = 0;
uint16_t uiI33vOtherSide = 0;
uint16_t uiTemperatureOtherSide = 0;
uint16_t uiVGainOtherSide = 0;
uint16_t uiVGainMinOtherSide = USHRT_MAX;
uint16_t uiVGainMaxOtherSide = 0;
int16_t iMV50vOtherSide = 0;

uint16_t uiVPHYODupplestherSide=0;
uint16_t uiVPHY1DupplestherSide=0;



void vSendWordToOtherSide(uint16_t uiData);
void vSendCharToOtherSide(uint8_t uiData);
void setData(void);
void reset_MCU(void);

void vUART2Init(void) {

    static usart_rs232_options_t usart_2_options = {
        .baudrate = USART_2_BAUDRATE,
        .charlength = USART_2_CHAR_LENGTH,
        .paritytype = USART_2_PARITY,
        .stopbits = USART_2_STOP_BIT,
    };
    ioport_set_pin_dir(USART_2_TX, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(USART_2_RX, IOPORT_DIR_INPUT);
    usart_init_rs232(USART_2, &usart_2_options);
	usart_set_rx_interrupt_level(USART_2, USART_INT_LVL_MED);
	
}

void vSendWordToOtherSide(uint16_t uiData) 
{
    usart_putchar(USART_2, uiData >> 8);
    usart_putchar(USART_2, uiData & 0xFF);
}
void vSendCharToOtherSide(uint8_t uiData)
{
	usart_putchar(USART_2, uiData);
}
void vClearSendBuffer(void) {
    memset(cSendBuffer, 0, SEND_BUFFER_SIZE);
    ucBytesInSendBuffer = 0;
}


void vSendDiagnosticToOtherSide(void) 
{
    if (bIsSending) 
	{
        _delay_us(100);
        if (bIsSending) 
		{
            return;
        }
    }
    bIsSending = true;
	setData();
   
	if(bIsBody)
	{
		memcpy(cCommandBuffer,&IRT_body.BODY, sizeof(IRT_body.BODY) * sizeof(uint8_t));
	}
	else
	{
		memcpy(cCommandBuffer,&IRT_body.HOLLOW, sizeof(IRT_body.HOLLOW) * sizeof(uint8_t));
	}
		
	uint16_t crcHelp =uiCalculateCrc(&cCommandBuffer, sizeof(IRT_body.BODY) * sizeof(uint8_t));
	cCommandBuffer[sizeof(IRT_body.BODY)]=(uint8_t)(crcHelp>>8);
	cCommandBuffer[sizeof(IRT_body.BODY)+1]=(uint8_t)crcHelp;
	ucBytesInCommandBuffer=sizeof(IRT_body.BODY)+2;
   
   	uint8_t ucIndex;
   	for (ucIndex = 0; ucIndex < ucBytesInCommandBuffer; ucIndex++)
   	{
	   	usart_putchar(USART_2,cCommandBuffer[ucIndex]);
   	}
    _delay_ms(1);
    bIsSending = false;


	if((bIsBody && IRT_body.BODY.SW_RESET==1) || (!bIsBody && IRT_body.HOLLOW.SW_RESET==1))
		reset_MCU();
	if(bIsBody)
		IRT_body.BODY.NEW_CONFIGURATION=0;
	else
		IRT_body.HOLLOW.NEW_CONFIGURATION=0;
}
void reset_MCU(void)
{
	CCP = CCP_IOREG_gc;
	RST.CTRL =  RST_SWRST_bm;
}
void setData(void)
{
	if(is_FPGA_Board==0)
		IRT_body.irt_status=TLK;
	else
		IRT_body.irt_status=FPGA;
			
	if(bIsBody)
	{
		IRT_body.irt_type = BODY;
		IRT_body.BODY.GAIN = uiVGain;
		IRT_body.BODY.GAIN_MIN = uiVGainMin;
		IRT_body.BODY.GAIN_MAX = uiVGainMax;
		IRT_body.BODY.VALUE_5V = uiV50v;
		IRT_body.BODY.VALUE_1V1_1V8 = ui_1V8_1V1;
		IRT_body.BODY.VALUE_3V3 = uiV33v;
		IRT_body.BODY.VALUE_n_5V = iMV50v;
		IRT_body.BODY.VALUE_2V5 = uiV25v;
		IRT_body.BODY.VALUE_I_3V3 = uiI33v;
		IRT_body.BODY.VALUE_I_5V = uiI50v;
		IRT_body.BODY.TEMPERATURE = uiTemperature;


		if(PC_body.BODY.PHY0_DUPPLEX==HALF)
			IRT_body.BODY.PHY0_DUPPLEX = HALF;
		else
			IRT_body.BODY.PHY0_DUPPLEX = FULL;
	
		if(PC_body.BODY.PHY1_DUPPLEX==HALF)
			IRT_body.BODY.PHY1_DUPPLEX = HALF;
		else
			IRT_body.BODY.PHY1_DUPPLEX = FULL;
	
		if(PC_body.BODY.LLF_PHY0==ACTIVE)
			IRT_body.BODY.LLF_PHY0 = ACTIVE;
		else
			IRT_body.BODY.LLF_PHY0 = DEACTIVE;
	
		if(PC_body.BODY.LLF_PHY1==ACTIVE)
			IRT_body.BODY.LLF_PHY1 = ACTIVE;
		else
			IRT_body.BODY.LLF_PHY1 = DEACTIVE;
			
		if(IRT_body.BODY.NEW_CONFIGURATION==1)//SEND to the other side
		{
			IRT_body.BODY.PHY0_DUPPLEX = PC_body.HOLLOW.PHY0_DUPPLEX;
			IRT_body.BODY.PHY1_DUPPLEX = PC_body.HOLLOW.PHY1_DUPPLEX;
			IRT_body.BODY.LLF_PHY0 = PC_body.HOLLOW.LLF_PHY0;
			IRT_body.BODY.LLF_PHY1 = PC_body.HOLLOW.LLF_PHY1;
		}
	
		IRT_body.BODY.SW_VERSION_1 = SW_VERSION[0];
		IRT_body.BODY.SW_VERSION_2 = SW_VERSION[1];
		IRT_body.BODY.SW_VERSION_3 = SW_VERSION[2];
		IRT_body.BODY.SW_VERSION_4 = SW_VERSION[3];
		IRT_body.BODY.SW_VERSION_5 = SW_VERSION[4];
		IRT_body.BODY.SW_VERSION_6 = SW_VERSION[5];
		IRT_body.BODY.SW_VERSION_7 = SW_VERSION[6];
		IRT_body.BODY.SW_VERSION_8 = SW_VERSION[7];
			
	}
	else
	{
		IRT_body.irt_type = HOLLOW;
		IRT_body.HOLLOW.GAIN = uiVGain;
		IRT_body.HOLLOW.GAIN_MIN = uiVGainMin;
		IRT_body.HOLLOW.GAIN_MAX = uiVGainMax;
		IRT_body.HOLLOW.VALUE_5V = uiV50v;
		IRT_body.HOLLOW.VALUE_1V1_1V8 = ui_1V8_1V1;
		IRT_body.HOLLOW.VALUE_3V3 = uiV33v;
		IRT_body.HOLLOW.VALUE_n_5V = iMV50v;
		IRT_body.HOLLOW.VALUE_2V5 = uiV25v;
		IRT_body.HOLLOW.VALUE_I_3V3 = uiI33v;
		IRT_body.HOLLOW.VALUE_I_5V = uiI50v;
		IRT_body.HOLLOW.TEMPERATURE = uiTemperature;


		if(PC_body.HOLLOW.PHY0_DUPPLEX==HALF)
			IRT_body.HOLLOW.PHY0_DUPPLEX = HALF;
		else
			IRT_body.HOLLOW.PHY0_DUPPLEX = FULL;
	
		if(PC_body.HOLLOW.PHY1_DUPPLEX==HALF)
			IRT_body.HOLLOW.PHY1_DUPPLEX = HALF;
		else
			IRT_body.HOLLOW.PHY1_DUPPLEX = FULL;
	
		if(PC_body.HOLLOW.LLF_PHY0==ACTIVE)
			IRT_body.HOLLOW.LLF_PHY0 = ACTIVE;
		else
			IRT_body.HOLLOW.LLF_PHY0 = DEACTIVE;
	
		if(PC_body.HOLLOW.LLF_PHY1==ACTIVE)
			IRT_body.HOLLOW.LLF_PHY1 = ACTIVE;
		else
			IRT_body.HOLLOW.LLF_PHY1 = DEACTIVE;
	
		
		IRT_body.HOLLOW.SW_VERSION_1 = SW_VERSION[0];
		IRT_body.HOLLOW.SW_VERSION_2 = SW_VERSION[1];
		IRT_body.HOLLOW.SW_VERSION_3 = SW_VERSION[2];
		IRT_body.HOLLOW.SW_VERSION_4 = SW_VERSION[3];
		IRT_body.HOLLOW.SW_VERSION_5 = SW_VERSION[4];
		IRT_body.HOLLOW.SW_VERSION_6 = SW_VERSION[5];
		IRT_body.HOLLOW.SW_VERSION_7 = SW_VERSION[6];
		IRT_body.HOLLOW.SW_VERSION_8 = SW_VERSION[7];
			
		if(IRT_body.HOLLOW.NEW_CONFIGURATION==1)//SEND to the other side
		{
			IRT_body.HOLLOW.PHY0_DUPPLEX = PC_body.BODY.PHY0_DUPPLEX;
			IRT_body.HOLLOW.PHY1_DUPPLEX = PC_body.BODY.PHY1_DUPPLEX;
			IRT_body.HOLLOW.LLF_PHY0 = PC_body.BODY.LLF_PHY0;
			IRT_body.HOLLOW.LLF_PHY1 = PC_body.BODY.LLF_PHY1;
		}
	}
}

ISR(USART_2_RX_ISR_HANDLER)
{
    if (usart_rx_is_complete(USART_2)) 
	{
		static uint8_t ucOtherSideReceivedByteIndex=0;
        uint32_t ulReceivedByte;
        ulReceivedByte = usart_getchar(USART_2);
		aucRcvBuffer[ucOtherSideReceivedByteIndex] = ulReceivedByte;
		ucOtherSideReceivedByteIndex++;
		
		if(ucOtherSideReceivedByteIndex * sizeof(uint8_t) == ((sizeof(IRT_DATA)* sizeof(uint8_t))+2))
		{
			uint16_t crcHelp =uiCalculateCrc(&aucRcvBuffer, (ucOtherSideReceivedByteIndex * sizeof(uint8_t))-2);
			uint16_t crcRecevied = (uint16_t)(aucRcvBuffer[ucOtherSideReceivedByteIndex-2]<<8) | aucRcvBuffer[ucOtherSideReceivedByteIndex-1];
			if(crcHelp == crcRecevied)
			{
				if(bIsBody)
				{
					memcpy(&IRT_body.HOLLOW,aucRcvBuffer, sizeof(IRT_body.HOLLOW) * sizeof(uint8_t));
				}
				else
				{
					memcpy(&IRT_body.BODY,aucRcvBuffer, sizeof(IRT_body.BODY) * sizeof(uint8_t));
				}
				
				ucOtherSideReceivedByteIndex=0;
				
			
				if((bIsBody && IRT_body.HOLLOW.SW_RESET==1) || (!bIsBody && IRT_body.BODY.SW_RESET==1))
					reset_MCU();//RESET COMMAND FROM OTHERSIDE
			
				if((bIsBody && IRT_body.HOLLOW.NEW_CONFIGURATION==1) || (!bIsBody && IRT_body.BODY.NEW_CONFIGURATION==1))
				{
					if(bIsBody)
					{
						PC_body.BODY.LLF_PHY0=IRT_body.HOLLOW.LLF_PHY0;
						PC_body.BODY.LLF_PHY1=IRT_body.HOLLOW.LLF_PHY1;
						PC_body.BODY.PHY0_DUPPLEX=IRT_body.HOLLOW.PHY0_DUPPLEX;
						PC_body.BODY.PHY1_DUPPLEX=IRT_body.HOLLOW.PHY1_DUPPLEX;
					}
					else
					{
						PC_body.HOLLOW.LLF_PHY0=IRT_body.BODY.LLF_PHY0;
						PC_body.HOLLOW.LLF_PHY1=IRT_body.BODY.LLF_PHY1;
						PC_body.HOLLOW.PHY0_DUPPLEX=IRT_body.BODY.PHY0_DUPPLEX;
						PC_body.HOLLOW.PHY1_DUPPLEX=IRT_body.BODY.PHY1_DUPPLEX;
					}
					PC_header.COMMAND = SET_CONFIG;
					
					PC_body.CRC_Checksum =bReceivedCrcIsCorrectBYTES(&PC_body, ((sizeof(PC_body)*sizeof(uint8_t))-2));
					bCommandBody=true;
				}
				memset(aucRcvBuffer, 0, ucOtherSideReceivedByteIndex);
			}			
		}
    }
}


ISR(USART_2_DRE_ISR_HANDLER)
{
    // USART_Dre(&USART_C0);
}