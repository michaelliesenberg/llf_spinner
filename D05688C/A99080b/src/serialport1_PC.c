/*
 * serialport_1.c
 *
 * Created: 03.01.2018 12:00:12
 *  Author: Eyermann
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <usart.h>
#include <string.h>
#include <stdio.h>
#include <util/atomic.h>

#include "serialport1_PC.h"
#include "serialport2_PLD.h"
#include "boardAdc.h"
#include "gluelogic.h"
#include "crcCalc.h"
#include "nvmManagement.h"
#include "protokollparser.h"
#include "main.h"

bool bFlagDiagMode = false;
bool bFlagEnableDiagMode = false;
bool bFlagEnableOtherSideConfigurationMode = false;

bool bConfigurationMode = false;
bool bOtherSideInConfigurationMode = false;
bool bFlagEnableConfigurationMode = false;
bool bErrorReceivingStruct = false;

bool bCommandHeader = false;
bool bCommandBody = false;

char cCommandBuffer[INPUT_BUFFER_SIZE];
volatile uint8_t ucBytesInCommandBuffer;

char cSendBuffer[BUFFER_SIZE];
volatile uint8_t ucBytesInSendBuffer = 0;

bool bCommandFromLocalSide = true;

bool bSendBufferEnabled = true;

HEADER IRT_header={};
HEADER PC_header={};
IRT_BODY IRT_body={};
PC_BODY PC_body={};
PC_BODY	PC_body_UART={};

RECEIVE_TYPE receive_type = RECEIVE_HEADER;

uint16_t crcHelper=0;

void vEnableSendBuffer(void) {
    bSendBufferEnabled = true;
}

void vDisableSendBuffer(void) {
    bSendBufferEnabled = false;
}


void vAddCharToSendBuffer(char cChar, bool bAddCrc) {
    usart_serial_putchar(USART_1, cChar);
    if (!bSendBufferEnabled || ucBytesInSendBuffer >= BUFFER_SIZE) {
        return;
    }
    cSendBuffer[ucBytesInSendBuffer] = cChar;
    ucBytesInSendBuffer++;
    if (bAddCrc && cChar != ' ') {
        crc_io_checksum_byte_add(cChar);
    }
}


void vUART1EnableTx(void) {
    ioport_set_pin_dir(USART_1_TX, IOPORT_DIR_OUTPUT);
}

void vUART1Init(void) {
	// ---------------------------------------------------------------
	// config UART1
	// ---------------------------------------------------------------
	static usart_serial_options_t usart_1_options = {
    	.baudrate = USART_1_BAUDRATE,
    	.charlength = USART_1_CHAR_LENGTH,
    	.paritytype = USART_1_PARITY,
    	.stopbits = USART_1_STOP_BIT
	};

    ioport_set_pin_dir(USART_1_TX, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(USART_1_RX, IOPORT_DIR_INPUT);
	usart_init_rs232(USART_1, &usart_1_options);
	usart_set_rx_interrupt_level(USART_1, USART_INT_LVL_LO);
	
	
}



ISR(USART_1_RX_ISR_HANDLER)
{
	//static uint8_t ucDiagEnableCounter = 0;
    static uint8_t ucReceiverIndex = 0;
    static uint8_t ucErrorCounter = 0;
    static char cInputBuffer[INPUT_BUFFER_SIZE];

    if (usart_rx_is_complete(USART_1)) 
	{
        volatile uint32_t ulReceivedByte;
        ulReceivedByte = usart_getchar(USART_1);
		//usart_putchar(USART_1,ulReceivedByte);

        cInputBuffer[ucReceiverIndex] = ulReceivedByte;
        ucReceiverIndex++;
		
		if(ucReceiverIndex * sizeof(uint8_t) == (sizeof(HEADER)* sizeof(uint8_t))  && receive_type == RECEIVE_HEADER)
		{
			//memcpy(&PC_header,cInputBuffer, ucReceiverIndex * sizeof(uint8_t));
			PC_header.COMMAND= cInputBuffer[0];
			PC_header.RESPONSE= cInputBuffer[1];
			PC_header.VERSION=   (uint16_t)((uint16_t)(cInputBuffer[2])<<8 | (uint8_t)cInputBuffer[3]);
			PC_header.SIZEBYTES= (uint16_t)((uint16_t)(cInputBuffer[4])<<8 | (uint8_t)cInputBuffer[5]);
			PC_header.CRC_CHECK= (uint16_t)((uint16_t)(cInputBuffer[6])<<8 | (uint8_t)cInputBuffer[7]);
				
			
			crcHelper = bReceivedCrcIsCorrectBYTES(&cInputBuffer, (sizeof(HEADER)* sizeof(uint8_t))-2);
			if(bReceivedCrcIsCorrectBYTES(&cInputBuffer, (sizeof(HEADER)* sizeof(uint8_t))-2) == PC_header.CRC_CHECK)
			{
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
				{					
					if(PC_header.COMMAND==SET_CONFIG)
						receive_type = RECEIVE_BODY;
					else
						bCommandHeader=true;
						
					bFlagUART_MODE=true;//Disable PHYs
				}
			}
			else
			{
				bErrorReceivingStruct=true;
			}
			ucReceiverIndex=0;
			memset(cInputBuffer, 0, INPUT_BUFFER_SIZE);
		}
		else if(ucReceiverIndex * sizeof(uint8_t) == (sizeof(PC_BODY) * sizeof(uint8_t))  && receive_type == RECEIVE_BODY)
		{
			memcpy(&PC_body_UART,cInputBuffer, ucReceiverIndex * sizeof(uint8_t));
			PC_body_UART.CRC_Checksum= (uint16_t)((uint16_t)(cInputBuffer[10])<<8 | (uint8_t)cInputBuffer[11]);
						
						
			if(bReceivedCrcIsCorrectBYTES(&cInputBuffer, sizeof(PC_BODY) * sizeof(uint8_t)-2) == PC_body_UART.CRC_Checksum)
			{
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
				{
					memcpy(&PC_body,&PC_body_UART, sizeof(PC_BODY) * sizeof(uint8_t));
					bCommandBody=true;
					
					if(bIsBody)
						IRT_body.BODY.NEW_CONFIGURATION=1;
					else
						IRT_body.HOLLOW.NEW_CONFIGURATION=1;
				}
			}
			else
			{
				bErrorReceivingStruct=true;
			}
			ucReceiverIndex=0;
			receive_type = RECEIVE_HEADER;
			memset(cInputBuffer, 0, INPUT_BUFFER_SIZE);
		}
	}
}


ISR(USART_1_DRE_ISR_HANDLER)
{
    // USART_Dre(&USART_C0);
}