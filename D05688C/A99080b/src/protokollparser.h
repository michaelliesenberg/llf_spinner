


#ifndef PROTOKOLLPARSER_H_
#define PROTOKOLLPARSER_H_

#include "serialport1_PC.h"
#include "main.h"


void vScanCommandLine(void);
void vAddStringLenToSendBuffer(const char *cBuffer, uint8_t ucLength, bool bAddCrc);

#endif /* PROTOKOLLPARSER_H_ */

