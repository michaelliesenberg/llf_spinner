


#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>
#include <stdio.h>
#include <twi_master.h>

#include "externalclock.h"
#include "config/hw_config_data.h"
#include "main.h"

#define CDCE_BYTE_OPERATION 0x80
#define CDCE_EEPIP          0x40


bool bClockChipVerifyData(void);
void vWriteByteClockChip(uint8_t ucAddress, uint8_t ucData);
uint8_t ucReadByteClockChip(uint8_t ucRegisterAddress);
bool bClockChipWriteConfigToEeprom(void);
void vClockChipConfig(void);

bool bExternalClockChipSetup(void) {
    bool result = bClockChipVerifyData();
    if (result) {
        return (true);
    } else {
		vClockChipConfig();
		
		result = bClockChipVerifyData();
		
		if (result) {		
		
            _delay_ms(100);
            result = bClockChipWriteConfigToEeprom();
            if (result) {
                return (true);
            }
		}
    }
    return (false);
}

uint8_t ucExpectedData;
uint8_t ucCounter;
uint8_t ucRegisterAddress;
uint8_t ucReceived;
bool bClockChipVerifyData(void) {
    // verifies clock chip configuration   


    for(ucCounter = 0; ucCounter < CDCE_REGISTER_DATA_LENGTH; ucCounter++) {

        ucRegisterAddress = uiClockChipRegAddr[ucCounter];                    
		
        if (ioport_get_pin_level(BODY_SELECT) == IS_BODY) {
            ucExpectedData = uiClockChipPllRegData[ucCounter];
        } else {
            ucExpectedData = uiClockChipVcxoRegData[ucCounter];
        }

			
		ucReceived = ucReadByteClockChip(ucRegisterAddress);
		
        if (ucReceived != ucExpectedData) {                
            return(false);
        }                
        _delay_ms(10);
    }    
    return true;
}    

uint8_t ucReadByteClockChip(uint8_t ucRegisterAddress) {
    uint8_t ucReceived;
	ucRegisterAddress |= CDCE_BYTE_OPERATION;

    twi_package_t sPacketRead = {
	    .addr         = {0},                  // TWI slave memory address data
	    .addr_length  = sizeof (uint8_t),   // TWI slave memory address data size
	    .chip         = CDCE_ADDR,          // TWI slave bus address
	    .buffer       = &ucReceived,        // transfer data destination buffer
	    .length       = sizeof(ucReceived)  // transfer data size (bytes)
    };

    sPacketRead.addr[0] = ucRegisterAddress;

    // Perform a multi-byte read access then check the result.
    if(twi_master_read(CDCE_PORT, &sPacketRead) != TWI_SUCCESS){
	    return(0);
    }
	return (ucReceived);
}

void vWriteByteClockChip(uint8_t ucAddress, uint8_t ucData){
    // Writes the clock configuration to the clock EEPROM	
	ucData |= CDCE_BYTE_OPERATION;

    twi_package_t packet_write = {
        .addr         = {ucAddress},        // TWI slave memory address data
        .addr_length  = sizeof (uint8_t),   // TWI slave memory address data size
        .chip         = CDCE_ADDR,          // TWI slave bus address
        .buffer       = (void *)&ucData,    // transfer data source buffer
        .length       = sizeof(ucData)      // transfer data size (bytes)
    };

    twi_master_write(CDCE_PORT, &packet_write);	
}

void vClockChipConfig(void) {
	uint16_t uiCounter = 0;	
	uint8_t ucSendData;
	
    for(uiCounter = 0; uiCounter < sizeof(uiClockChipRegAddr); uiCounter++)	{
		uint8_t ucAddress;
        ucAddress = uiClockChipRegAddr[uiCounter];
		if (ioport_get_pin_level(BODY_SELECT) == IS_BODY) {
			ucSendData = uiClockChipPllRegData[uiCounter];
		} else {
			ucSendData = uiClockChipVcxoRegData[uiCounter];		
		}

			
		vWriteByteClockChip(ucAddress, ucSendData);
		_delay_ms(10);
	}
}



bool bClockChipWriteConfigToEeprom(void){
    // Initiates writing data from clock chip RAM into clock chip EEPROM
	// monitors writing to EEPROM; then resets the write flag
    uint8_t ucReceived;    
	uint8_t ucTimeoutCounter = 0;
    
	vWriteByteClockChip(CDCE_GENERIC_06, CDCE_GENERIC_06_EEWRITE);
	_delay_ms(10);
		
	ucReceived = ucReadByteClockChip(CDCE_GENERIC_01);	
    // Reset EE Write Bit
    while ((ucReceived & CDCE_EEPIP) == CDCE_EEPIP) {
        // in programming mode
		ucTimeoutCounter++;
		_delay_ms(10);
		ucReceived = ucReadByteClockChip(CDCE_GENERIC_01);
		if (ucTimeoutCounter > 100) {
            return false;
		}
    }	
	_delay_ms(10);
	vWriteByteClockChip(CDCE_GENERIC_06, CDCE_GENERIC_06_EEWRITE_RESET);
	
	_delay_ms(10);
    return true;
}



void vClockChipBusInit(void) {
    static twi_master_options_t twi_master_options = {
        .speed = CDCE_SPEED,
        .chip = CDCE_ADDR
    };
    
    twi_master_setup(CDCE_PORT, &twi_master_options);
}