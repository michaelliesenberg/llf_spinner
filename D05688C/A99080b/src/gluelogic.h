/*
 * gluelogic.h
 *
 * Created: 09.01.2018 10:44:00
 *  Author: Eyermann
 */ 


#ifndef GLUELOGIC_H_
#define GLUELOGIC_H_

void vEnableNDiagTx(void);
void vDisableNDiagTx(void);
void vGlueLogicInit(void);

#endif /* GLUELOGIC_H_ */