/*
 * phy.h
 *
 * Created: 04.01.2018 07:58:42
 *  Author: Eyermann
 */ 


#ifndef PHY_H_
#define PHY_H_

#define MAX_PHY_ADDR            0x1F
#define MAX_PHY_REGISTER_ADDR   0x1F

#define PHY0_ADDR               0x03
#define PHY1_ADDR               0x01


#define MDIO_FRAME_READY_READ   0x6000
#define MDIO_FRAME_READY_WRITE  0x5002

#define PHY_ADDR_ANAR           0x04


#define PHY_ANAR_100BT_ONLY     0x0101

#define PHY_ADDR_BMCR			0x0000
#define PHY_BMCR_POWER_DOWN		0x0800
#define PHY_BMCR_POWER_UP		0xF7FF

#define PHY_ADDR_BMSR			0x0001


extern bool bLLFPHY0;
extern bool bLLFPHY1;
extern bool bLLFPHY0_OtherSide;
extern bool bLLFPHY1_OtherSide;

extern bool bLLF_PHY0_Active;
extern bool bLLF_PHY0_ActiveOtherSide;
extern bool bLLF_PHY1_Active;
extern bool bLLF_PHY1_ActiveOtherSide;

bool bPhyPowerDown(uint8_t ucPhy);
bool bPhyPowerUp(uint8_t ucPhy);
uint16_t uiMdioReadRegister(uint8_t ucPhy, uint8_t ucAddress);
void vMdioWriteRegister(uint8_t ucPhy, uint8_t ucAddress, uint16_t uiData);
void bPhyCheckStatus(void);
bool bInitializePhy(void);
extern uint16_t uiDataGlobal;
extern uint16_t uiDataGlobal1;
#endif /* PHY_H_ */