

#ifndef CRC_CALC_H_
#define CRC_CALC_H_

bool bWaitForCrcFree(void);
void vReleaseCrc(void);


bool bReceivedCrcIsCorrect(uint8_t * pcBuffer, uint8_t ucCRCIndex);
uint16_t uiCalculateEepromCrc(eeprom_addr_t uiIndex);
uint16_t uiCalculateCrc(uint8_t * pcBuffer, uint8_t ucCRCIndex); 
uint16_t bReceivedCrcIsCorrectBYTES(uint8_t * pcBuffer, uint8_t ucCRCIndex);

#endif /* CRC_CALC_H_ */