import QtQuick 2.13
import QtQuick.Controls 1.4
import QtQuick.Controls 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.13
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.0

Window
{
    id: mainWindow
    title: ApplicationName

    property int myX: Screen.width/2 - width/2
    property int myY: Screen.height/2 - height/2
    property int myH: Screen.height *0.7
    property int myW: Screen.height *0.5
    property int indexValue:0
    property string textColor: "#f6f6f6"
    property string backgroundColor: "#606365"
    property var portName: ""
    property var urlpath: ""
    property var connected_to: "none"
    property var sideInBootloader: false
    property string spinnerColor: "#00AFE5"

    width: 800
    minimumWidth: 800
    maximumWidth: 800


    height: 700
    minimumHeight: 700
    maximumHeight: 700

    visible: true


    property int radioHeight: mainWindow.height*0.03
    property int sourceSizeImageWidth:700
    property int sourceSizeImageHeigh:200
    property var config: ['F', 'F', 'A', 'A', 'F', 'F', 'D', 'D']
    property var configuration: Bridge.irtCongfiguration //[H_PHY0, H_PHY1, H_LLF0, H_LLF1, B_PHY0, B_PHY1, B_LLF0, B_LLF1]
    property var comport : Bridge.listComPortValues
    property var stringUpdate : Bridge.stringUpdate
    property var flag_Update_Otherside: false
    property var flag_Update_side: false
    property var flag_is_firmware_Updatable: Bridge.isUpdatable
    property var flag_firmware_Recovery: false
    property int click_enable_update_buttons: 0
    property int click_enable_recovery: 0
    property var command_update: ""
    property var command_button_start: false
    property var textDiagnosis: ""

    function get_name(side)
    {
        var name=""
        if(side === "Otherside")
        {
            /*if(connected_to==="Body")
                name="Hollow"
            else if(connected_to==="Hollow")
                name="Body"
            else
                name="Otherside"*/
            name = "Remote"
        }
        else if(side === "Side")
        {
            /*if(connected_to==="Body")
                name="Body"
            else if(connected_to==="Hollow")
                name="Hollow"
            else
                name="Side"*/
            name = "Connected"
        }
        return name
    }
    onStringUpdateChanged:
    {
        if(flag_Update_Otherside && stringUpdate !== "Update Success" && stringUpdate !== "Update Failed")
            //updateText_otherside.text = get_name("Otherside")+" "+stringUpdate
            updateText_otherside.text = "Remote"+" "+stringUpdate
        else if(flag_Update_side && stringUpdate !== "Update Success" && stringUpdate !== "Update Failed")
            //updateText_side.text = get_name("Side")+" "+stringUpdate
            updateText_side.text = "Connected"+" "+stringUpdate

        if(stringUpdate.includes("%"))
        {
            var substring = stringUpdate.substr(-4,3)

            if(substring.includes("g"))//Workaround
                substring = substring.substr(1,2)

            var value = parseFloat(substring)

            if(value > 100)
                value=100

            if(flag_Update_Otherside)
                progressbarupdate_otherside.value= value/100
            else if(flag_Update_side)
                progressbarupdate_side.value= value/100

        }
        else
        {
            if(stringUpdate==="Update Success")
            {
                if(flag_Update_Otherside)
                {
                    updateText_otherside_info.visible=true
                    updateText_otherside_info.text="OK"

                    progressbarupdate_otherside.value=1
                    flag_Update_Otherside=false

                    if(command_update=="both")
                    {
                        flag_Update_side=true
                        progressbarupdate_side.value=0
                        command_update="normal"

                        startUpdate()
                    }
                    else if(command_update=="otherside")
                    {
                        flag_Update_side=false
                        progressbarupdate_side.value=0
                    }
                }
                else if(flag_Update_side)
                {
                    updateText_side_info.visible=true
                    updateText_side_info.text="OK"
                    progressbarupdate_side.value=1
                    flag_Update_side=false
                }
                activateButton("Update Success")
            }
            else if(stringUpdate==="Update Failed" || stringUpdate==="IRT not in Bootloader")
            {
                if(flag_Update_Otherside)
                {
                    progressbarupdate_otherside.value=0
                    updateText_otherside_info.visible=true
                    updateText_otherside_info.text="Failed"
                    flag_Update_Otherside=false
                }
                else if(flag_Update_side)
                {
                    progressbarupdate_side.value=0
                    updateText_side_info.visible=true
                    updateText_side_info.text="Failed"
                    flag_Update_side=false
                }
                activateButton(stringUpdate)
            }
            else if(stringUpdate==="Starting Update")
            {
                if(flag_Update_Otherside)
                {
                    progressbarupdate_otherside.value=0
                    updateText_otherside.color=textColor
                }
                else if(flag_Update_side)
                {
                    progressbarupdate_side.value=0
                    updateText_side.color=textColor
                }
                activateButton(stringUpdate)
            }
        }
    }
    onComportChanged:
    {
        var i=0
        if(comport.length>=1)
        {
            contactModel.clear()
            for(i=0; i< comport.length; i++)
                contactModel.append({"name":comport[i]})

            if(!comPortDialog.visible)
            {
                comPortDialog.open()
                updateDialog.close()
                fileDialog.close()
                listviewPort.currentIndex=-1
            }
        }
    }
    onConfigurationChanged:
    {
        var configuration = Bridge.irtCongfiguration

        if(configuration.length >= 8)
        {
            if(configuration[0]==="FULL")
            {
                rb_duplex_PHY0_FULL.checked=true
                rb_duplex_PHY0_HALF.checked=false
            }
            else if(configuration[0]==="HALF")
            {
                rb_duplex_PHY0_FULL.checked=false
                rb_duplex_PHY0_HALF.checked=true
            }
            else
            {
                if(configuration[4]==="FULL")
                {
                    rb_duplex_PHY0_FULL.checked=true
                    rb_duplex_PHY0_HALF.checked=false
                }
                else if(configuration[4]==="HALF")
                {
                    rb_duplex_PHY0_FULL.checked=false
                    rb_duplex_PHY0_HALF.checked=true
                }
                else
                {
                    console.log("Couldnt parse anything 1")
                }
            }

            if(configuration[1]==='FULL')
            {
                rb_duplex_PHY1_FULL.checked=true
                rb_duplex_PHY1_HALF.checked=false
            }
            else if(configuration[1]==='HALF')
            {
                rb_duplex_PHY1_FULL.checked=false
                rb_duplex_PHY1_HALF.checked=true
            }
            else
            {
                if(configuration[5]==="FULL")
                {
                    rb_duplex_PHY1_FULL.checked=true
                    rb_duplex_PHY1_HALF.checked=false
                }
                else if(configuration[5]==="HALF")
                {
                    rb_duplex_PHY1_FULL.checked=false
                    rb_duplex_PHY1_HALF.checked=true
                }
                else
                {
                    console.log("Couldnt parse anything 2")
                }
            }

            if(configuration[2]==="ACTIVE" && configuration[6]==="DEACTIVE")
            {
                rb_llf_PHY0_Hollow.checked=true
                rb_llf_PHY0_Body.checked=false
                rb_llf_PHY0_Deactivated.checked=false
            }
            else if(configuration[2]==="DEACTIVE" && configuration[6]==="ACTIVE")
            {
                rb_llf_PHY0_Hollow.checked=false
                rb_llf_PHY0_Body.checked=true
                rb_llf_PHY0_Deactivated.checked=false
            }
            else if(configuration[2]==='DEACTIVE' && configuration[6]==="DEACTIVE")
            {
                rb_llf_PHY0_Hollow.checked=false
                rb_llf_PHY0_Body.checked=false
                rb_llf_PHY0_Deactivated.checked=true
            }


            if(configuration[3]==="ACTIVE" && configuration[7]==='DEACTIVE')
            {
                rb_llf_PHY1_Hollow.checked=true
                rb_llf_PHY1_Body.checked=false
                rb_llf_PHY1_Deactivated.checked=false
            }
            else if(configuration[3]==="DEACTIVE" && configuration[7]==="ACTIVE")
            {
                rb_llf_PHY1_Hollow.checked=false
                rb_llf_PHY1_Body.checked=true
                rb_llf_PHY1_Deactivated.checked=false
            }
            else if(configuration[3]==="DEACTIVE" && configuration[7]==="DEACTIVE")
            {
                rb_llf_PHY1_Hollow.checked=false
                rb_llf_PHY1_Body.checked=false
                rb_llf_PHY1_Deactivated.checked=true
            }
            else
            {
                console.log("Couldnt parse anything 3"+configuration[3]+" "+configuration[7])
            }
        }
    }

    Rectangle
    {
        anchors.fill: parent
        anchors.centerIn: parent
        color: backgroundColor
    }


    GridLayout
    {
        id: mainGrid
        width:parent.width*0.90
        height: parent.height*0.90
        columns: 4
        rows: 8
        anchors.centerIn: parent

        Rectangle
        {
            color: "transparent"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 4
            Layout.rowSpan: 2
            Layout.preferredHeight: parent.height * 0.25

            Image
            {
                id: mainImage
                source: "SPINNER_IRT.jpg"
                anchors.fill: parent

                Image
                {
                    id: mainLogo
                    source: "spinner_logo.png"
                    height: mainImage.paintedHeight*0.2
                    width: mainImage.paintedWidth*0.2
                    fillMode: Image.PreserveAspectFit
                    anchors.bottom: parent.bottom
                    x: mainImage.width - mainImage.paintedWidth
                    anchors.top: parent.top
                }
            }
        }


        Rectangle
        {
            radius: 5
            color: "transparent"
            border.width: 1
            border.color:"black"
            Layout.columnSpan: 2
            Layout.rowSpan: 4
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: parent.height * 0.65

            Row
            {
                width:parent.width
                height: parent.height
                anchors.centerIn: parent

                Rectangle
                {
                    color: "lightgrey"
                    opacity: 0.1
                    width: parent.width/2
                    height: parent.height
                }
                Rectangle
                {
                    color: "white"
                    opacity: 0.2
                    width: parent.width/2
                    height: parent.height
                }
            }
            GridLayout
            {
                id: myGrid
                anchors.centerIn: parent
                width: parent.width //* 0.9
                height: parent.height * 0.9
                columns: 4

                Text
                {
                    text: qsTr("ETH1")
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    font.pixelSize: rb_llf_PHY0_Hollow.indicator.height * 0.8
                    fontSizeMode: Text.Fit
                    minimumPixelSize: 1
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    color: textColor
                }
                Text
                {
                    text: qsTr("ETH2")
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    font.pixelSize: rb_llf_PHY0_Hollow.indicator.height * 0.8
                    fontSizeMode: Text.Fit
                    minimumPixelSize: 1
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    color: textColor
                }
                Rectangle
                {
                    color: textColor
                    border.color: "black"
                    radius: 4
                    Layout.fillWidth: true
                    Layout.columnSpan: 4
                    height: duplexText.height + 10

                    Text
                    {
                        id: duplexText
                        text: qsTr("Duplex Mode")
                        width: parent.width
                        font.pixelSize: rb_llf_PHY0_Hollow.indicator.height * 0.8
                        fontSizeMode: Text.Fit
                        minimumPixelSize: 1
                        horizontalAlignment: Text.AlignHCenter
                        anchors.verticalCenter: parent.verticalCenter
                        color: "black"
                    }
                }

                Rectangle
                {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    Layout.preferredWidth: parent.width/2
                    color: "transparent"
                    ColumnLayout
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        spacing: 0
                        RadioButtonCustom
                        {
                            id: rb_duplex_PHY0_FULL
                            checked: true
                            text: qsTr("Full")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[0]='F'
                                config[4]='F'
                            }
                        }
                        RadioButtonCustom
                        {
                            id: rb_duplex_PHY0_HALF
                            text: qsTr("Half")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[0]='H'
                                config[4]='H'
                            }
                        }
                    }
                }
                Rectangle
                {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    Layout.preferredWidth: parent.width/2
                    color: "transparent"

                    ColumnLayout
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        spacing: 0

                        RadioButtonCustom
                        {
                            id: rb_duplex_PHY1_FULL
                            checked: true
                            text: qsTr("Full")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[1]='F'
                                config[5]='F'
                            }
                        }
                        RadioButtonCustom
                        {
                            id: rb_duplex_PHY1_HALF
                            text: qsTr("Half")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[1]='H'
                                config[5]='H'
                            }
                        }
                    }
                }
                Rectangle
                {
                    color: textColor
                    border.color: "black"
                    radius: 4
                    Layout.fillWidth: true
                    Layout.columnSpan: 4
                    height: llfText.height + 10

                    Text
                    {
                        id: llfText
                        text: qsTr("Link Loss Forwarding")
                        width: parent.width
                        font.pixelSize: rb_llf_PHY0_Hollow.indicator.height * 0.8
                        fontSizeMode: Text.Fit
                        minimumPixelSize: 1
                        horizontalAlignment: Text.AlignHCenter
                        anchors.verticalCenter: parent.verticalCenter
                        color: "black"
                    }
                }

                Rectangle
                {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    Layout.preferredWidth: parent.width/2
                    color: "transparent"

                    ColumnLayout
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        spacing: 0

                        RadioButtonCustom
                        {
                            id: rb_llf_PHY0_Hollow
                            checked: true
                            text: qsTr("Hollow Shaft")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[2]='A'
                                config[6]='D'
                            }
                        }
                        RadioButtonCustom
                        {
                            id: rb_llf_PHY0_Body
                            text: qsTr("Body")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[2]='D'
                                config[6]='A'
                            }
                        }
                        RadioButtonCustom
                        {
                            id: rb_llf_PHY0_Deactivated
                            text: qsTr("Deactivated")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[2]='D'
                                config[6]='D'
                            }
                        }
                    }
                }
                Rectangle
                {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    Layout.preferredWidth: parent.width/2
                    color: "transparent"
                    ColumnLayout
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        spacing: 0

                        RadioButtonCustom
                        {
                            id: rb_llf_PHY1_Hollow
                            checked: true
                            text: qsTr("Hollow Shaft")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[3]='A'
                                config[7]='D'
                            }
                        }
                        RadioButtonCustom
                        {
                            id: rb_llf_PHY1_Body
                            text: qsTr("Body")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[3]='D'
                                config[7]='A'
                            }
                        }
                        RadioButtonCustom
                        {
                            id: rb_llf_PHY1_Deactivated
                            text: qsTr("Deactivated")
                            width: parent.width*0.9
                            height: parent.height/4
                            onCheckedChanged:
                            {
                                config[3]='D'
                                config[7]='D'
                            }
                        }
                    }
                }
            }
        }

        Rectangle
        {
            radius: 5
            color: "transparent"
            border.width: 1
            border.color:"black"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: parent.height * 0.65
            Layout.columnSpan: 2
            Layout.rowSpan: 4

            GridLayout
            {
                anchors.centerIn: parent
                width: parent.width * 0.9
                height: parent.height * 0.9
                columns: 1
                columnSpacing: 0
                rowSpacing: 0

                Row
                {
                    Layout.fillWidth: true
                    spacing: 10
                    Text
                    {
                        text: qsTr("Connection:")
                        color: textColor
                    }
                    Text
                    {
                        id: connectionText
                        text: Bridge.connection
                        font.bold: true
                        color:
                        {
                            if(text=="Connected")
                                 return spinnerColor
                            else
                                return textColor
                        }
                        onTextChanged:
                        {
                            if(text=="Connected")
                            {
                                comPortDialog.close()
                                textSearch.text=qsTr("Reading diagnosis...")

                                if(!flag_Update_Otherside && !flag_Update_side)
                                {
                                    Bridge.get_configuration()
                                    timeDiagnosis.start()
                                }
                            }
                            else
                            {
                                connectionIndicator.visible=true
                                gridDisplay.visible=false
                                textDisplay.visible=false
                                textSearch.text=qsTr("Searching for device...")
                                timeDiagnosis.stop()
                                buttonUpdate.visible=false
                                buttonExport.visible=false
                            }
                        }
                        onVisibleChanged:
                        {
                            if(visible===false)
                                timeDiagnosis.stop()
                        }
                        MouseArea
                        {
                            anchors.fill: parent
                            onClicked:
                            {
                                click_enable_update_buttons++

                                if(click_enable_update_buttons >= 3)
                                {
                                    click_enable_update_buttons=0;
                                    buttonUpdate.visible=true

                                    updateTextID.text = "Recovery"
                                    flag_firmware_Recovery = true
                                    button_both.visible=false

                                    connected_to="none"
                                    timeDiagnosis.stop()
                                }
                            }
                        }
                    }
                }
                Rectangle
                {
                    id: displayValues
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "black"
                    radius: 5

                    property var arr : Bridge.listStringValues

                    onArrChanged:
                    {
                        if(flag_is_firmware_Updatable)
                            buttonUpdate.visible=true
                        else
                            buttonUpdate.visible=false

                        var i=0
                        var inBootloader=false

                        if(arr.length>1)
                        {
                            textDiagnosis=" "

                            for(i=0; i< arr.length; i++)
                            {
                                textRepeater.itemAt(i).text=arr[i]

                                if(arr[i] === "Connected to")
                                {
                                    if(arr[i+1]==="X")
                                        connected_to="Body"
                                    else if(arr[i+2]==="X")
                                        connected_to="Hollow"
                                    else
                                        connected_to="none"
                                }

                                if(arr[i] === "In Bootloader")
                                    inBootloader=true

                                switch(indexValue)
                                {
                                    case 0:
                                        textRepeater.itemAt(i).color = "#FFFFFF"
                                        textDiagnosis = textDiagnosis + arr[i]+","
                                        break
                                    case 1:
                                        textRepeater.itemAt(i).color = "#6495ed"
                                        textDiagnosis = textDiagnosis + arr[i]+","
                                        break
                                    case 2:
                                        textRepeater.itemAt(i).color = "#FF0000"
                                        textDiagnosis = textDiagnosis + arr[i]+"\n "
                                        break
                                }

                                indexValue+=1
                                if(indexValue===3)
                                    indexValue=0
                            }

                            if(inBootloader===false)
                            {
                                flag_firmware_Recovery=false //Leave recovery modus
                                //button_both.visible=true
                                updateTextID.text = "Update"
                                buttonExport.visible=true
                            }
                            else
                            {
                                flag_firmware_Recovery=true
                                button_both.visible=false
                                updateTextID.text = "Recovery"
                                buttonExport.visible=false
                            }


                            gridDisplay.visible=true
                            textDisplay.visible=false
                            connectionIndicator.visible=false
                            timeDiagnosis.stop()
                        }
                    }
                    Text
                    {
                        id: textDisplay
                        visible: false
                        anchors.centerIn: parent
                        width:parent.width*0.9
                        height: parent.height*0.5
                        text: Bridge.uart_message
                        horizontalAlignment: Text.AlignHCenter
                        //verticalAlignment: Text.AlignVCenter
                        onTextChanged:
                        {
                            gridDisplay.visible=false
                            textDisplay.visible=true

                            if(text.includes("Values transmitted are invalid"))
                            {
                                if(flag_Update_side)
                                {
                                    updateText_otherside_info.visible=true
                                    updateText_otherside_info.text="OK"
                                    progressbarupdate_side.value=0
                                    command_update=="normal"
                                    startUpdate();
                                }
                            }
                            else if(text.includes("IRT is in Bootloader"))
                            {
                                connectionIndicator.visible=false
                                flag_firmware_Recovery=true
                                button_both.visible=false
                                button_otherside.visible=false
                                updateTextID.text = "Recovery"
                                buttonExport.visible=false
                                sideInBootloader=true
                            }
                            else
                                sideInBootloader=false
                        }
                        color:"white"
                        fontSizeMode: Text.Fit
                        minimumPointSize: 1
                    }
                    GridLayout
                    {
                        id: gridDisplay
                        columns: 3
                        anchors.centerIn: parent
                        width:parent.width*0.9
                        height: parent.height*0.9
                        visible: false

                        Repeater
                        {
                            model: 57
                            id: textRepeater
                            Text
                            {
                                text: ""
                                fontSizeMode: Text.FixedSize
                                font.pixelSize: 12
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                color: "white"
                            }
                        }
                    }
                    Rectangle
                    {
                        id: connectionIndicator
                        visible: true
                        width: displayValues.width*0.5
                        height: displayValues.height*0.5
                        anchors.centerIn: parent
                        color: "transparent"

                        BusyIndicator
                        {
                            id: indicatorID
                            anchors.centerIn: parent
                            running: parent.visible
                        }
                        Text
                        {
                            id: textSearch
                            anchors.horizontalCenter: indicatorID.horizontalCenter
                            anchors.top: indicatorID.bottom
                            anchors.topMargin: 10
                            text: qsTr("Searching for device...")
                            color: "white"
                        }
                    }
                }
            }
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"
            Button
            {
                id: button1
                text: "Send Configuration"
                anchors.fill:parent

                background:
                Rectangle
                {
                    color: button1.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }
                visible:
                {
                    if(connectionText.text=="Connected")
                        return true
                    else
                        return false
                }

                onClicked:
                {
                    stopAutoConnection()
                    Bridge.send_configuration(config)
                }
            }
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"
            Button
            {
                id: button2
                text: "Get Configuration"
                anchors.fill: parent
                background:
                Rectangle
                {
                    color: button2.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }
                visible:
                {
                    if(connectionText.text=="Connected")
                        return true
                    else
                        return false
                }
                onClicked:
                {
                    stopAutoConnection()
                    Bridge.get_configuration()
                }
            }
        }
         Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"
            Button
            {
                id: button3
                text: "Reset IRT"
                anchors.fill: parent

                background:
                Rectangle
                {
                    color: button3.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }
                visible:
                {
                    if(connectionText.text=="Connected")
                        return true
                    else
                        return false
                }
                onClicked:
                {
                    stopAutoConnection()
                    Bridge.reset_IRT()
                }
            }
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"
            Button
            {
                id: button4
                //Layout.fillWidth: true
                anchors.fill: parent
                text: "Help"
                background:
                Rectangle
                {
                    color: button4.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }

                onClicked:
                {
                    var ret = false

                    if(Qt.platform.os === "windows")
                        ret = Qt.openUrlExternally("file:\\"+pathToPDF);
                    else
                        ret = Qt.openUrlExternally("file://"+pathToPDF);

                    console.log(Qt.platform.os)
                    if(ret === false)
                    {
                        pdfDialog.open()
                    }
                }
            }
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"

            Button
            {
                id: buttonUpdate
                anchors.fill: parent
                text: "Update Firmware"
                background:
                Rectangle
                {
                    color: buttonUpdate.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }
                visible:
                {
                    if(connectionText.text=="Connected" && flag_is_firmware_Updatable)
                        return true
                    else
                        return false
                }
                onClicked:
                {
                    timeDiagnosis.stop()

                    flag_Update_Otherside=false
                    flag_Update_side=false

                    updateText_otherside.text=get_name("Otherside")
                    updateText_side.text=get_name("Side")

                    updateText_otherside.color=textColor
                    updateText_side.color=textColor

                    progressbarupdate_otherside.value=0
                    progressbarupdate_side.value=0

                    updateText_side_info.visible=false
                    updateText_otherside_info.visible=false

                    buttonExport.visible=false

                    activateButton("Update Firmware")

                    updateDialog.open()
                }
            }
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"

            Button
            {
                id: buttonExport
                anchors.fill: parent
                visible: false
                text: "Diagnosis Export"
                background:
                Rectangle
                {
                    color: buttonExport.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }

                onClicked:
                {
                    saveFileDialog.open()
                }
            }
        }
        Rectangle
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1
            Layout.rowSpan:1
            Layout.preferredHeight: parent.height*0.05
            color: "transparent"

            Button
            {
                id: buttonExit
                anchors.fill: parent
                text: "Exit"
                background:
                Rectangle
                {
                    color: buttonExit.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }

                onClicked:
                {
                    Qt.quit()
                }
            }
        }
    }

    function stopAutoConnection()
    {
        timeDiagnosis.stop()
        comPortDialog.close()
        connectionIndicator.visible=false
    }
    Dialog
    {
        id: updateDialog
        width: mainWindow.width*0.6
        height: mainWindow.height*0.4
        modal: true//Qt.NonModal
        closePolicy: Popup.CloseOnEscape
        anchors.centerIn: parent

        contentItem:
        Rectangle
        {
            anchors.fill: parent
            color: backgroundColor
            radius: 5
            border.color: "black"

            Text
            {
                id: updateTextID
                text: "Update"
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.horizontalCenter: parent.horizontalCenter
                color: textColor
                font.bold: true

                MouseArea
                {
                    anchors.fill: parent
                    onClicked:
                    {
                        click_enable_recovery++

                        if(click_enable_recovery >= 3)
                        {
                            click_enable_recovery=0

                            if(updateTextID.text == "Update")
                            {
                                updateTextID.text = "Recovery"
                                flag_firmware_Recovery = true
                                button_both.visible=false
                            }
                            else
                            {
                                updateTextID.text = "Update"
                                flag_firmware_Recovery = false
                                button_both.visible=true
                            }
                        }
                    }
                }
            }

            GridLayout
            {
                anchors.centerIn: parent
                columns: 4
                rows: 7
                width: parent.width *0.9
                height: parent.height *0.6
                rowSpacing: 5

                Rectangle
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "transparent"
                    Layout.columnSpan: 4
                    Layout.rowSpan: 3

                    Text
                    {
                        id: updateText_otherside
                        text: "Remote:"
                        anchors.bottom: progressbarupdate_otherside.top
                        anchors.bottomMargin: 5
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: textColor
                    }
                    Text
                    {
                        id: updateText_otherside_info
                        text: ""
                        anchors.bottom: progressbarupdate_otherside.top
                        anchors.bottomMargin: 5
                        anchors.right: progressbarupdate_otherside.right
                        color: textColor
                        visible: false
                    }

                    ProgressBar
                    {
                        id: progressbarupdate_otherside
                        value: 0
                        width: parent.width
                        anchors.centerIn: parent

                        background: Rectangle
                        {
                                implicitWidth: width
                                implicitHeight: 6
                                color: "#e6e6e6"
                                radius: 3
                        }

                        contentItem: Item
                        {
                            implicitWidth: width
                            implicitHeight: 4

                            Rectangle {
                                width: progressbarupdate_otherside.visualPosition * parent.width
                                height: parent.height
                                radius: 2
                                color: spinnerColor
                            }
                        }
                    }
                }
                Rectangle
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "transparent"
                    Layout.columnSpan: 4
                    Layout.rowSpan: 3

                    Text
                    {
                        id: updateText_side
                        text: "Connected:"
                        anchors.bottom: progressbarupdate_side.top
                        anchors.bottomMargin: 5
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: "#f6f6f6"
                    }
                    Text
                    {
                        id: updateText_side_info
                        text: ""
                        anchors.bottom: progressbarupdate_side.top
                        anchors.bottomMargin: 5
                        anchors.right: progressbarupdate_side.right
                        color: textColor
                        visible: false
                    }
                    ProgressBar
                    {
                        id: progressbarupdate_side
                        value: 0
                        width: parent.width
                        anchors.centerIn: parent

                        background: Rectangle
                        {
                                implicitWidth: width
                                implicitHeight: 6
                                color: "#e6e6e6"
                                radius: 3
                            }

                        contentItem: Item {
                            implicitWidth: width
                            implicitHeight: 4

                            Rectangle {
                                width: progressbarupdate_side.visualPosition * parent.width
                                height: parent.height
                                radius: 2
                                color: spinnerColor
                            }
                        }
                    }
                }
                Rectangle
                {
                    Layout.fillWidth: true
                    //Layout.fillHeight: true
                    Layout.columnSpan: 4
                    Layout.rowSpan: 1
                    Layout.preferredHeight: parent.height*0.15
                    color: "transparent"

                    Rectangle
                    {
                        id: iconRect
                        height: parent.height
                        width: height
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        color: "transparent"
                        Image
                        {
                            id: imagePicture
                            source: "folder_icon.png"
                            width: parent.width
                            fillMode: Image.PreserveAspectFit
                        }
                    }
                    Rectangle
                    {
                        height: parent.height
                        width: parent.width * 0.7
                        anchors.left: iconRect.right
                        anchors.leftMargin: 5
                        anchors.verticalCenter: parent.verticalCenter
                        color: "transparent"
                        Text
                        {
                            id: filename
                            text: "No file selected"
                            color: textColor
                            anchors.fill: parent
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            elide: Text.ElideMiddle
                            maximumLineCount:1
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked:
                        {
                            fileDialog.open()
                        }
                    }
                }

                Rectangle
                {
                    Layout.fillWidth: true
                    //Layout.fillHeight: true
                    Layout.columnSpan: 1
                    Layout.rowSpan: 1
                    Layout.preferredHeight: parent.height*0.15
                    color: "transparent"
                    Button
                    {
                        id: button_both
                        //Layout.fillWidth: true
                        anchors.fill: parent
                        text: "Both"
                        background:
                        Rectangle
                        {
                            color: button_both.down ? "#808284" : "#f6f6f6"
                            border.color: "#26282a"
                            border.width: 1
                            radius: 4
                        }
                        onClicked:
                        {
                            updateText_otherside.text=get_name("Otherside")+" "+"Starting.."
                            updateText_otherside.color=textColor
                            updateText_side.text=get_name("Side")+" "+"Waiting.."
                            updateText_side.color=textColor
                            progressbarupdate_otherside.value=0
                            progressbarupdate_side.value=0
                            command_update="both"
                            updateText_side_info.visible=false
                            updateText_otherside_info.visible=false

                            if(urlpath === "")
                            {
                                command_button_start=true
                                fileDialog.open()
                            }
                            else
                                startUpdate()
                        }
                        visible:
                        {
                            if(flag_firmware_Recovery)
                                return false
                            else
                                return true
                        }
                    }
                }

                Rectangle
                {
                    Layout.fillWidth: true
                    //Layout.fillHeight: true
                    Layout.columnSpan: 1
                    Layout.rowSpan: 1
                    Layout.preferredHeight: parent.height*0.15
                    color: "transparent"
                    Button
                    {
                        id: button_side
                        anchors.fill: parent
                        text: get_name("Side")
                        background:
                        Rectangle
                        {
                            color: button_side.down ? "#808284" : "#f6f6f6"
                            border.color: "#26282a"
                            border.width: 1
                            radius: 4
                        }
                        onClicked:
                        {
                            updateText_side.text=get_name("Side")+" "+"Starting.."
                            updateText_side.color=textColor
                            progressbarupdate_side.value=0
                            updateText_side_info.visible=false

                            command_update="normal"

                            if(urlpath === "")
                            {
                                command_button_start=true
                                fileDialog.open()
                            }
                            else
                                startUpdate()
                        }
                    }
                }
                Rectangle
                {
                    Layout.fillWidth: true
                    //Layout.fillHeight: true
                    Layout.columnSpan: 1
                    Layout.rowSpan: 1
                    Layout.preferredHeight: parent.height*0.15
                    color: "transparent"
                    Button
                    {
                        id: button_otherside
                        anchors.fill: parent
                        text: get_name("Otherside")
                        background:
                        Rectangle
                        {
                            color: button_otherside.down ? "#808284" : "#f6f6f6"
                            border.color: "#26282a"
                            border.width: 1
                            radius: 4
                        }
                        onClicked:
                        {
                            updateText_otherside.text=get_name("Otherside")+" "+"Starting.."
                            updateText_otherside.color=textColor
                            progressbarupdate_otherside.value=0
                            updateText_otherside_info.visible=false

                            if(!flag_firmware_Recovery)
                                command_update="otherside"
                            else
                                command_update="bridge"

                            if(urlpath === "")
                            {
                                command_button_start=true
                                fileDialog.open()
                            }
                            else
                                startUpdate()
                        }
                    }
                }
                Rectangle
                {
                    Layout.fillWidth: true
                    //Layout.fillHeight: true
                    Layout.columnSpan: 1
                    Layout.rowSpan: 1
                    Layout.preferredHeight: parent.height*0.15
                    color: "transparent"
                    Button
                    {
                        id: buttonExitUpdate
                        anchors.fill: parent
                        text: "Exit"
                        background:
                        Rectangle
                        {
                            color: buttonExitUpdate.down ? "#808284" : "#f6f6f6"
                            border.color: "#26282a"
                            border.width: 1
                            radius: 4
                        }
                        onClicked:
                        {
                            if(flag_Update_Otherside || flag_Update_side || flag_firmware_Recovery)
                            {
                                stopAutoConnection()
                                Bridge.stopupdateMCU()
                            }

                            flag_Update_Otherside=false
                            flag_Update_side=false
                            updateDialog.close()
                            //activateButton("Update Firmware")
                        }
                    }
                }
            }
        }
    }
    Dialog
    {
        id: comPortDialog
        width: mainWindow.width*0.3
        height: mainWindow.height*0.3
        modal: Qt.NonModal
        anchors.centerIn: parent

        contentItem:
        Rectangle
        {
            anchors.fill: parent
            color: textColor
            radius: 5
            border.color: "black"

            Text
            {
                text: "Please select Port:"
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Rectangle
            {
                id: rectList
                width: parent.width*0.8
                height: parent.height*0.65
                anchors.centerIn: parent
                color: "white"

                ListView
                {
                    id: listviewPort
                    width: rectList.width
                    height: rectList.height
                    anchors.centerIn: parent
                    contentWidth: parent.width*0.8
                    flickableDirection: Flickable.AutoFlickDirection
                    highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
                    model: contactModel
                    focus: true


                    delegate: Rectangle
                    {
                        width: listviewPort.width
                        height: textdelegate.height+5
                        color: ListView.isCurrentItem ? "lightsteelblue" : "white"
                        Text
                        {
                            id: textdelegate
                            text: name
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        MouseArea
                        {
                            id: mouse_area1
                            z: 1
                            hoverEnabled: false
                            anchors.fill: parent
                            onClicked:
                            {
                                listviewPort.currentIndex = index
                                portName=textdelegate.text
                                buttonPort.visible=true
                            }
                        }
                    }
                }
            }
            Button
            {
                id: buttonPort
                visible: false
                text: "Connect"
                height: (parent.height-rectList.bottom) *0.5
                anchors.top: rectList.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 2
                background:
                Rectangle
                {
                    color: buttonPort.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                    height: (parent.height-rectList.bottom) *0.5
                }

                onClicked:
                {
                    comPortDialog.close()
                    Bridge.connectToPort(portName)
                }
            }

        }
    }
    Dialog
    {
        id: pdfDialog
        width: mainWindow.width*0.25
        height: mainWindow.height*0.25
        modal: Qt.NonModal
        anchors.centerIn: parent

        contentItem:
        Rectangle
        {
            anchors.fill: parent
            color: textColor
            radius: 5
            border.color: "black"


            Text
            {
                text: "PDF not found"
                anchors.centerIn: parent
            }
            Button
            {
                id: buttonOK
                text: "Ok"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: 5
                background:
                Rectangle
                {
                    color: buttonOK.down ? "#808284" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 1
                    radius: 4
                }

                onClicked:
                {
                    pdfDialog.close()
                }
            }
        }
    }
    FileDialog
    {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        nameFilters: ["Hex Files (*.hex)"]

        onAccepted:
        {
            urlpath = fileDialog.fileUrls.toString()

            if(Qt.platform.os === "windows")
                urlpath = urlpath.replace(/^(file:\/{3})/,"");
            else
                urlpath = urlpath.replace(/^(file:\/{2})/,"");

            filename.text = urlpath

            fileDialog.close()

            if(command_button_start)
            {
                command_button_start=false
                startUpdate()
            }
        }
        onRejected:
        {
            fileDialog.close()
        }
    }
    FileDialog
    {
        id: saveFileDialog
        selectExisting: false
        nameFilters: ["Text files (*.txt)", "All files (*)"]
        onAccepted: saveFile(saveFileDialog.fileUrl, textDiagnosis)
    }
    function saveFile(fileUrl, text)
    {
        var request = new XMLHttpRequest();
        request.open("PUT", fileUrl, false);
        request.send(text);
        return request.status;
    }
    function startUpdate()
    {
        stopAutoConnection()
        fileDialog.close()
        var recovery="false"

        deactivateButton()

        if(flag_firmware_Recovery)
            recovery="true"

        if(command_update=="both" || command_update=="otherside")
        {
            flag_Update_Otherside=true
            flag_Update_side=false
            Bridge.updateMCU("otherside", urlpath,recovery)
        }
        else if(command_update=="normal")
        {
            flag_Update_Otherside=false
            flag_Update_side=true
            Bridge.updateMCU("normal", urlpath,recovery)
        }
        else if(command_update=="bridge")
        {
            flag_Update_Otherside=true
            flag_Update_side=false
            Bridge.updateMCU("bridge", urlpath,recovery)
        }
    }

    function deactivateButton()
    {
        button_both.visible=false
        button_side.visible=false
        button_otherside.visible=false
        buttonExitUpdate.visible = false
    }
    function activateButton(value)
    {
        if(value === "Update Success" )
        {
            button_both.visible=false
            button_side.visible=false
            button_otherside.visible=false
            buttonUpdate.visible=false
            buttonExitUpdate.visible = true
        }
        else if(value === "Update Failed")
        {
            updateTextID.text = "Recovery"
            flag_firmware_Recovery = true

            if(updateText_side_info.text==="Failed")
            {
                button_otherside.visible=false//Is not possible to update otherside wenn side is broken
            }
            button_both.visible=false
            button_side.visible=true

            if(!sideInBootloader)
                button_otherside.visible=true

            buttonExitUpdate.visible = true
        }
        else if(value === "IRT not in Bootloader")
        {
            flag_firmware_Recovery = false
            updateTextID.text = "Update"

            button_side.visible=true
            if(!sideInBootloader)
            {
                button_otherside.visible=true
                button_both.visible=true
            }
            buttonExitUpdate.visible = true
        }
        else if(value === "Update Firmware")
        {
            button_side.visible=true
            if(!sideInBootloader)
            {
                button_both.visible=true
                button_otherside.visible=true
            }
        }
        else if(value === "Starting Update")
        {
            buttonExitUpdate.visible = false
        }


        if(flag_firmware_Recovery)
            button_both.visible=false
        //buttonExitUpdate.visible = true
    }
    ListModel
    {
        id: contactModel
        ListElement
        {
            name: ""
        }
    }
    Timer
    {
        id: timeDiagnosis
        interval: 4000
        repeat: true
        onTriggered:
        {
            Bridge.get_configuration()
        }
    }
}
