
import sys
import os

__author__ = 'Michael Liesenberg'


from PyQt5 import QtCore, QtSerialPort
from PyQt5.QtCore import QThread, QObject
import serial, serial.tools.list_ports
import PyQt5.QtQuick  #Do not erase this include
import qml_rc #Do not erase this include, use it for .exe

import time
from ctypes import *
import binascii

import IRT_unit
from IRT_unit import HEADER, IRT_BODY, PC_BODY
from PySide2.QtCore import *
from PySide2 import QtGui, QtQml
from PySide2.QtCore import SIGNAL, QObject

import serial.tools.list_ports
import serial
import collections
from BootLoader import BootLoader
import platform

if platform.system() == 'Windows':
    serial_use_QSerialPort = True
else:
    serial_use_QSerialPort = False
dev = None
isready = False
stop_threads = True
go_to_bootloader = False
go_to_bootloader_recovery = False
widget = None
loader = None
IRT_HEADER = HEADER(bytearray(100))
PC_HEADER = HEADER(bytearray(100))
IRT_body = IRT_BODY(bytearray(100))
PC_body = PC_BODY(bytearray(100))
stop_bootmodus = "STOP_BOOTLOADER_GO_NORMAL"
firmware_file = ""
BytesToWrite = None
BytesToWrite_FLAG = False
class MySignal(QObject):
        sig = Signal(str)

class MySignalList(QObject):
    sig = Signal(list)

def getENUM( classArg, character):

    try:
        ret = classArg(character)
    except Exception as e:
        ret = "UNDEFINED"

    return str(ret)

def setUTF8( bytes):

    try:
        ret = str(bytes.decode("utf-8"))
    except Exception as e:
        ret = "UNDEFINED"
    return str(ret)

class ExecuteThread(QThread):
    global dev
    global IRT_body
    global IRT_HEADER

    def __init__(self, parent = None):
        QThread.__init__(self, parent)
        self.my_signal = MySignal()
        self.my_signalIRT = MySignalList()
        self.stop_exception_signal = MySignal()
        self.my_signal_Update = MySignal()
        self.is_Updatable_signal = MySignal()
        self.IRT_STRUCT = [""]

    def run(self):
        global go_to_bootloader
        global go_to_bootloader_recovery
        global loader
        global BytesToWrite
        global BytesToWrite_FLAG
        global stop_threads

        try:

            while True:
                #if serial_use_QSerialPort:
                if serial_use_QSerialPort:
                    dev.waitForReadyRead(10)

                    if dev.bytesAvailable() >=sizeof(HEADER):
                        byteData = dev.read(sizeof(HEADER))
                    else:
                        byteData = bytearray()
                else:
                    byteData = dev.read(sizeof(HEADER))

                if byteData == None:
                    sizeofdata = 0
                else:
                    sizeofdata = len(byteData)

                if sizeofdata >= sizeof(HEADER):
                    print(bytearray(byteData).hex())
                    IRT_HEADER = IRT_unit.HEADER(bytearray(byteData))
                    data = bytearray(IRT_HEADER)
                    datawithoutcrc = data[: len(data) - 2]
                    IRT_HEADER.VERSION=byteData[2] << 8 | byteData[3]
                    IRT_HEADER.CRC_Checksum_INIT = byteData[6] << 8 | byteData[7]
                    crcCalc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)

                    if IRT_HEADER.CRC_Checksum_INIT == crcCalc:

                        if str(getENUM(IRT_unit.RESPONSE, IRT_HEADER.RESPONSE)) == "UART_BAD":
                            self.my_signal.sig.emit("")
                            self.my_signal.sig.emit("Values transmitted are invalid, please try again")
                            if go_to_bootloader:
                                go_to_bootloader = False
                        if str(getENUM(IRT_unit.COMMAND, IRT_HEADER.COMMAND)) == "IN_BOOTLOADER":
                            self.my_signal.sig.emit("")
                            self.my_signal.sig.emit("IRT is in Bootloader")
                        else:
                            sendToMain = "\nCOMMAND: " + str(getENUM(IRT_unit.COMMAND, IRT_HEADER.COMMAND)) + \
                                         "\nRESPONSE: " + str(getENUM(IRT_unit.RESPONSE,IRT_HEADER.RESPONSE)) + \
                                         "\nVERSION: " + str("%.4X" % (IRT_HEADER.VERSION)) + \
                                         "\nCRC_Checksum_INIT: " + str(hex(IRT_HEADER.CRC_Checksum_INIT))
                            self.my_signal.sig.emit(sendToMain)

                            if IRT_HEADER.VERSION > 1:
                                self.is_Updatable_signal.sig.emit("true")
                            else:
                                self.is_Updatable_signal.sig.emit("false")

                            if IRT_HEADER.COMMAND == b'B':
                                global IRT_body

                                if serial_use_QSerialPort:
                                    countBreak=0
                                    while dev.bytesAvailable() < sizeof(IRT_BODY):
                                        dev.waitForReadyRead(100)
                                        countBreak+=1
                                        if countBreak >= 80:
                                            break
                                buf = bytes(dev.read(sizeof(IRT_BODY)))

                                if buf == None:
                                    sizeofdata = 0
                                else:
                                    sizeofdata = len(buf)

                                if sizeofdata == sizeof(IRT_BODY):
                                    IRT_body = IRT_BODY(buf)
                                    data = bytearray(IRT_body)
                                    datawithoutcrc = buf[:  len(data) - 2]
                                    IRT_body.CRC_Checksum = buf[len(data) - 2] << 8 | buf[len(data) - 1]

                                    if IRT_body.CRC_Checksum == binascii.crc_hqx(datawithoutcrc, 0xFFFF):

                                        type_board_hollow = ""
                                        type_board_body = ""

                                        if str(IRT_unit.IRT_TYPE(IRT_body.connected_to)) == "HOLLOW":
                                            type_board_hollow = "X"
                                        else:
                                            type_board_body = "X"

                                        if str(IRT_unit.BOARD_TYPE(IRT_body.board_type)) == "FPGA":
                                            type_board_voltage = "1V1"
                                        else:
                                            type_board_voltage = "1V8"

                                        sw_version_body = IRT_body.BODY.SW_VERSION_1+IRT_body.BODY.SW_VERSION_2+IRT_body.BODY.SW_VERSION_3+IRT_body.BODY.SW_VERSION_4 + IRT_body.BODY.SW_VERSION_5+IRT_body.BODY.SW_VERSION_6+IRT_body.BODY.SW_VERSION_7+IRT_body.BODY.SW_VERSION_8
                                        sw_version_hollow = IRT_body.HOLLOW.SW_VERSION_1+IRT_body.HOLLOW.SW_VERSION_2+IRT_body.HOLLOW.SW_VERSION_3+IRT_body.HOLLOW.SW_VERSION_4 + IRT_body.HOLLOW.SW_VERSION_5+IRT_body.HOLLOW.SW_VERSION_6+IRT_body.HOLLOW.SW_VERSION_7+IRT_body.HOLLOW.SW_VERSION_8

                                        if sw_version_body == b'\x00\x00\x00\x00\x00\x00\x00\x00':
                                            sw_version_body = "UNDEFINED"
                                        else:
                                            sw_version_body = setUTF8(sw_version_body)

                                        if sw_version_hollow == b'\x00\x00\x00\x00\x00\x00\x00\x00':
                                            sw_version_hollow = "UNDEFINED"
                                        else:
                                            sw_version_hollow = setUTF8(sw_version_hollow)

                                        if str(getENUM(IRT_unit.RESPONSE,IRT_HEADER.RESPONSE))  == str(getENUM(IRT_unit.RESPONSE,b'L')):
                                            if str(IRT_unit.IRT_TYPE(IRT_body.connected_to)) == "HOLLOW":
                                                sw_version_body = "In Bootloader"
                                            else:
                                                sw_version_hollow = "In Bootloader"

                                        stringArray=["", "BODY", "HOLLOW",
                                                    "Board Type", getENUM(IRT_unit.BOARD_TYPE,IRT_body.board_type), getENUM(IRT_unit.BOARD_TYPE,IRT_body.board_type),
                                                    "Connected to", type_board_body, type_board_hollow,
                                                    "SW Version", sw_version_body,sw_version_hollow,
                                                    "Duplex ETH1", getENUM(IRT_unit.DUPLEX,IRT_body.BODY.PHY0_DUPPLEX),getENUM(IRT_unit.DUPLEX,IRT_body.HOLLOW.PHY0_DUPPLEX),
                                                    "Duplex ETH2", getENUM(IRT_unit.DUPLEX,IRT_body.BODY.PHY1_DUPPLEX),getENUM(IRT_unit.DUPLEX,IRT_body.HOLLOW.PHY1_DUPPLEX),
                                                    "LLF ETH1", getENUM(IRT_unit.ENABLE,IRT_body.BODY.LLF_PHY0),getENUM(IRT_unit.ENABLE,IRT_body.HOLLOW.LLF_PHY0),
                                                    "LLF ETH2", getENUM(IRT_unit.ENABLE,IRT_body.BODY.LLF_PHY1),getENUM(IRT_unit.ENABLE,IRT_body.HOLLOW.LLF_PHY1),
                                                    type_board_voltage, str("%.2f" % (float(IRT_body.BODY.VALUE_1V1_1V8)/1000)), str("%.2f" % (float(IRT_body.HOLLOW.VALUE_1V1_1V8)/1000)),
                                                    "3V3", str("%.2f" % (float(IRT_body.BODY.VALUE_3V3)/100)), str("%.2f" % (float(IRT_body.HOLLOW.VALUE_3V3)/100)),
                                                    "5V", str("%.2f" % (float(IRT_body.BODY.VALUE_5V)/100)),str("%.2f" % (float(IRT_body.HOLLOW.VALUE_5V)/100)),
                                                    "-5V", str("%.2f" % (float(IRT_body.BODY.VALUE_n5V)/100)),str("%.2f" % (float(IRT_body.HOLLOW.VALUE_n5V)/100)),
                                                    "2V5", str("%.2f" % (float(IRT_body.BODY.VALUE_2V5)/1000)),str("%.2f" % (float(IRT_body.HOLLOW.VALUE_2V5)/1000)),
                                                    "3V3 mA", str("%i" % (IRT_body.BODY.VALUE_I3V3)),str("%i" % (IRT_body.HOLLOW.VALUE_I3V3)),
                                                    "5V mA", str("%i" % (IRT_body.BODY.VALUE_I5V)),str("%i" % (IRT_body.HOLLOW.VALUE_I5V)),
                                                    "Gain mV", str("%i" % (IRT_body.BODY.VALUE_GAIN)),str("%i" % (IRT_body.HOLLOW.VALUE_GAIN)),
                                                    "Gain Min mV", str("%i" % (IRT_body.BODY.VALUE_GAIN_MIN)),str("%i" % (IRT_body.HOLLOW.VALUE_GAIN_MIN)),
                                                    "Gain Max mV", str("%i" % (IRT_body.BODY.VALUE_GAIN_MAX)),str("%i" % (IRT_body.HOLLOW.VALUE_GAIN_MAX)),
                                                    "Temperature °C", str("%i" % (IRT_body.BODY.VALUE_TEMPERATURE)),str("%i" % (IRT_body.HOLLOW.VALUE_TEMPERATURE))]

                                        self.my_signalIRT.sig.emit(stringArray)
                                else:
                                    self.my_signal.sig.emit("")
                                    if not go_to_bootloader and not go_to_bootloader_recovery:
                                        self.my_signal.sig.emit("Values received are invalid, please try again")

                            elif (IRT_HEADER.COMMAND == b'U' or IRT_HEADER.COMMAND == b'O') and (go_to_bootloader or go_to_bootloader_recovery):
                                self.update_MCU()
                    else:
                        self.my_signal.sig.emit("")
                        self.is_Updatable_signal.sig.emit("false")

                        if not go_to_bootloader and not go_to_bootloader_recovery:
                            self.my_signal.sig.emit("Values received are invalid, please try again")

                            if serial_use_QSerialPort:
                                dev.readAll()
                            else:
                                dev.read()

                elif sizeofdata >= 1:
                    print(byteData)

                if go_to_bootloader or go_to_bootloader_recovery:
                    self.update_MCU()

                time.sleep(0.01)

                if BytesToWrite_FLAG:
                    BytesToWrite_FLAG = False

                    if serial_use_QSerialPort:
                        dev.write(BytesToWrite)
                        dev.waitForBytesWritten(100)
                        dev.flush()
                    else:
                        dev.write(BytesToWrite)

                if stop_threads:
                    if dev != None:
                        dev.close()
                    break

        except Exception as e:
            print("No Hardware connected to PC!")
            stop_threads = True
            self.stop_exception_signal.sig.emit("")


    def update_MCU(self):
        global go_to_bootloader
        global go_to_bootloader_recovery
        global loader
        global firmware_file

        try:
            self.my_signal_Update.sig.emit("")
            self.my_signal_Update.sig.emit("Starting Update")

            if firmware_file == "":
                raise ValueError("No File selected!")
            #time.sleep(1)

            if serial_use_QSerialPort:
                dev.readAll()

            counter = 0
            counter_success=0
            while counter < 500:
                dev.write(b'\x1b')
                dev.flush()
                if serial_use_QSerialPort:
                    dev.waitForBytesWritten(500)
                    time.sleep(0.02)
                else:
                    time.sleep(0.1)
                counter += 1
                if serial_use_QSerialPort:
                    value = dev.read(1)
                    print(value)
                    if value == b'\r': #Check if it is inside bootloader
                        counter_success += 1
                        if counter_success > 5:
                            inBootloader=True
                            break
                else:
                    if dev.in_waiting:
                        value = dev.read(1)
                        print(value)
                        if value == b'\r': #Check if it is inside bootloader
                            counter_success += 1
                            if counter_success > 5:
                                inBootloader = True
                                break
                if counter == 500:
                    #raise ValueError("Device not in Bootloader!")
                    inBootloader = False
                    self.my_signal_Update.sig.emit("IRT not in Bootloader")

            time.sleep(0.2)

            if inBootloader:
                if serial_use_QSerialPort:
                    dev.readAll()
                print("init Bootloader")
                loader = BootLoader(dev, dev, firmware_file, serial_use_QSerialPort)

                ret = False
                count = 0
                while not ret:
                    try:
                        print("chip_erase()")
                        loader.chip_erase()
                        ret = True
                    except Exception as e:
                        count += 1
                        if count > 5:
                            raise ValueError("Flashing failed!")

                ret = False
                count = 0
                while not ret:
                    print("loader.load(self.my_signal_Update)")
                    ret = loader.load(self.my_signal_Update)
                    if not ret:
                        count += 1
                    if count >= 5:
                        raise ValueError("Flashing failed!")
                print("loader.verify")
                verified = loader.verify(self.my_signal_Update)

                if not verified:
                    loader.chip_erase()
                    go_to_bootloader = False
                    go_to_bootloader_recovery = False
                    loader = None
                    raise ValueError("Verification failed!")
                else:
                    print("loader.exit()")
                    time.sleep(0.2)
                    loader.exit()
                    go_to_bootloader = False
                    go_to_bootloader_recovery = False
                    loader = None
                    self.leave_Update_Modus()
                    self.my_signal_Update.sig.emit("")
                    self.my_signal_Update.sig.emit("Update Success")
            else:
                go_to_bootloader = False
                go_to_bootloader_recovery = False
                loader = None

        except Exception as e:
            loader = None
            go_to_bootloader = False
            go_to_bootloader_recovery = False
            self.my_signal_Update.sig.emit("")
            self.my_signal_Update.sig.emit("Update Failed")
            self.leave_Update_Modus()

    def leave_Update_Modus(self):
        time.sleep(0.01)
        dev.write(str(stop_bootmodus).encode())
        time.sleep(0.01)
        dev.write(str(stop_bootmodus).encode())
        time.sleep(1)

class Bridge(QObject):

    timer = QTimer()
    textChanged_connection = Signal(str)
    textChanged_uart = Signal(str)
    valuesChanged = Signal()
    comportChanged = Signal()
    valuesChangedUpdate = Signal()
    valuesChangedisUpdatable = Signal()
    startupdate = Signal()

    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        self.myThread = ExecuteThread()
        self.myThread.my_signal.sig.connect(self.onTextReady)
        self.myThread.my_signalIRT.sig.connect(self.onTextIRTReady)
        self.myThread.my_signal_Update.sig.connect(self.onTextUpdate)
        self.myThread.is_Updatable_signal.sig.connect(self.onisUpdatableChanged)
        self.myThread.stop_exception_signal.sig.connect(self.onThreadExceptionFinished)
        self.text_connection = ""
        self.update_text = ""
        self.is_updatable = False
        self.text_UART_message = ""
        self.start_FTDI()
        self.listString = [""]
        self.listComPort = []
        self.IRT_Configuration = [""]
        self.comPortConnectTo = ""
        self.timer.timeout.connect(self.start_FTDI)


    def start_FTDI(self):
        global dev
        global stop_threads
        global serial_use_QSerialPort

        if stop_threads == True:
            try:
                if dev == None:
                    if serial_use_QSerialPort:
                        dev = QtSerialPort.QSerialPort()
                    else:
                        dev = serial.Serial()

                ports = serial.tools.list_ports.comports()
                counter = 0
                list = []

                for port, desc, hwid in sorted(ports):
                    if "0403" in hwid and "6001" in hwid:
                        counter = counter+1
                        list.append(port)

                if self.comPortConnectTo != "":
                    print("Device selected from list " + self.comPortConnectTo)

                    dev.close()
                    if serial_use_QSerialPort:
                        dev.setPortName(str(self.comPortConnectTo))
                        dev.setBaudRate(QtSerialPort.QSerialPort.Baud9600)
                        dev.setDataBits(QtSerialPort.QSerialPort.Data8)
                        dev.setStopBits(QtSerialPort.QSerialPort.OneStop)
                        dev.setParity(QtSerialPort.QSerialPort.EvenParity)  # serial.PARITY_NONE
                        dev.open(QtCore.QIODevice.ReadWrite)
                        #QObject.connect(dev, SIGNAL('errorOccurred()'), self.serialQtError)
                        dev.errorOccurred.connect(self.serialQtError)

                        #dev.error()

                    else:
                        dev.port = str(self.comPortConnectTo)
                        dev.baudrate = 9600
                        dev.bytesize = 8
                        dev.stopbits = serial.STOPBITS_ONE
                        dev.parity = serial.PARITY_EVEN  # serial.PARITY_NONE
                        dev.timeout = 2
                        dev.open()
                    print(dev)
                    stop_threads = False
                    self.myThread.start()
                    self.text_connection = "Connected"
                    self.textChanged_connection.emit(self.text_connection)
                else:
                    # More than 2 FTDI ICs are connected
                    if collections.Counter(self.listComPort) != collections.Counter(list):
                        self.listComPort = list
                    self.comportChanged.emit()

                    self.text_connection = "Not connected"
                    self.textChanged_connection.emit(self.text_connection)
                    self.timer.stop()
                    self.timer.start(2000)


            except Exception as e:
                dev = None
                self.text_connection = "Not connected"
                self.textChanged_connection.emit(self.text_connection)
                self.timer.stop()
                self.timer.start(2000)

    def serialQtError(self):
        global dev
        dev = None
        self.text_connection = "Not connected"
        self.textChanged_connection.emit(self.text_connection)
        self.timer.stop()
        self.timer.start(2000)

    def makestringWithCrc(self, string):
        byteString = bytes(string, 'ASCII')
        crc = binascii.crc_hqx(byteString, 0xFFFF)
        returnstring = "%s %.4X" % (string, crc)
        return (returnstring)

    def onThreadExceptionFinished(self):
        global dev
        self.text_connection = "Not connected"
        self.comPortConnectTo = ""
        self.textChanged_connection.emit(self.text_connection)
        dev = None
        self.start_FTDI()

    def onTextReady(self,i):
        self.text_UART_message=i
        self.textChanged_uart.emit(self.text_UART_message)
        print(i)

    def onTextIRTReady(self,i):
        global IRT_body
        self.listString = []
        self.listString = i
        self.IRT_Configuration = []
        self.IRT_Configuration = [getENUM(IRT_unit.DUPLEX,IRT_body.HOLLOW.PHY0_DUPPLEX), getENUM(IRT_unit.DUPLEX,IRT_body.HOLLOW.PHY1_DUPPLEX),
                                  getENUM(IRT_unit.ENABLE,IRT_body.HOLLOW.LLF_PHY0), getENUM(IRT_unit.ENABLE,IRT_body.HOLLOW.LLF_PHY1),
                                  getENUM(IRT_unit.DUPLEX,IRT_body.BODY.PHY0_DUPPLEX), getENUM(IRT_unit.DUPLEX,IRT_body.BODY.PHY1_DUPPLEX),
                                  getENUM(IRT_unit.ENABLE,IRT_body.BODY.LLF_PHY0), getENUM(IRT_unit.ENABLE,IRT_body.BODY.LLF_PHY1)]

        print(self.listString)

        self.valuesChanged.emit()

    def onTextUpdate(self, i):
        self.update_text = i
        self.valuesChangedUpdate.emit()
    def onisUpdatableChanged(self, i):
        if i == "true":
            self.is_updatable = True
            self.valuesChangedisUpdatable.emit()
        else:
            self.is_updatable = False
            self.valuesChangedisUpdatable.emit()


    @Property("QVariantList", notify=valuesChanged)
    def listStringValues(self):
        return self.listString

    @Property("QVariantList", notify=comportChanged)
    def listComPortValues(self):
        return self.listComPort
    @Property("QString", notify=valuesChangedUpdate)
    def stringUpdate(self):
        return self.update_text

    @Property(bool, notify=valuesChangedisUpdatable)
    def isUpdatable(self):
        return self.is_updatable

    @Property("QVariantList", notify=valuesChanged)
    def irtCongfiguration(self):
        return self.IRT_Configuration

    @Property(str, notify=textChanged_connection)
    def connection(self):
        return self.text_connection
    @Property(str, notify=textChanged_uart)
    def uart_message(self):
        return self.text_UART_message

    @Slot()
    def get_configuration(self):
        global PC_HEADER
        global PC_body
        PC_HEADER.COMMAND = IRT_unit.COMMAND.GET_BODY.value
        PC_HEADER.RESPONSE = IRT_unit.RESPONSE.UART_OK.value
        PC_HEADER.VERSION = 0
        PC_HEADER.SIZEBYTES = 8
        data = bytearray(PC_HEADER)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)
        PC_HEADER.CRC_Checksum_INIT = crc
        print(bytearray(PC_HEADER).hex())
        self.write_to_IRT(PC_HEADER)

    @Slot("QString")
    def connectToPort(self, value):
        self.comPortConnectTo = value

    @Slot("QVariantList")
    def send_configuration(self, values):
        global PC_HEADER
        global PC_body

        PC_HEADER.COMMAND = IRT_unit.COMMAND.SET_CONFIG.value
        PC_HEADER.RESPONSE = IRT_unit.RESPONSE.UART_OK.value
        PC_HEADER.VERSION = 0
        PC_HEADER.SIZEBYTES = len(bytearray(PC_body))

        data = bytearray(PC_HEADER)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)

        PC_HEADER.CRC_Checksum_INIT = crc

        self.write_to_IRT(PC_HEADER)

        time.sleep(1)

        PC_body.HOLLOW.PHY0_DUPPLEX = ord(values[0])
        PC_body.HOLLOW.PHY1_DUPPLEX = ord(values[1])
        PC_body.HOLLOW.LLF_PHY0 = ord(values[2])
        PC_body.HOLLOW.LLF_PHY1 = ord(values[3])

        PC_body.BODY.PHY0_DUPPLEX = ord(values[4])
        PC_body.BODY.PHY1_DUPPLEX = ord(values[5])
        PC_body.BODY.LLF_PHY0 = ord(values[6])
        PC_body.BODY.LLF_PHY1 = ord(values[7])

        data = bytearray(PC_body)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)
        PC_body.CRC_Checksum = crc

        self.write_to_IRT(PC_body)
        time.sleep(1)
        self.onTextReady("Configuration has been send,\n please click on get configuration")

    @Slot()
    def reset_IRT(self):
        global PC_HEADER

        PC_HEADER.COMMAND = IRT_unit.COMMAND.RESET_IRT.value
        PC_HEADER.RESPONSE = IRT_unit.RESPONSE.UART_OK.value
        PC_HEADER.VERSION = 0
        PC_HEADER.SIZEBYTES = len(bytearray(PC_HEADER))

        data = bytearray(PC_HEADER)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)

        PC_HEADER.CRC_Checksum_INIT = crc

        self.write_to_IRT(PC_HEADER)
        time.sleep(1)
        self.onTextReady("Reset has been send,\n wait for boot...")

    def write_to_IRT(self, struct_to_send):
        global dev
        global serial_use_QSerialPort
        global BytesToWrite
        global BytesToWrite_FLAG

        buf = (c_char * sizeof(struct_to_send))()
        memmove(buf, byref(struct_to_send), sizeof(struct_to_send))
        print(bytearray(buf))

        BytesToWrite = bytearray(buf)
        BytesToWrite_FLAG = True


    @Slot("QString", "QString", "QString")
    def updateMCU(self, value, url, recovery):
        global dev
        global go_to_bootloader
        global go_to_bootloader_recovery
        global PC_HEADER
        global firmware_file

        if value == "normal":
            PC_HEADER.COMMAND = IRT_unit.COMMAND.START_UPDATE.value
        elif value == "otherside":
            PC_HEADER.COMMAND = IRT_unit.COMMAND.START_UPDATE_OTHERSIDE.value
        elif value == "bridge": #Force Make bridge
            PC_HEADER.COMMAND = IRT_unit.COMMAND.START_UPDATE_MAKE_BRIDGE.value
        else:
            PC_HEADER.COMMAND = IRT_unit.COMMAND.START_UPDATE.value

        PC_HEADER.RESPONSE = IRT_unit.RESPONSE.UART_OK.value
        PC_HEADER.VERSION = 0
        PC_HEADER.SIZEBYTES = len(bytearray(PC_HEADER))

        data = bytearray(PC_HEADER)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)

        PC_HEADER.CRC_Checksum_INIT = crc

        firmware_file = url

        try:
            file = open(firmware_file)
            file.close()
            time.sleep(1)

            if recovery == "true":
                if value == "bridge":
                    self.write_to_IRT(PC_HEADER)
                    time.sleep(2)
                go_to_bootloader = True
                go_to_bootloader_recovery = True
            else:
                go_to_bootloader_recovery = False
                self.write_to_IRT(PC_HEADER)
                time.sleep(3)

                #if value != "otherside":
                go_to_bootloader = True


        except FileNotFoundError:
            print("File not found. Check the path variable and filename")


    @Slot()
    def stopupdateMCU(self):
        global dev
        global go_to_bootloader

        go_to_bootloader = False
        dev.write(str(stop_bootmodus).encode())
        dev.write(str(stop_bootmodus).encode())


    def closeEvent(self, event):
        global stop_threads
        self.timer.stop()
        stop_threads=False
        time.sleep(1)

if __name__ == "__main__":


    print(os.path.abspath("What_means_LLF_01.pdf"))

    app = QtGui.QGuiApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(":/spinner_logo.png"))
    engine = QtQml.QQmlApplicationEngine()
    context = engine.rootContext()
    bridge = Bridge()
    context.setContextProperty("Bridge", bridge)
    context.setContextProperty("ApplicationName", "IRT Tool - D20279D")
    context.setContextProperty("pathToPDF", os.path.abspath("What_means_LLF_01.pdf"))
    app.setOrganizationName("Spinner GmbH")
    app.setOrganizationDomain("www.spinner-group.com")
    context.setBaseUrl(QUrl.fromLocalFile(""))
    engine.load("qrc:/main.qml")#For release to .exe-> must do "pyside2-rcc qml_rc.qrc -o qml_rc.py "
    #engine.load("ain.qml")#For Debug

    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec_())
