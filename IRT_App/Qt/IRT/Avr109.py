import time
from struct import pack
from builtins import bytes

class Avr109(object):
    """AVR109 firmware upload protocol.  This currently just implements the
    limited subset of AVR109 (butterfly) protocol necessary to load the
    Atmega 328P used on the Mobilinkd TNC.
    ""#""#!/usr/bin/env python2.7
    """
    
    def __init__(self, reader, writer, serial_use_QSerialPort):
        self.sio_reader = reader
        self.sio_writer = writer
        self.USE_QSerialPort = serial_use_QSerialPort
        
    def start(self):
        self.sio_writer.write(b'\033')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(100)
        time.sleep(0.1)
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(100)
        buf = self.sio_reader.read(10)
    
    def send_address(self, address):
        address //= 2 # convert from byte to word address
        ah = (address & 0xFF00) >> 8
        al = address & 0xFF
        time.sleep(1)
        self.sio_writer.write(bytes(pack('cBB', b'A', ah, al)))
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(100)
        time.sleep(1)
        self.verify_command_sent("Set address to: %04x" % address)
    
    def send_block(self, memtype, data):
        """Send a block of memory to the bootloader.  This command should
        be preceeded by a call to send_address() to set the address that
        the block will be written to.
        
        @note The block must be in multiple of 2 bytes (word size).
        
        @param memtype is the type of memory to write. 'E' is for EEPROM,
            'F' is for flash.
        @data is a block of data to be written."""
        
        assert(len(data) % 2 == 0)
        
        ah = 0
        al = len(data)

        #time.sleep(0.4)
        self.sio_writer.write(bytes(pack('cBBc', b'G', ah, al, memtype)))
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(100)
        time.sleep(0.1)
        if self.USE_QSerialPort:
            self.sio_writer.write(bytearray(data))
        else:
            self.sio_writer.write(data)
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(100)
        time.sleep(0.2)
        self.verify_command_sent("Block load: %d" % len(data))
    
    def read_block(self, memtype, size):
        """Read a block of memory from the bootloader.  This command should
        be preceeded by a call to send_address() to set the address that
        the block will be written to.
        
        @note The block must be in multiple of 2 bytes (word size).
        
        @param memtype is the type of memory to write. 'E' is for EEPROM,
            'F' is for flash.
        @param size is the size of the block to read."""

        """
        2020/09/29 Michael Liesenberg: 4x Dummy bytes are used here to fix a problem in pyserial/COMPort
        The first sended bytes are broken somehow if 128 bytes are receives before
        """

        if self.USE_QSerialPort:
            self.sio_writer.write(bytes(pack('cBBc', b'g', 0, size, memtype)))
        else:
            self.sio_writer.write(bytes(pack('BBBBcBBc', 0, 0, 0, 0, b'g', 0, size, memtype)))

        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(100)
        self.sio_reader.timeout = 2
        time.sleep(0.2)
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(100)
        result = self.sio_reader.read(size)

        #time.sleep(0.1)

        return result
        
    def write_block(self, address, memtype, data):
        pass
    
    def do_upload(self):
        pass
    
    def verify_command_sent(self, cmd):
        
        timeout = self.sio_reader.timeout
        self.sio_reader.timeout = 5#2#1
        #time.sleep(1)
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(5000)

        c = self.sio_reader.read(1)
        self.sio_reader.timeout = timeout
        if c != b'\r':
            # Do not report c because it could be None
            print(str("REPLY failed Value returned "))# + c.decode("utf-8")))
            raise IOError("programmer did not respond to command: %s" % cmd)
        else:

            pass
            # print "programmer success: %s" % cmd

    
    def chip_erase(self):
        time.sleep(0.2)
        self.sio_writer.write(b'e')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.2)
        self.verify_command_sent("chip erase")
    
    def enter_program_mode(self):
        time.sleep(0.2)
        self.sio_writer.write(b'P')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.2)
        self.verify_command_sent("enter program mode")
        
    def leave_program_mode(self):
        time.sleep(.2)
        self.sio_writer.write(b'L')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.2)
        self.verify_command_sent("leave program mode")
   
    def exit_bootloader(self):
        time.sleep(.2)
        self.sio_writer.write(b'E')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.2)
        #self.verify_command_sent("exit bootloader")
        
    def supports_auto_increment(self):
        # Auto-increment support
        counterBreak = 0
        while self.sio_reader.read(1) != b'Y':
            counterBreak += 1
            time.sleep(.2)
            self.sio_writer.write(b'a')
            if self.USE_QSerialPort:
                self.sio_writer.waitForBytesWritten(500)
            time.sleep(0.2)
            if self.USE_QSerialPort:
                self.sio_writer.waitForReadyRead(500)
            if counterBreak > 5:
                return False

        return True
    
    def get_block_size(self):
        for i in range(20):
            time.sleep(.2)
            self.sio_writer.write(b'b')
            if self.USE_QSerialPort:
                self.sio_writer.waitForBytesWritten(500)
            time.sleep(0.2)
            if self.USE_QSerialPort:
                self.sio_writer.waitForReadyRead(500)
            if self.sio_reader.read(1) == b'Y':
                tmp = bytearray(self.sio_reader.read(2))
                return tmp[0] * 256 + tmp[1]
        return 0
   
    def get_bootloader_signature(self):
        
        self.sio_reader.timeout = 1
        if self.USE_QSerialPort:
            junk = self.sio_reader.readAll()
        else:
            junk = self.sio_reader.read()
        self.sio_reader.timeout = .1
        for i in range(20):
            self.start()
            # Bootloader
            self.sio_writer.write(b'S')

            if self.USE_QSerialPort:
                self.sio_writer.waitForBytesWritten(500)

            time.sleep(0.1)
            if self.USE_QSerialPort:
                self.sio_writer.waitForReadyRead(500)
            loader = self.sio_reader.read(7)
            if loader == b'XBoot++':
                return loader
            else:
                self.sio_reader.read(10000)
            time.sleep(.1)
        raise RuntimeError("Invalid bootloader: {}".format(loader))
    
    def send_expect(self, cmd, expected, retries = 5):
        
        expected_len = len(expected)
        self.sio_reader.timeout = .1
        if self.USE_QSerialPort:
            junk = self.sio_reader.readAll()
        else:
            junk = self.sio_reader.read()
        for i in range(retries):
            self.sio_writer.write(cmd)
            if self.USE_QSerialPort:
                self.sio_writer.waitForBytesWritten(500)
            time.sleep(0.1)
            if self.USE_QSerialPort:
                self.sio_writer.waitForReadyRead(500)
            received = bytes(self.sio_reader.read(expected_len))
            if received == expected:
                return True
            time.sleep(.1)
        return False
        
    
    def get_software_version(self):
        """Return the bootloader software version as a string with the format
        MAJOR.MINOR (e.g. "1.7")."""

        time.sleep(.1)
        self.sio_writer.write(b'V')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.1)
        self.sio_reader.timeout = .1
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(500)
        sw_version = bytearray(bytes(self.sio_reader.read(2)))
        if len(sw_version) < 2: return "unknown"
        return "%d.%d" % (sw_version[0] - 48, sw_version[1] - 48)
    
    def get_programmer_type(self):
        
        return b'S' if self.send_expect(b'p', b'S') else None
        
        time.sleep(.1)
        self.sio_writer.write(b'p')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.1)
        self.sio_reader.timeout = .1
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(500)
        return self.sio_reader.read(1)
    
    def get_device_list(self):
        
        # Device Code
        self.sio_reader.timeout = .1
        self.sio_writer.write(b't')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.1)
        device_list = []
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(500)
        while True:
            device = bytearray(self.sio_reader.read(1))
            if device[0] == 0: break
            device_list.append(device)
        
        return device_list
        
    def get_device_signature(self):
        # Read Signature

        self.sio_writer.write(b's')
        if self.USE_QSerialPort:
            self.sio_writer.waitForBytesWritten(500)
        time.sleep(0.1)
        if self.USE_QSerialPort:
            self.sio_writer.waitForReadyRead(500)
        return self.sio_reader.read(3)

