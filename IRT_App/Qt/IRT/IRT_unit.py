import ctypes
from ctypes import *
from enum import Enum




class COMMAND(Enum):
    START_UPDATE = b'U'
    START_UPDATE_OTHERSIDE = b'O'
    START_UPDATE_MAKE_BRIDGE = b'M'
    GET_BODY = b'B'
    SET_CONFIG = b'C'
    RESET_IRT = b'R'
    NORMAL_OPERATION = b'N'
    IN_BOOTLOADER = b'X'
    START_UPDATE_OTHERSIDE_ACKNOWLEDGE = b'A'

    def __str__(self):
        return self.name

class BOARD_TYPE(Enum):
    FPGA = b'F'
    TLK = b'T'
    UNDEFINED = b'\x00'

    def __str__(self):
        return self.name

class IRT_TYPE(Enum):
    BODY = b'B'
    HOLLOW = b'H'
    UNDEFINED = b'\x00'

    def __str__(self):
        return self.name

class RESPONSE(Enum):
    UART_OK = b'O'
    UART_BAD = b'B'
    OtherSide_In_Bootloader = b'L'
    UNDEFINED = b'\x00'

    def __str__(self):
        return self.name


class COM_STATUS(Enum):
    INIT = b'I'
    RUNTIME = b'R'
    UNDEFINED = b'\x00'

    def __str__(self):
        return self.name


class ENABLE(Enum):
    ACTIVE = b'A'
    DEACTIVE = b'D'
    UNDEFINED = b'\x00'

    def __str__(self):
        return self.name
    
class DUPLEX(Enum):
    FULL = b'F'
    HALF = b'H'
    UNDEFINED = b'\x00'

    def __str__(self):
        return self.name

class HEADER(ctypes.Structure):
    _fields_ = [
        ("COMMAND", c_char),
        ("RESPONSE", c_char),
        ("VERSION", ctypes.c_uint16, 16),

        ("SIZEBYTES", ctypes.c_uint16, 16),
        ("CRC_Checksum_INIT", ctypes.c_uint16, 16)
    ]
    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class

        
class IRT_DATA(ctypes.BigEndianStructure):
    _fields_ = [

        ('VALUE_1V1_1V8', ctypes.c_int16, 16),
        ('VALUE_3V3', ctypes.c_int16, 16),

        ('VALUE_5V', ctypes.c_int16, 16),
        ('VALUE_n5V', ctypes.c_int16, 16),

        ('VALUE_2V5', ctypes.c_int16, 16),
        ('VALUE_I3V3', ctypes.c_int16, 16),

        ('VALUE_I5V', ctypes.c_int16, 16),
        ('', ctypes.c_int16, 16),

        ('VALUE_GAIN', ctypes.c_uint16, 16),
        ('VALUE_GAIN_MIN', ctypes.c_uint16, 16),

        ('VALUE_GAIN_MAX', ctypes.c_uint16, 16),
        ('VALUE_TEMPERATURE', ctypes.c_uint16, 16),

        ('PHY0_DUPPLEX', c_char),
        ('PHY1_DUPPLEX', c_char),
        ('LLF_PHY0', c_char),
        ('LLF_PHY1', c_char),

        ('', ctypes.c_uint8, 8),
        ('', ctypes.c_uint8, 8),
        ('LLF_PHY0_LINKUP', ctypes.c_uint8, 8),
        ('LLF_PHY1_LINKUP', ctypes.c_uint8, 8),

        ('SW_VERSION_1', c_char),
        ('SW_VERSION_2', c_char),
        ('SW_VERSION_3', c_char),
        ('SW_VERSION_4', c_char),
        ('SW_VERSION_5', c_char),
        ('SW_VERSION_6', c_char),
        ('SW_VERSION_7', c_char),
        ('SW_VERSION_8', c_char)

    ]
    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class

    
class IRT_BODY(ctypes.Structure):
    _fields_ = [

        ('irt_status', c_char),
        ('board_type', c_char),
        ('connected_to', c_char),
        ('', ctypes.c_uint8, 8),

        ('BODY', IRT_DATA),
        ('HOLLOW', IRT_DATA),

        ('', ctypes.c_uint8, 8),
        ('', ctypes.c_uint8, 8),
        ('CRC_Checksum', ctypes.c_uint16, 16)
    ]
    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class
    

class SETTINGS(ctypes.Structure):
    _fields_ = [

        ('PHY0_DUPPLEX', c_char),
        ('PHY1_DUPPLEX', c_char),
        ('LLF_PHY0', c_char),
        ('LLF_PHY1', c_char)
    ]
    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class

class PC_BODY(ctypes.Structure):
    _fields_ = [

        ('HOLLOW', SETTINGS),
        ('BODY', SETTINGS),

        ('', ctypes.c_uint8, 8),
        ('', ctypes.c_uint8, 8),
        ('CRC_Checksum', ctypes.c_uint16, 16)
    ]
    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class
