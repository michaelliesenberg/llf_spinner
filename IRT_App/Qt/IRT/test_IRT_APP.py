import unittest
import IRT_APP as irt_app
import time
  
class TestStringMethods(unittest.TestCase): 
      
    def setUp(self): 
        pass
  
    # Returns True if the string contains 4 a. 
    def test_main(self):
        testObject = irt_app.Bridge()
        testObject.start_FTDI()
        time.sleep(2)

        if len(testObject.listComPort) > 0:
            testObject.comPortConnectTo = testObject.listComPort[0]
            testObject.start_FTDI()
            testObject.get_configuration()
            time.sleep(2)
            testObject.updateMCU("normal", "/Users/michaelliesenberg/Desktop/D05688E.hex", "false")

            assert True

        else:
            assert False


if __name__ == '__main__': 
    unittest.main() 
