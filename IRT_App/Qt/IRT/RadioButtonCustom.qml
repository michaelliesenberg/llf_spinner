import QtQuick 2.13
import QtQuick.Controls 1.4
import QtQuick.Controls 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.13


RadioButton
{

    indicator: Rectangle
    {
        id: indicatorRect
        implicitWidth: radioHeight
        implicitHeight: radioHeight
        x: parent.leftPadding
        y: parent.height / 2 - height / 2
        radius: parent.height / 2
        border.color: "black"
        color: "#f6f6f6"

        Rectangle
        {
            width: radioHeight/2
            height: radioHeight/2
            x: height/2
            y: height/2
            radius: height/2
            color: "black"
            visible: checked
        }
    }
    contentItem: Text
    {
        id: contentItemComponent
        text: parent.text
        font.pixelSize: indicatorRect.height * 0.8
        fontSizeMode: Text.Fit
        minimumPixelSize: 1
        opacity: enabled ? 1.0 : 0.3
        //color: "black"
        color: "#f6f6f6"
        verticalAlignment: Text.AlignVCenter
        leftPadding: parent.indicator.width + parent.spacing
    }
}
