__author__ = 'Michael Liesenberg'

import sys, serial, serial.tools.list_ports
from PyQt5.QtCore import QThread, QTimer
import time
import ctypes
from ctypes import *
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.uic import loadUi
import subprocess
import binascii

import IRT_unit
from  IRT_unit import HEADER, IRT_BODY, PC_BODY
from pylibftdi import Device

dev = None
myThread = None
isready = False
stop_threads = True
widget = None

IRT_HEADER = HEADER(bytearray(100))
PC_HEADER = HEADER(bytearray(100))
IRT_body = IRT_BODY(bytearray(100))
PC_body = PC_BODY(bytearray(100))


class ExecuteThread(QThread):
    my_signal = pyqtSignal(str)
    stop_exception_signal = pyqtSignal()

    global dev
    global IRT_body
    global IRT_HEADER
    def run(self):
        try:
            while True:
                byteData = dev.read(sizeof(HEADER))

                if len(byteData) >= sizeof(HEADER):
                    IRT_HEADER = IRT_unit.HEADER(bytearray(byteData,'utf8'))
                    data = bytearray(IRT_HEADER)
                    datawithoutcrc = data[: len(data) - 2]
                    print(data)

                    if IRT_HEADER.CRC_Checksum_INIT == binascii.crc_hqx(datawithoutcrc, 0xFFFF):

                        sendToMain = "\nCOMMAND: " + str(IRT_unit.COMMAND(IRT_HEADER.COMMAND)) + \
                                     "\nRESPONSE: " + str(IRT_unit.RESPONSE(IRT_HEADER.RESPONSE)) + \
                                     "\nVERSION: " + str("%.4X" % (IRT_HEADER.VERSION)) + \
                                     "\nCRC_Checksum_INIT: " + str("%.4X" % (IRT_HEADER.CRC_Checksum_INIT))
                        self.my_signal.emit(sendToMain)

                        if IRT_HEADER.COMMAND == b'B':
                            buf = bytes(dev.read(sizeof(IRT_body)))
                            IRT_body = IRT_BODY(buf)
                            data = bytearray(IRT_body)
                            datawithoutcrc = data[:  len(data) - 2]

                            if IRT_body.CRC_Checksum == binascii.crc_hqx(datawithoutcrc, 0xFFFF):

                                sendToMain = "\nVALUE_1V1_1V8 Body: " + str("%i" % (IRT_body.BODY.VALUE_1V1_1V8)) + \
                                            "\nVALUE_3V3: " + str("%i" % (IRT_body.BODY.VALUE_3V3)) + \
                                            "\nVALUE_5V: " + str("%i" % (IRT_body.BODY.VALUE_5V)) + \
                                            "\nVALUE_-5V: " + str("%i" % (IRT_body.BODY.VALUE_n5V)) + \
                                            "\nVALUE_2V5: " + str("%i" % (IRT_body.BODY.VALUE_2V5)) + \
                                            "\nVALUE_I3V3: " + str("%i" % (IRT_body.BODY.VALUE_I3V3)) + \
                                            "\nVALUE_I5V: " + str("%i" % (IRT_body.BODY.VALUE_I5V)) + \
                                            "\nVALUE_In5V: " + str("%i" % (IRT_body.BODY.VALUE_In5V)) + \
                                            "\nVALUE_GAIN: " + str("%i" % (IRT_body.BODY.VALUE_GAIN)) + \
                                            "\nVALUE_GAIN_MIN: " + str("%i" % (IRT_body.BODY.VALUE_GAIN_MIN)) + \
                                            "\nVALUE_GAIN_MAX: " + str("%i" % (IRT_body.BODY.GAIN_MAX)) + \
                                            "\nVALUE_TEMPERATURE: " + str("%i" % (IRT_body.BODY.TEMPERATURE)) + \
                                            "\nVALUE_PHY0_DUPPLEX: " + str("%i" % (IRT_body.BODY.PHY0_DUPPLEX))  + \
                                            "\nVALUE_TEMPERATURE: " + str("%i" % (IRT_body.BODY.TEMPERATURE)) + \
                                            "\nPHY1_DUPPLEX: " + str(IRT_body.BODY.PHY1_DUPPLEX) + \
                                            "\nLLF_PHY0: " + str(IRT_body.BODY.LLF_PHY0) + \
                                            "\nLLF_PHY1: " + str(IRT_body.BODY.LLF_PHY1) + \
                                            "\nSW_VERSION: " + str(IRT_body.BODY.SW_VERSION) + \
                                            "\nVALUE_1V1_1V8 Body: " + str("%i" % (IRT_body.HOLLOW.VALUE_1V1_1V8)) + \
                                            "\nVALUE_3V3: " + str("%i" % (IRT_body.HOLLOW.VALUE_3V3)) + \
                                            "\nVALUE_5V: " + str("%i" % (IRT_body.HOLLOW.VALUE_5V)) + \
                                            "\nVALUE_-5V: " + str("%i" % (IRT_body.HOLLOW.VALUE_n5V)) + \
                                            "\nVALUE_2V5: " + str("%i" % (IRT_body.HOLLOW.VALUE_2V5)) + \
                                            "\nVALUE_I3V3: " + str("%i" % (IRT_body.HOLLOW.VALUE_I3V3)) + \
                                            "\nVALUE_I5V: " + str("%i" % (IRT_body.HOLLOW.VALUE_I5V)) + \
                                            "\nVALUE_In5V: " + str("%i" % (IRT_body.HOLLOW.VALUE_In5V)) + \
                                            "\nVALUE_GAIN: " + str("%i" % (IRT_body.HOLLOW.VALUE_GAIN)) + \
                                            "\nVALUE_GAIN_MIN: " + str("%i" % (IRT_body.HOLLOW.VALUE_GAIN_MIN)) + \
                                            "\nVALUE_GAIN_MAX: " + str("%i" % (IRT_body.HOLLOW.GAIN_MAX)) + \
                                            "\nVALUE_TEMPERATURE: " + str("%i" % (IRT_body.HOLLOW.TEMPERATURE)) + \
                                            "\nVALUE_PHY0_DUPPLEX: " + str("%i" % (IRT_body.HOLLOW.PHY0_DUPPLEX)) + \
                                            "\nVALUE_TEMPERATURE: " + str("%i" % (IRT_body.HOLLOW.TEMPERATURE)) + \
                                            "\nPHY1_DUPPLEX: " + str(IRT_body.HOLLOW.PHY1_DUPPLEX) + \
                                            "\nLLF_PHY0: " + str(IRT_body.HOLLOW.LLF_PHY0) + \
                                            "\nLLF_PHY1: " + str(IRT_body.HOLLOW.LLF_PHY1) + \
                                            "\nSW_VERSION: " + str(IRT_body.HOLLOW.SW_VERSION)


                            self.my_signal.emit(sendToMain)

                time.sleep(0.01)
                global stop_threads
                if stop_threads:
                    if dev != None:
                        dev.close()
                    break
        except Exception as e:
            print("No Hardware connected to PC!")
            stop_threads = True
            self.stop_exception_signal.emit()

class qt(QMainWindow):

    timer = QTimer()

    def __init__(self):
        global myThread

        QMainWindow.__init__(self)
        self.setWindowIcon(QIcon('spinner_logo.png'))
        self.setWindowTitle('IRT Tool')

        loadUi('qt.ui', self)

        self.btn_get_configuration.clicked.connect(self.get_configuration)
        self.btn_send_configuration.clicked.connect(self.send_configuration)
        myThread = ExecuteThread()
        myThread.my_signal.connect(self.onTextReady)
        myThread.stop_exception_signal.connect(self.onThreadExceptionFinished)
        self.start_FTDI()

    def start_FTDI(self):
        global dev
        global stop_threads

        if stop_threads == True:
            global myThread
            try:
                if dev == None:
                    dev = Device(mode='t')

                dev.baudrate = 9600
                dev.bytesize = 8
                dev.stopbits = serial.STOPBITS_ONE
                dev.parity = serial.PARITY_NONE
                dev.timeout = 1
                dev.close()
                dev.open()
                print(dev)
                stop_threads = False
                self.label_5.setText("Connected!")
                self.label_5.setStyleSheet('color: green')
                #myThread = ExecuteThread()
                #myThread.my_signal.connect(self.onTextReady)
                #myThread.stop_exception_signal.connect(self.onThreadExceptionFinished)
                myThread.start()

            except Exception as e:
                dev = None
                self.textEdit.setText('No IRT Tool connected to PC!')
                self.label_5.setText("Disconnected!")
                self.label_5.setStyleSheet('color: red')
                self.timer.timeout.connect(self.start_FTDI)
                self.timer.start(1000)



    def makestringWithCrc(self, string):
        byteString = bytes(string, 'ASCII')
        crc = binascii.crc_hqx(byteString, 0xFFFF)
        returnstring = "%s %.4X" % (string, crc)
        return (returnstring)


    def get_configuration(self):
        global PC_HEADER
        global PC_body

        PC_HEADER.COMMAND = IRT_unit.COMMAND.GET_BODY.value
        PC_HEADER.RESPONSE = IRT_unit.RESPONSE.UART_OK.value
        PC_HEADER.VERSION = 0
        PC_HEADER.SIZEBYTES = len(bytearray(PC_HEADER))

        data = bytearray(PC_HEADER)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)

        PC_HEADER.CRC_Checksum_INIT = crc

        self.write_to_IRT(PC_HEADER)


    def onTextReady(self,i):
        self.textEdit_3.append(i)
        print(i)


    def onThreadExceptionFinished(self):
        global dev
        self.label_5.setText("Disconnected!")
        self.label_5.setStyleSheet('color: red')
        self.textEdit.setText('Connection Stopped! Trying to connect ...')
        dev = None
        self.start_FTDI()

    def send_configuration(self):
        global PC_HEADER
        global PC_body

        PC_HEADER.COMMAND = IRT_unit.COMMAND.SET_CONFIG.value
        PC_HEADER.RESPONSE = IRT_unit.RESPONSE.UART_OK.value
        PC_HEADER.VERSION = 0
        PC_HEADER.SIZEBYTES = len(bytearray(PC_body))

        data = bytearray(PC_HEADER)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)

        PC_HEADER.CRC_Checksum_INIT = crc

        self.write_to_IRT(PC_HEADER)


        time.sleep(2)

        PC_body.HOLLOW.PHY0_DUPPLEX = IRT_unit.DUPLEX.FULL.value
        PC_body.HOLLOW.PHY1_DUPPLEX = IRT_unit.DUPLEX.FULL.value
        PC_body.HOLLOW.LLF_STATUS = IRT_unit.ENABLE.ACTIVE.value
        PC_body.HOLLOW.LLF_PHY0 = IRT_unit.ENABLE.ACTIVE.value
        PC_body.HOLLOW.LLF_PHY1 = IRT_unit.ENABLE.ACTIVE.value

        PC_body.BODY.PHY0_DUPPLEX = IRT_unit.DUPLEX.FULL.value
        PC_body.BODY.PHY1_DUPPLEX = IRT_unit.DUPLEX.FULL.value
        PC_body.BODY.LLF_STATUS = IRT_unit.ENABLE.DEACTIVE.value
        PC_body.BODY.LLF_PHY0 = IRT_unit.ENABLE.DEACTIVE.value
        PC_body.BODY.LLF_PHY1 = IRT_unit.ENABLE.DEACTIVE.value

        data = bytearray(PC_body)
        datawithoutcrc = data[: len(data) - 2]
        crc = binascii.crc_hqx(datawithoutcrc, 0xFFFF)
        PC_body.CRC_Checksum = crc

        self.write_to_IRT(PC_body)

    def write_to_IRT(self, struct_to_send):
        global dev
        buf = (c_char * sizeof(struct_to_send))()
        memmove(buf, byref(struct_to_send), sizeof(struct_to_send))
        if dev != None:
            dev.write(buf)

    def submitclicked(self):
        mytext = str(self.comboBox_4.currentText())
        global dev

        if mytext == "DIAGNOSIS":
            stringWithCrc = self.makestringWithCrc("DIAGNOSE") + "\n"
        elif mytext == "STATUS":
            stringWithCrc = self.makestringWithCrc("S") + "\n"
        elif mytext == "READ CONFIGURATION HOLLOW":
            stringWithCrc = self.makestringWithCrc("RH") + "\n"
        elif mytext == "READ CONFIGURATION BODY":
            stringWithCrc = self.makestringWithCrc("RB") + "\n"
        elif mytext == "ERASE CONFIGURATION HOLLOW":
            stringWithCrc = self.makestringWithCrc("WH0AFFFFFFFFFF") + "\n"
        elif mytext == "ERASE CONFIGURATION BODY":
            stringWithCrc = self.makestringWithCrc("WB0AFFFFFFFFFF") + "\n"
        elif mytext == "HALF DUPLEX HOLLOW PHY0&PHY1":
            stringWithCrc = self.makestringWithCrc("WH1400000400810100040081") + "\n"
        elif mytext == "HALF DUPLEX BODY PHY0&PHY1":
            stringWithCrc = self.makestringWithCrc("WB1400000400810100040081") + "\n"
        elif mytext == "HALF DUPLEX HOLLOW PHY0":
            stringWithCrc = self.makestringWithCrc("WH0A0000040081") + "\n"
        elif mytext == "HALF DUPLEX BODY PHY0":
            stringWithCrc = self.makestringWithCrc("WB0A0000040081") + "\n"
        elif mytext == "HALF DUPLEX HOLLOW PHY1":
            stringWithCrc = self.makestringWithCrc("WH0A0100040081") + "\n"
        elif mytext == "HALF DUPLEX BODY PHY1":
            stringWithCrc = self.makestringWithCrc("WB0A0100040081") + "\n"
        elif mytext == "ACTIVATE LINK LOSS FEATURE HOLLOW":
            stringWithCrc = self.makestringWithCrc("WH0A0200000041") + "\n"
        elif mytext == "DEACTIVATE LINK LOSS FEATURE HOLLOW":
            stringWithCrc = self.makestringWithCrc("WH0A0200000061") + "\n"
        elif mytext == "ACTIVATE LINK LOSS FEATURE BODY":
            stringWithCrc = self.makestringWithCrc("WB0A0200000041") + "\n"
        elif mytext == "DEACTIVATE LINK LOSS FEATURE BODY":
            stringWithCrc = self.makestringWithCrc("WB0A0200000061") + "\n"
        elif mytext == "VERSION":
            stringWithCrc = self.makestringWithCrc("VERSION") + "\n"
        else:
            stringWithCrc = self.makestringWithCrc(mytext) + "\n"

        self.onTextReady(stringWithCrc)
        bytesWithCrc = bytes(stringWithCrc, 'utf8')
        print(bytesWithCrc)
        dev.write(bytesWithCrc)

        if mytext.__contains__(str("UPDATE")):
            self.stop_loop()
            time.sleep(0.1)
            if dev.in_waiting:  # Or: while ser.inWaiting():
                reading = dev.readline().decode('utf-8', errors='replace')

                if reading.__contains__(str("BOOTLOADER")):
                    dev.close()
                    self.onTextReady("Starting AVRDUDE")
                    port = "-P"+str(self.comboBox_3.currentText())
                    #self.onTextReady(subprocess.call(['avrdude','-cavr109','-patxmega32e5', '-P/dev/tty.usbmodem1842010029671','-b 9600','-U flash:w:IRT-Diag.hex']))
                    output = str(subprocess.call(['avrdude', '-cavr109', '-patxmega32e5', port, '-b 115200','-U flash:w:IRT-Diag.hex']))
                    self.onTextReady(output)
                    dev.open()
                    self.start_loop()
                    self.onTextReady("Stop AVRDUDE")
            else:
                self.start_loop()
                self.onTextReady("No Bootlaoder message returned")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = qt()
    widget.show()
    sys.exit(app.exec_())
