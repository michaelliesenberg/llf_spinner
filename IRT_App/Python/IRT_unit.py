import ctypes
from ctypes import *
from enum import Enum


class COMMAND(Enum):
    START_UPDATE = b'U'
    GET_BODY = b'B'
    SET_CONFIG = b'C'
    RESET_IRT = b'R'
    NORMAL_OPERATION = b'N'


class BOARD_TYPE(Enum):
    FPGA = b'F'
    TLK = b'T'


class IRT_TYPE(Enum):
    BODY = b'B'
    HOLLOW = b'H'


class RESPONSE(Enum):
    UART_OK = b'O'
    UART_BAD = b'B'


class COM_STATUS(Enum):
    INIT = b'I'
    RUNTIME = b'R'


class ENABLE(Enum):
    ACTIVE = b'A'
    DEACTIVE = b'D'


class DUPLEX(Enum):
    FULL = b'F'
    HALF = b'H'


class HEADER(ctypes.Structure):
    _fields_ = [
        ("COMMAND", c_char),
        ("RESPONSE", c_char),
        ("VERSION", ctypes.c_uint16, 16),

        ("SIZEBYTES", ctypes.c_uint16, 16),
        ("CRC_Checksum_INIT", ctypes.c_uint16, 16)
    ]

    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class


class IRT_DATA(ctypes.Structure):
    _fields_ = [

        ('VALUE_1V1_1V8', ctypes.c_int16, 16),
        ('VALUE_3V3', ctypes.c_int16, 16),

        ('VALUE_5V', ctypes.c_int16, 16),
        ('VALUE_n5V', ctypes.c_int16, 16),

        ('VALUE_2V5', ctypes.c_int16, 16),
        ('VALUE_I3V3', ctypes.c_int16, 16),

        ('VALUE_I5V', ctypes.c_int16, 16),
        ('VALUE_In5V', ctypes.c_int16, 16),

        ('VALUE_GAIN', ctypes.c_uint16, 16),
        ('VALUE_GAIN_MIN', ctypes.c_uint16, 16),

        ('VALUE_GAIN_MAX', ctypes.c_uint16, 16),
        ('VALUE_TEMPERATURE', ctypes.c_uint16, 16),

        ('PHY0_DUPPLEX', c_char),
        ('PHY1_DUPPLEX', c_char),
        ('LLF_PHY0', c_char),
        ('LLF_PHY1', c_char),

        ('', ctypes.c_uint8, 8),
        ('', ctypes.c_uint8, 8),
        ('SW_VERSION', ctypes.c_uint16, 16)

    ]

    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class


class IRT_BODY(ctypes.Structure):
    _fields_ = [

        ('irt_status', c_char),
        ('board_type', c_char),
        ('connected_to', c_char),
        ('', ctypes.c_uint8, 8),

        ('BODY', IRT_DATA),
        ('HOLLOW', IRT_DATA),

        ('', ctypes.c_uint8, 8),
        ('', ctypes.c_uint8, 8),
        ('CRC_Checksum', ctypes.c_uint16, 16)
    ]

    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class


class SETTINGS(ctypes.Structure):
    _fields_ = [

        ('PHY0_DUPPLEX', c_char),
        ('PHY1_DUPPLEX', c_char),
        ('LLF_PHY0', c_char),
        ('LLF_PHY1', c_char)
    ]

    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class


class PC_BODY(ctypes.Structure):
    _fields_ = [

        ('HOLLOW', SETTINGS),
        ('BODY', SETTINGS),

        ('', ctypes.c_uint8, 8),
        ('', ctypes.c_uint8, 8),
        ('CRC_Checksum', ctypes.c_uint16, 16)
    ]

    def __new__(cls, buf):
        return cls.from_buffer_copy(buf)

    def __init__(self, data):
        pass  ## data is already present in class
