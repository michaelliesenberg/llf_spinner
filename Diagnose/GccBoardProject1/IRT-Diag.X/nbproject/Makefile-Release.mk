#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-Release.mk)" "nbproject/Makefile-local-Release.mk"
include nbproject/Makefile-local-Release.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=Release
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/ASF/common/services/clock/xmega/sysclk.c ../src/ASF/common/services/ioport/xmega/ioport_compat.c ../src/ASF/common/services/serial/usart_serial.c ../src/ASF/common/services/sleepmgr/xmega/sleepmgr.c ../src/ASF/xmega/boards/xmega_e5_xplained/init.c ../src/ASF/xmega/drivers/ac/ac.c ../src/ASF/xmega/drivers/cpu/ccp.s ../src/ASF/xmega/drivers/tc45/tc45.c ../src/ASF/xmega/drivers/usart/usart.c ../src/fifo.c ../src/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1210819865/sysclk.o ${OBJECTDIR}/_ext/1269781500/ioport_compat.o ${OBJECTDIR}/_ext/1931867146/usart_serial.o ${OBJECTDIR}/_ext/518909634/sleepmgr.o ${OBJECTDIR}/_ext/48923613/init.o ${OBJECTDIR}/_ext/29056832/ac.o ${OBJECTDIR}/_ext/900759350/ccp.o ${OBJECTDIR}/_ext/2141723214/tc45.o ${OBJECTDIR}/_ext/1970355623/usart.o ${OBJECTDIR}/_ext/1360937237/fifo.o ${OBJECTDIR}/_ext/1360937237/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1210819865/sysclk.o.d ${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d ${OBJECTDIR}/_ext/1931867146/usart_serial.o.d ${OBJECTDIR}/_ext/518909634/sleepmgr.o.d ${OBJECTDIR}/_ext/48923613/init.o.d ${OBJECTDIR}/_ext/29056832/ac.o.d ${OBJECTDIR}/_ext/900759350/ccp.o.d ${OBJECTDIR}/_ext/2141723214/tc45.o.d ${OBJECTDIR}/_ext/1970355623/usart.o.d ${OBJECTDIR}/_ext/1360937237/fifo.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1210819865/sysclk.o ${OBJECTDIR}/_ext/1269781500/ioport_compat.o ${OBJECTDIR}/_ext/1931867146/usart_serial.o ${OBJECTDIR}/_ext/518909634/sleepmgr.o ${OBJECTDIR}/_ext/48923613/init.o ${OBJECTDIR}/_ext/29056832/ac.o ${OBJECTDIR}/_ext/900759350/ccp.o ${OBJECTDIR}/_ext/2141723214/tc45.o ${OBJECTDIR}/_ext/1970355623/usart.o ${OBJECTDIR}/_ext/1360937237/fifo.o ${OBJECTDIR}/_ext/1360937237/main.o

# Source Files
SOURCEFILES=../src/ASF/common/services/clock/xmega/sysclk.c ../src/ASF/common/services/ioport/xmega/ioport_compat.c ../src/ASF/common/services/serial/usart_serial.c ../src/ASF/common/services/sleepmgr/xmega/sleepmgr.c ../src/ASF/xmega/boards/xmega_e5_xplained/init.c ../src/ASF/xmega/drivers/ac/ac.c ../src/ASF/xmega/drivers/cpu/ccp.s ../src/ASF/xmega/drivers/tc45/tc45.c ../src/ASF/xmega/drivers/usart/usart.c ../src/fifo.c ../src/main.c

# Pack Options 
PACK_COMPILER_OPTIONS=-I "${DFP_DIR}/include"
PACK_COMMON_OPTIONS=-B "${DFP_DIR}/gcc/dev/atxmega32e5"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-Release.mk dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATxmega32E5
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/900759350/ccp.o: ../src/ASF/xmega/drivers/cpu/ccp.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/900759350" 
	@${RM} ${OBJECTDIR}/_ext/900759350/ccp.o.d 
	@${RM} ${OBJECTDIR}/_ext/900759350/ccp.o 
	@${RM} ${OBJECTDIR}/_ext/900759350/ccp.o.ok ${OBJECTDIR}/_ext/900759350/ccp.o.err 
	 ${MP_CC} $(MP_EXTRA_AS_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -DDEBUG   -x assembler-with-cpp -c -D__$(MP_PROCESSOR_OPTION)__   -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -MD -MP -MF "${OBJECTDIR}/_ext/900759350/ccp.o.d" -MT "${OBJECTDIR}/_ext/900759350/ccp.o.d" -MT ${OBJECTDIR}/_ext/900759350/ccp.o  -o ${OBJECTDIR}/_ext/900759350/ccp.o ../src/ASF/xmega/drivers/cpu/ccp.s  -DXPRJ_Release=$(CND_CONF)  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/900759350/ccp.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--gdwarf-2 -mrelax -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT
	
else
${OBJECTDIR}/_ext/900759350/ccp.o: ../src/ASF/xmega/drivers/cpu/ccp.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/900759350" 
	@${RM} ${OBJECTDIR}/_ext/900759350/ccp.o.d 
	@${RM} ${OBJECTDIR}/_ext/900759350/ccp.o 
	@${RM} ${OBJECTDIR}/_ext/900759350/ccp.o.ok ${OBJECTDIR}/_ext/900759350/ccp.o.err 
	 ${MP_CC} $(MP_EXTRA_AS_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x assembler-with-cpp -c -D__$(MP_PROCESSOR_OPTION)__   -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -MD -MP -MF "${OBJECTDIR}/_ext/900759350/ccp.o.d" -MT "${OBJECTDIR}/_ext/900759350/ccp.o.d" -MT ${OBJECTDIR}/_ext/900759350/ccp.o  -o ${OBJECTDIR}/_ext/900759350/ccp.o ../src/ASF/xmega/drivers/cpu/ccp.s  -DXPRJ_Release=$(CND_CONF)  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/900759350/ccp.o.asm.d" -mrelax -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1210819865/sysclk.o: ../src/ASF/common/services/clock/xmega/sysclk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1210819865" 
	@${RM} ${OBJECTDIR}/_ext/1210819865/sysclk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1210819865/sysclk.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1210819865/sysclk.o.d" -MT "${OBJECTDIR}/_ext/1210819865/sysclk.o.d" -MT ${OBJECTDIR}/_ext/1210819865/sysclk.o  -o ${OBJECTDIR}/_ext/1210819865/sysclk.o ../src/ASF/common/services/clock/xmega/sysclk.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1269781500/ioport_compat.o: ../src/ASF/common/services/ioport/xmega/ioport_compat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1269781500" 
	@${RM} ${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d 
	@${RM} ${OBJECTDIR}/_ext/1269781500/ioport_compat.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d" -MT "${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d" -MT ${OBJECTDIR}/_ext/1269781500/ioport_compat.o  -o ${OBJECTDIR}/_ext/1269781500/ioport_compat.o ../src/ASF/common/services/ioport/xmega/ioport_compat.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1931867146/usart_serial.o: ../src/ASF/common/services/serial/usart_serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1931867146" 
	@${RM} ${OBJECTDIR}/_ext/1931867146/usart_serial.o.d 
	@${RM} ${OBJECTDIR}/_ext/1931867146/usart_serial.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1931867146/usart_serial.o.d" -MT "${OBJECTDIR}/_ext/1931867146/usart_serial.o.d" -MT ${OBJECTDIR}/_ext/1931867146/usart_serial.o  -o ${OBJECTDIR}/_ext/1931867146/usart_serial.o ../src/ASF/common/services/serial/usart_serial.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/518909634/sleepmgr.o: ../src/ASF/common/services/sleepmgr/xmega/sleepmgr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/518909634" 
	@${RM} ${OBJECTDIR}/_ext/518909634/sleepmgr.o.d 
	@${RM} ${OBJECTDIR}/_ext/518909634/sleepmgr.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/518909634/sleepmgr.o.d" -MT "${OBJECTDIR}/_ext/518909634/sleepmgr.o.d" -MT ${OBJECTDIR}/_ext/518909634/sleepmgr.o  -o ${OBJECTDIR}/_ext/518909634/sleepmgr.o ../src/ASF/common/services/sleepmgr/xmega/sleepmgr.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/48923613/init.o: ../src/ASF/xmega/boards/xmega_e5_xplained/init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/48923613" 
	@${RM} ${OBJECTDIR}/_ext/48923613/init.o.d 
	@${RM} ${OBJECTDIR}/_ext/48923613/init.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/48923613/init.o.d" -MT "${OBJECTDIR}/_ext/48923613/init.o.d" -MT ${OBJECTDIR}/_ext/48923613/init.o  -o ${OBJECTDIR}/_ext/48923613/init.o ../src/ASF/xmega/boards/xmega_e5_xplained/init.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/29056832/ac.o: ../src/ASF/xmega/drivers/ac/ac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/29056832" 
	@${RM} ${OBJECTDIR}/_ext/29056832/ac.o.d 
	@${RM} ${OBJECTDIR}/_ext/29056832/ac.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/29056832/ac.o.d" -MT "${OBJECTDIR}/_ext/29056832/ac.o.d" -MT ${OBJECTDIR}/_ext/29056832/ac.o  -o ${OBJECTDIR}/_ext/29056832/ac.o ../src/ASF/xmega/drivers/ac/ac.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/2141723214/tc45.o: ../src/ASF/xmega/drivers/tc45/tc45.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141723214" 
	@${RM} ${OBJECTDIR}/_ext/2141723214/tc45.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141723214/tc45.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2141723214/tc45.o.d" -MT "${OBJECTDIR}/_ext/2141723214/tc45.o.d" -MT ${OBJECTDIR}/_ext/2141723214/tc45.o  -o ${OBJECTDIR}/_ext/2141723214/tc45.o ../src/ASF/xmega/drivers/tc45/tc45.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1970355623/usart.o: ../src/ASF/xmega/drivers/usart/usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1970355623" 
	@${RM} ${OBJECTDIR}/_ext/1970355623/usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1970355623/usart.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1970355623/usart.o.d" -MT "${OBJECTDIR}/_ext/1970355623/usart.o.d" -MT ${OBJECTDIR}/_ext/1970355623/usart.o  -o ${OBJECTDIR}/_ext/1970355623/usart.o ../src/ASF/xmega/drivers/usart/usart.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1360937237/fifo.o: ../src/fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/fifo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/fifo.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1360937237/fifo.o.d" -MT "${OBJECTDIR}/_ext/1360937237/fifo.o.d" -MT ${OBJECTDIR}/_ext/1360937237/fifo.o  -o ${OBJECTDIR}/_ext/1360937237/fifo.o ../src/fifo.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -MT "${OBJECTDIR}/_ext/1360937237/main.o.d" -MT ${OBJECTDIR}/_ext/1360937237/main.o  -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
else
${OBJECTDIR}/_ext/1210819865/sysclk.o: ../src/ASF/common/services/clock/xmega/sysclk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1210819865" 
	@${RM} ${OBJECTDIR}/_ext/1210819865/sysclk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1210819865/sysclk.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1210819865/sysclk.o.d" -MT "${OBJECTDIR}/_ext/1210819865/sysclk.o.d" -MT ${OBJECTDIR}/_ext/1210819865/sysclk.o  -o ${OBJECTDIR}/_ext/1210819865/sysclk.o ../src/ASF/common/services/clock/xmega/sysclk.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1269781500/ioport_compat.o: ../src/ASF/common/services/ioport/xmega/ioport_compat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1269781500" 
	@${RM} ${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d 
	@${RM} ${OBJECTDIR}/_ext/1269781500/ioport_compat.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d" -MT "${OBJECTDIR}/_ext/1269781500/ioport_compat.o.d" -MT ${OBJECTDIR}/_ext/1269781500/ioport_compat.o  -o ${OBJECTDIR}/_ext/1269781500/ioport_compat.o ../src/ASF/common/services/ioport/xmega/ioport_compat.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1931867146/usart_serial.o: ../src/ASF/common/services/serial/usart_serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1931867146" 
	@${RM} ${OBJECTDIR}/_ext/1931867146/usart_serial.o.d 
	@${RM} ${OBJECTDIR}/_ext/1931867146/usart_serial.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1931867146/usart_serial.o.d" -MT "${OBJECTDIR}/_ext/1931867146/usart_serial.o.d" -MT ${OBJECTDIR}/_ext/1931867146/usart_serial.o  -o ${OBJECTDIR}/_ext/1931867146/usart_serial.o ../src/ASF/common/services/serial/usart_serial.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/518909634/sleepmgr.o: ../src/ASF/common/services/sleepmgr/xmega/sleepmgr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/518909634" 
	@${RM} ${OBJECTDIR}/_ext/518909634/sleepmgr.o.d 
	@${RM} ${OBJECTDIR}/_ext/518909634/sleepmgr.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/518909634/sleepmgr.o.d" -MT "${OBJECTDIR}/_ext/518909634/sleepmgr.o.d" -MT ${OBJECTDIR}/_ext/518909634/sleepmgr.o  -o ${OBJECTDIR}/_ext/518909634/sleepmgr.o ../src/ASF/common/services/sleepmgr/xmega/sleepmgr.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/48923613/init.o: ../src/ASF/xmega/boards/xmega_e5_xplained/init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/48923613" 
	@${RM} ${OBJECTDIR}/_ext/48923613/init.o.d 
	@${RM} ${OBJECTDIR}/_ext/48923613/init.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/48923613/init.o.d" -MT "${OBJECTDIR}/_ext/48923613/init.o.d" -MT ${OBJECTDIR}/_ext/48923613/init.o  -o ${OBJECTDIR}/_ext/48923613/init.o ../src/ASF/xmega/boards/xmega_e5_xplained/init.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/29056832/ac.o: ../src/ASF/xmega/drivers/ac/ac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/29056832" 
	@${RM} ${OBJECTDIR}/_ext/29056832/ac.o.d 
	@${RM} ${OBJECTDIR}/_ext/29056832/ac.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/29056832/ac.o.d" -MT "${OBJECTDIR}/_ext/29056832/ac.o.d" -MT ${OBJECTDIR}/_ext/29056832/ac.o  -o ${OBJECTDIR}/_ext/29056832/ac.o ../src/ASF/xmega/drivers/ac/ac.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/2141723214/tc45.o: ../src/ASF/xmega/drivers/tc45/tc45.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141723214" 
	@${RM} ${OBJECTDIR}/_ext/2141723214/tc45.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141723214/tc45.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/2141723214/tc45.o.d" -MT "${OBJECTDIR}/_ext/2141723214/tc45.o.d" -MT ${OBJECTDIR}/_ext/2141723214/tc45.o  -o ${OBJECTDIR}/_ext/2141723214/tc45.o ../src/ASF/xmega/drivers/tc45/tc45.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1970355623/usart.o: ../src/ASF/xmega/drivers/usart/usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1970355623" 
	@${RM} ${OBJECTDIR}/_ext/1970355623/usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1970355623/usart.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1970355623/usart.o.d" -MT "${OBJECTDIR}/_ext/1970355623/usart.o.d" -MT ${OBJECTDIR}/_ext/1970355623/usart.o  -o ${OBJECTDIR}/_ext/1970355623/usart.o ../src/ASF/xmega/drivers/usart/usart.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1360937237/fifo.o: ../src/fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/fifo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/fifo.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1360937237/fifo.o.d" -MT "${OBJECTDIR}/_ext/1360937237/fifo.o.d" -MT ${OBJECTDIR}/_ext/1360937237/fifo.o  -o ${OBJECTDIR}/_ext/1360937237/fifo.o ../src/fifo.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atxmega32e5 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -DNDEBUG -DBOARD=XMEGA_E5_XPLAINED -DIOPORT_XMEGA_COMPAT  -I "../common/applications/user_application/atxmega32e5_xmega_e5_xplained/config" -I "../src/ASF/xmega/utils" -I "../src/config" -I "../src/ASF/common/boards" -I "../src/ASF/xmega/boards/xmega_e5_xplained" -I "../src/ASF/xmega/boards" -I "../src/ASF/xmega/utils/preprocessor" -I "../src/ASF/common/utils" -I "../src/ASF/common/services/ioport" -I "../src" -I "../src/ASF/common/services/gpio" -I "../src/ASF/xmega/drivers/ac" -I "../src/ASF/xmega/drivers/cpu" -I "../src/ASF/xmega/drivers/pmic" -I "../src/ASF/xmega/drivers/sleep" -I "../src/ASF/common/services/clock" -I "../src/ASF/common/services/sleepmgr" -I "../src/ASF/common/services/delay" -I "../src/ASF/common/services/serial/xmega_usart" -I "../src/ASF/common/services/serial" -I "../src/ASF/xmega/drivers/usart" -I "../src/ASF/xmega/drivers/tc45" -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -MT "${OBJECTDIR}/_ext/1360937237/main.o.d" -MT ${OBJECTDIR}/_ext/1360937237/main.o  -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c  -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -std=gnu99 -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Werror-implicit-function-declaration -Wpointer-arith -mrelax -fdata-sections
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atxmega32e5 ${PACK_COMMON_OPTIONS}   -gdwarf-2 -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.map"   -Wl,-section-start=.text=0x0  -o dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1 -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,-lm -Wl,--end-group  -Wl,--relax -Wl,--section-start=.BOOT=0x8000
	
	${MP_CC_DIR}/avr-objcopy -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.eep" || exit 0
	${MP_CC_DIR}/avr-objdump -h -S "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" > "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.lss"
	${MP_CC_DIR}/avr-objcopy -O srec -R .eeprom -R .fuse -R .lock -R .signature "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.srec"
	
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atxmega32e5 ${PACK_COMMON_OPTIONS}  -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.map"   -Wl,-section-start=.text=0x0  -o dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_Release=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION) -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,-lm -Wl,--end-group  -Wl,--relax -Wl,--section-start=.BOOT=0x8000
	${MP_CC_DIR}/avr-objcopy -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.hex"
	${MP_CC_DIR}/avr-objcopy -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.eep" || exit 0
	${MP_CC_DIR}/avr-objdump -h -S "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" > "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.lss"
	${MP_CC_DIR}/avr-objcopy -O srec -R .eeprom -R .fuse -R .lock -R .signature "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/IRT-Diag.X.${IMAGE_TYPE}.srec"
	
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Release
	${RM} -r dist/Release

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
