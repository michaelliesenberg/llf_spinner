#include <asf.h>
#include <string.h>
#include <stdio.h>
#define VOLTAGE_SCALED_VALUE 32
#define CONF_BOARD_ENABLE_USARTD0_REMAP //remap USARTD0 to PD6/PD7

#define FIFO_BUFFER_LENGTH  128
union buffer_element {
    uint8_t  byte;
   uint16_t halfword;
   uint32_t word;
};

        
static int iterator=0;
static int enterBootloader=0;
union buffer_element fifo_buffer[FIFO_BUFFER_LENGTH];
fifo_desc_t fifo_desc;
static volatile uint8_t datain[200]={};
void diag_print(uint8_t data);
void (*jump) (void) = (void (*)(void))(BOOT_SECTION_START/2 + 0x01FC/2);
static void example_ac_window_interrupt(AC_t *ac, uint8_t channel,  enum ac_status_t status)
{
	switch (status) {
		case AC_STATUS_BELOW: //voltage below threshold
		    gpio_set_pin_high(LED0_GPIO);
		    break;
		case AC_STATUS_ABOVE://voltage above threshold
		    gpio_set_pin_low(LED0_GPIO);
		    break;
		default: break;
	}
}

void bootloader(void)
{
   //  CCP = CCP_IOREG_gc;
    // RST.CTRL = RST_SWRST_bm;
    //EIND = 1;
    //jump();
    //asm("ldi r30, 0xfe");      // put 0x01fc(byte addresss/0x00fe(word address) a in Z register (r30-r31 pair)
    //asm("ldi r31, 0x00");
    //asm("eijmp");         
     
   //  asm ("ldi r30, 0xFE\n"  /* Low byte to ZL */
//	  "ldi r31, 0x00\n" /* mid byte to ZH */
//	  "ldi r24, 0x02\n" /* high byte to EIND which lives */
//	  "out 0x3c, r24\n" /* at addr 0x3c in I/O space */
//	  "eijmp":  :: "r24", "r30", "r31");
    //asm volatile("jmp 0x1FC00")
    EIND = BOOT_SECTION_START>>17;
    jump();
}
void diag_print(uint8_t data)
{
	uint8_t write_pointer = 0;
	// uint8_t portdata = 0;
	//initalize as high, to generate correct start bit level
	uint16_t target_data = 0xffff; 
	bool pinstate = false;
	//generate uart frame
	target_data = target_data << 8;
	//add data to frame
	target_data |= data;
	target_data = target_data << 3;
	//add add stop bit
	target_data |= 0x3;
	
	tc45_write_count(&TCC4, 0);
	while (write_pointer < 15) {
		if (tc45_is_overflow(&TCC4)) {
			tc45_clear_overflow(&TCC4);
			//write bit to output
			pinstate = (target_data >> write_pointer) & 0x1; 
			ioport_set_pin_level(J4_PIN0, pinstate);
			ioport_set_pin_level(J4_PIN1, !pinstate);
			write_pointer++;
		}
		
	}
	//reset send pins
	//ioport_set_pin_level(J4_PIN0, false);
	ioport_set_pin_level(J4_PIN1, false);
}

int main (void)
{
	struct ac_config aca_config;
	bool key_pressed = false;

	pmic_init();
	board_init();
	sysclk_init();
	sleepmgr_init();

	//config AC
	memset(&aca_config, 0, sizeof(struct ac_config));
	ac_set_mode(&aca_config, AC_MODE_SINGLE); //AC Mode single compare
	ac_set_voltage_scaler(&aca_config, VOLTAGE_SCALED_VALUE); 
	ac_set_hysteresis(&aca_config, AC_HYSMODE_LARGE_gc); //large hysteresis (28mV)
	ac_set_negative_reference(&aca_config, AC_MUXNEG_PIN1_gc); //negative reference ADC1
	ac_set_positive_reference(&aca_config, AC_MUXPOS_PIN0_gc); //positive reference ADC0
	ac_set_interrupt_mode(&aca_config, AC_INT_MODE_BOTH_EDGES); //interrupt on both edges
	ac_set_interrupt_level(&aca_config, AC_INT_LVL_MED);
	ac_set_interrupt_callback(&ACA, example_ac_window_interrupt); //set interrupt callback
	ac_write_config(&ACA, 0, &aca_config);
	ac_enable(&ACA, 0); //enable AC
	cpu_irq_enable();
	irq_initialize_vectors();
	ioport_set_pin_level(LED0_GPIO, false);
	ioport_set_pin_level(LED0_GPIO, true);
	//init uart
	static usart_rs232_options_t USART_SERIAL_OPTIONS = {
		.baudrate = USART_SERIAL_BAUDRATE,
		.charlength = USART_SERIAL_CHAR_LENGTH,
		.paritytype = USART_SERIAL_PARITY,
		.stopbits = USART_SERIAL_STOP_BIT
	};
	
	sysclk_enable_module(SYSCLK_PORT_D, PR_USART0_bm);
	usart_init_rs232(USART_SERIAL, &USART_SERIAL_OPTIONS);
    usart_set_rx_interrupt_level(USART_SERIAL, USART_INT_LVL_MED);
    ioport_set_pin_dir(USART_SERIAL_RX, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(USART_SERIAL_TX, IOPORT_DIR_OUTPUT);
    
   
    
    
	//timer for uart
	tc45_enable(&TCC4);
	tc45_set_direction(&TCC4, TC45_UP);
	tc45_write_period(&TCC4, 104);
	tc45_write_clock_source(&TCC4, TC_CLKSEL_DIV8_gc);
	// 1 ms mainloop timer
	tc45_enable(&TCC5);
	tc45_set_direction(&TCC5, TC45_UP);
	tc45_write_period(&TCC5, 125);
	tc45_write_clock_source(&TCC5, TC_CLKSEL_DIV64_gc);
	//config send pins
	ioport_set_pin_dir(J4_PIN0, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(J4_PIN1, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(J4_PIN0, false);
	ioport_set_pin_level(J4_PIN1, false);
    
    fifo_init(&fifo_desc, fifo_buffer, FIFO_BUFFER_LENGTH);
    
    
    uint8_t uiStatus;
    uint8_t pull_value = 0;
           

	while(1)
	{
		if (tc45_is_overflow(&TCC5)) 
		{ //1ms mainloop
			tc45_clear_overflow(&TCC5);
			
			if (!ioport_get_pin_level(GPIO_PUSH_BUTTON_1))// && !key_pressed)
			{
                /*gpio_set_pin_low(LED0_GPIO);
                gpio_set_pin_low(LED1_GPIO);
				//key_pressed= true;
                if(usart_putchar(USART_SERIAL,'O')==STATUS_OK)
                {
                    gpio_set_pin_high(LED0_GPIO);
                    gpio_set_pin_high(LED1_GPIO);
                }

                usart_putchar(USART_SERIAL,'K');
                usart_putchar(USART_SERIAL,'\n');*/
                
                //EIND = 1;
                //jump();
                //iterator=0;
                key_pressed= true;               

			}
			/*else if (ioport_get_pin_level(GPIO_PUSH_BUTTON_1) && key_pressed)
			{
				key_pressed=false;
				for (int i=0;i<20;i++)
				{
					diag_print('U');
					delay_ms(80);
				}
                //ioport_set_pin_level(J4_PIN0, false);
                usart_putchar(USART_SERIAL,'B');
                usart_putchar(USART_SERIAL,'O');
                usart_putchar(USART_SERIAL,'O');
                usart_putchar(USART_SERIAL,'T');
                usart_putchar(USART_SERIAL,'\n');
				
                
                gpio_set_pin_low(LED0_GPIO);
                gpio_set_pin_low(LED1_GPIO);
                //EIND = 1;
                //jump();
			}*/
			
		}
        if (!fifo_is_empty(&fifo_desc)) 
		{
            uiStatus = fifo_pull_uint8(&fifo_desc, &pull_value);
            diag_print(pull_value);
        }
       /* if(enterBootloader)
        {
            enterBootloader=0;
            gpio_set_pin_low(LED0_GPIO);
            gpio_set_pin_low(LED1_GPIO);
            delay_ms(2000);
            bootloader();
        }*/
	}
}




ISR(USART_SERIAL_RX_ISR_HANDLER)
{
    uint8_t uiStatus;
    if (usart_rx_is_complete(USART_SERIAL)) 
	{
        uint32_t uiReceivedByte;
        uiReceivedByte = usart_getchar(USART_SERIAL);
        uiStatus = fifo_push_uint8(&fifo_desc, uiReceivedByte);
        
        //diag_print(uiReceivedByte);
		/*static int i=0;
		datain[i]=uiReceivedByte;
		
		if(uiReceivedByte == '\n')
		{
			for(int j=0; j<i+1;j++)
			{
				uiReceivedByte=datain[j];
				uiStatus = fifo_push_uint8(&fifo_desc, uiReceivedByte);
				delay_ms(1);
				diag_print(uiReceivedByte);
				delay_ms(1);
			}
			//while(!usart_rx_is_complete(USART_SERIAL));//Get Stop bit
			delay_ms(80);
			usart_getchar(USART_SERIAL);
			i=0;
		}
		else
			i++;*/
        
		/*datain[iterator]=uiReceivedByte;
		
		if(uiReceivedByte == '\n')
		{
			if( datain[0]=='U' &&
                datain[1]=='P' &&
                datain[2]=='D' &&
                datain[3]=='A' &&
                datain[4]=='T' &&
                datain[5]=='E' &&
                datain[6]==' ' &&
                datain[7]=='7' &&
                datain[8]=='C' &&
                datain[9]=='5' &&
                datain[10]=='D')
            {
                usart_putchar(USART_SERIAL,'B');
                usart_putchar(USART_SERIAL,'O');
                usart_putchar(USART_SERIAL,'O');
                usart_putchar(USART_SERIAL,'T');
                usart_putchar(USART_SERIAL,'L');
                usart_putchar(USART_SERIAL,'O');
                usart_putchar(USART_SERIAL,'A');
                usart_putchar(USART_SERIAL,'D');
                usart_putchar(USART_SERIAL,'E');
                usart_putchar(USART_SERIAL,'R');
                usart_putchar(USART_SERIAL,'\n');

                //delay_ms(2000);
                enterBootloader=1;
            }
			iterator=0;
		}
		else
			iterator++;*/
		
    }
}