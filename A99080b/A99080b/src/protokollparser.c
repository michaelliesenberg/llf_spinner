
#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <string.h>

#include "serialport1.h"
#include "serialport2.h"
#include "crcCalc.h"
#include "protokollparser.h"
#include "diagnosticData.h"

static const char cHexTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void vAddConfigurationDataToSendBuffer(void);

uint8_t ucHexToDec(char cChar);
void vAddBytesAsAsciiToSendBuffer(uint8_t ucByte, bool bWithCrc);
char cDiagGetCharacter(void);
void vPrintDecimal(char* cFormatter, uint16_t uiData1, uint16_t uiData2, uint16_t uiDivisor);
void vAddStringLenToSendBuffer(const char *cBuffer, uint8_t ucLength, bool bAddCrc);
void vCommandEnable(void);
void vCommandRead(void);
void vCommandWrite(void);
void vCommandStatus(void);
void vCommandResponseRead(void);
void vCommandResponseWrite(void);
void vCommandResponseError(void);
void vCommandEmptyLine(void);


uint8_t ucHexToDec(char cChar) {
    if (cChar >= '0' && cChar <= '9') {
        return (uint8_t) cChar - '0';
        } else {
        if (cChar >= 'A' && cChar <= 'F') {
            return (uint8_t) (10 + cChar - 'A');
            } else {
            return 0;
        }
    }
}
uint16_t uiHexStringToDec(char * pcBuffer, uint8_t ucLength) {
    uint16_t uiValue = 0;
    uint8_t ucIndex;
    for(ucIndex = 0; ucIndex < ucLength; ucIndex++) {
        uiValue <<= 4;
        uiValue |= ucHexToDec(pcBuffer[ucIndex]);
    }
    return uiValue;
}


void vAddStringToSendBuffer(const char *cBuffer, bool bAddCrc) {
    char cChar;
    while (cBuffer[0] != 0x00) {
        cChar = cBuffer[0];
        vAddCharToSendBuffer(cChar, bAddCrc);
        cBuffer++;
    }
}

void vAddStringLenToSendBuffer(const char *cBuffer, uint8_t ucLength, bool bAddCrc) {
    uint8_t ucIndex;
    for (ucIndex = 0; ucIndex < ucLength; ucIndex++)
    {
        vAddCharToSendBuffer(cBuffer[ucIndex], bAddCrc);
    }
}

void vAddBytesAsAsciiToSendBuffer(uint8_t ucByte, bool bWithCrc) {
    char cBuffer;
    cBuffer = cHexTable[ucByte >> 4];
    vAddCharToSendBuffer(cBuffer, bWithCrc);

    cBuffer = cHexTable[ucByte & 0x0F];
    vAddCharToSendBuffer(cBuffer, bWithCrc);
}


void vAddConfigurationDataToSendBuffer(void) {
    bool bCrcEeprom;
    uint8_t ucIndex1 = 0;
    uint8_t ucIndex2 = 0;
    uint8_t ucByte;
    uint16_t uiChecksum;

    uint8_t ucOutputBuffer[SEND_BUFFER_SIZE];

    struct stConfigData stStructureConfigData;

    bCrcEeprom = bNvmConfigIsEmpty() || bNvmConfigCrcIsOk();
    if (!bCrcEeprom) {
        vAddCharToSendBuffer(DIAG_RESPONSE_READ, false);

        if (ioport_get_pin_level(BODY_SELECT) == IS_BODY) {
            ucByte = DIAG_RESPONSE_BODY;
            } else {
            ucByte = DIAG_RESPONSE_HOLLOWSHAFT;
        }
        vAddCharToSendBuffer(ucByte, false);
        vAddStringToSendBuffer(DIAG_RESPONSE_CRC_ERROR, false);
        //return;
    }

    do
    {
        vGetNvmConfig(&stStructureConfigData, ucIndex1);
        ucIndex1++;
        if (stStructureConfigData.ucChip == NVM_ID_END) {
            break;
        }
        ucOutputBuffer[ucIndex2] = stStructureConfigData.ucChip;
        ucIndex2++;
        ucOutputBuffer[ucIndex2] = stStructureConfigData.uiAddress >> 8;
        ucIndex2++;
        ucOutputBuffer[ucIndex2] = stStructureConfigData.uiAddress & 0xFF;
        ucIndex2++;
        ucOutputBuffer[ucIndex2] = stStructureConfigData.uiData >> 8;
        ucIndex2++;
        ucOutputBuffer[ucIndex2] = stStructureConfigData.uiData & 0xFF;
        ucIndex2++;
        if (stStructureConfigData.ucChip != NVM_ID_PHY0 && stStructureConfigData.ucChip != NVM_ID_PHY1) {
            break;
        }
    } while (ucIndex2 < (NVM_BLOCK_SIZE * NVM_BLOCK_MAXENTRIES * 2));


    if (!bWaitForCrcFree()) {
        return;
    }
    crc_set_initial_value(0xFFFF);
    crc_io_checksum_byte_start(CRC_16BIT);
    vAddCharToSendBuffer(DIAG_RESPONSE_READ, true);

    if (ioport_get_pin_level(BODY_SELECT) == IS_BODY) {
        ucByte = DIAG_RESPONSE_BODY;
        } else {
        ucByte = DIAG_RESPONSE_HOLLOWSHAFT;
    }
    vAddCharToSendBuffer(ucByte, true);
    vAddCharToSendBuffer(' ', false);

    vAddBytesAsAsciiToSendBuffer(ucIndex2 * 2, true);
    vAddCharToSendBuffer(' ', false);

    for(ucIndex1 = 0; ucIndex1 < ucIndex2; ucIndex1++) {
        vAddBytesAsAsciiToSendBuffer(ucOutputBuffer[ucIndex1], true);
        if (ucIndex1 % 5 == 4) {
            vAddCharToSendBuffer(' ', false);
        }
    }

    if (!bCrcEeprom) {
        vAddBytesAsAsciiToSendBuffer(stStructureConfigData.uiAddress >> 8, true);
        vAddBytesAsAsciiToSendBuffer(stStructureConfigData.uiAddress & 0x00FF, true);
        vAddCharToSendBuffer(' ', false);
    }

    uiChecksum = (uint16_t) crc_io_checksum_byte_stop();

    vReleaseCrc();

    vAddBytesAsAsciiToSendBuffer(uiChecksum >> 8, false);
    vAddBytesAsAsciiToSendBuffer(uiChecksum & 0xFF, false);
    vAddCharToSendBuffer('\n', false);
}


void vCommandEnable(void) {
    // Check: length and character    
    if (ucBytesInCommandBuffer != 11 ||
        cCommandBuffer[1] != 'N' ||
        cCommandBuffer[2] != 'A' ||
        cCommandBuffer[3] != 'B' ||
        cCommandBuffer[4] != 'L' ||
        cCommandBuffer[5] != 'E' ||
        cCommandBuffer[10] != LINE_ENDING[0] ) {
            return;
    }
    // check CRC
    if (bReceivedCrcIsCorrect(cCommandBuffer, 6)) {
        bFlagDiagMode = false;
        if (bCommandFromLocalSide) {
            bFlagEnableConfigurationMode = true;
            vSendConfigurationToOtherSide(cCommandBuffer, ucBytesInCommandBuffer);
        } else {
            bFlagEnableOtherSideConfigurationMode = true;
            vAddStringToSendBuffer(DIAG_RESPONSE_ENABLED_OTHERSIDE, false);
            // TODO: send some string
        }
    }
}

void vCommandRead(void) {
    // in configuration mode:
    // check length
    if ((!bConfigurationMode &&
        !bOtherSideInConfigurationMode )||
        ucBytesInCommandBuffer != 7 ||
        cCommandBuffer[6] != LINE_ENDING[0]) {
            vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
            return;
    }
    // check CRC
    if (bReceivedCrcIsCorrect(cCommandBuffer, 2)) {
        vAddConfigurationDataToSendBuffer();
        if(bCommandFromLocalSide) {
            vClearSendBuffer();
            if (cCommandBuffer[1] == '2') {
                vSendConfigurationToOtherSide(cCommandBuffer, ucBytesInCommandBuffer);
            }
        }
    } else {
        vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
    }
}

void vCommandWrite(void) {
    uint8_t ucBlockSize;
    uint8_t ucByte;

    ucBlockSize = (uint8_t) uiHexStringToDec(cCommandBuffer  + 2, 2);

    // in configuration mode:
    // check
    //    * maximum block size
    //    * bytes in buffer, depending on length value
    if ((!bConfigurationMode &&
        !bOtherSideInConfigurationMode) ||
        ucBlockSize > 2 * NVM_BLOCK_SIZE * NVM_BLOCK_MAXENTRIES ||
        ucBytesInCommandBuffer != (ucBlockSize + 9) ||
        cCommandBuffer[ucBytesInCommandBuffer - 1] != LINE_ENDING[0]) {
            vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
            return;
    }
    // check CRC
    if (bReceivedCrcIsCorrect(cCommandBuffer, ucBlockSize + 4)) {
        if (bWriteConfigToNvm(&cCommandBuffer[4], ucBlockSize)) {
            vAddCharToSendBuffer(DIAG_RESPONSE_WRITE, false);
            if (ioport_get_pin_level(BODY_SELECT) == IS_BODY) {
                ucByte = DIAG_RESPONSE_BODY;
            } else {
                ucByte = DIAG_RESPONSE_HOLLOWSHAFT;
            }
            vAddCharToSendBuffer(ucByte, true);
            vAddCharToSendBuffer(LINE_ENDING[0], false);
        } else {
            vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
        }
        if(bCommandFromLocalSide) {
            vClearSendBuffer();
            if (cCommandBuffer[1] == '2') {
                vSendConfigurationToOtherSide(cCommandBuffer, ucBytesInCommandBuffer);
            }
        }

    } else {
        vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
    }
}

void vCommandStatus(void) {
    // in configuration mode (only local side)
    // check command length
    if (!bConfigurationMode ||
        ucBytesInCommandBuffer != 6 ||
        cCommandBuffer[5] != LINE_ENDING[0]) {
            vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
            return;
    }
    if (bReceivedCrcIsCorrect(cCommandBuffer, 1)) {
        uint8_t ucDiagTxCounter;
        for(ucDiagTxCounter = 0; ucDiagTxCounter < PRINT_INDEX_END; ucDiagTxCounter++) {
            vDiagPrintText(ucDiagTxCounter);
        }
    } else {
        vAddStringToSendBuffer(DIAG_RESPONSE_ERROR, false);
    }
}

void vCommandResponseRead(void) {
    if (!bCommandFromLocalSide) {
        vAddStringLenToSendBuffer(cCommandBuffer, ucBytesInCommandBuffer, false);
        vClearSendBuffer();
    }
}

void vCommandResponseWrite(void) {
    if (!bCommandFromLocalSide) {
        vAddStringLenToSendBuffer(cCommandBuffer, ucBytesInCommandBuffer, false);
        vClearSendBuffer();
    }
}

void vCommandResponseError(void) {
    if (!bCommandFromLocalSide) {
        vAddStringLenToSendBuffer(cCommandBuffer, ucBytesInCommandBuffer, false);
        vClearSendBuffer();
    }
}

void vCommandEmptyLine(void) {
    if (ucBytesInCommandBuffer == 1) {
        vAddCharToSendBuffer(LINE_ENDING[0], false);
        vClearSendBuffer();
    }
}

void vScanCommandLine() {
    if (ucBytesInCommandBuffer != 0) {
        switch (cCommandBuffer[0])
        {
            case DIAG_CMD_ENABLE_DIAG:
                vCommandEnable();
                break;
            case DIAG_CMD_READ:
                vCommandRead();
                break;
            case DIAG_CMD_WRITE:
                vCommandWrite();
                break;
            case DIAG_CMD_STATUS:
                vCommandStatus();
                break;
            case DIAG_CMD_END:
                break;
            case DIAG_RESPONSE_READ:
                vCommandResponseRead();
                break;
            case DIAG_RESPONSE_WRITE:
                vCommandResponseWrite();
                break;
            case DIAG_REMOTE_RESPONSE_ERROR:
                vCommandResponseError();
                break;
            case '\n':
                vCommandEmptyLine();
                break;
        }
        memset(cCommandBuffer, 0, INPUT_BUFFER_SIZE);
        ucBytesInCommandBuffer = 0;
        if (ucBytesInSendBuffer > 0) {
            vSendConfigurationToOtherSide(cSendBuffer, ucBytesInSendBuffer);
        }
    }
}

