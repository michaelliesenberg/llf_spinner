

#ifndef FUSES_H_
#define FUSES_H_

FUSES = {
	0,
	0xFF,
	0xFF,
	0,
	FUSE_SUT0 & FUSE_SUT1 & FUSE_RSTDISBL,
	FUSE_EESAVE,
	0xFF	
};


#endif  /* FUSES_H_ */