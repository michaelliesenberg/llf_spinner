/*
 * diagnostic_data.h
 *
 * Created: 19.01.2018 12:58:58
 *  Author: eyermann
 */ 


#ifndef DIAGNOSTIC_DATA_H_
#define DIAGNOSTIC_DATA_H_

#define PRINT_INDEX_5V          0
#define PRINT_INDEX_3V3         1
#define PRINT_INDEX_1V8         2
#define PRINT_INDEX_2V5         3
#define PRINT_INDEX_M5V         4
#define PRINT_INDEX_I3V3        5
#define PRINT_INDEX_I5V         6
#define PRINT_INDEX_VGAIN       7
#define PRINT_INDEX_VGAIN_MIN   8
#define PRINT_INDEX_VGAIN_MAX   9
#define PRINT_INDEX_TEMPERATURE 10
#define PRINT_INDEX_CONNECTEDTO 11
#define PRINT_INDEX_END         12

#define S_5V_START              "V50v     : B: "
#define S_3V3_START             "V33v     : B: "
#define S_1V8_START             "V18v     : B: "
#define S_2V5_START             "V25v     : B: "
#define S_VGAIN_START           "Vgain    : B: "
#define S_VGAIN_MIN_START       "Vgain_min: B: "
#define S_VGAIN_MAX_START       "Vgain_max: B: "
#define S_VOLTAGE_MID           " V, H: "
#define S_VOLTAGE_END           " V" LINE_ENDING

#define S_M5V_START             "-5V      : B: "
#define S_M5V_MID               ", H: "
#define S_M5V_END               LINE_ENDING
#define S_M5V_OK                "true"
#define S_M5V_NOK               "false"

#define S_I5V_START             "I50v     : B: "
#define S_I33V_START            "I33v     : B: "
#define S_CURRENT_MID           " mA, H: "
#define S_CURRENT_END           " mA" LINE_ENDING

#define S_TEMPERATURE_START     "Temp     : B: "
#define S_TEMPERATURE_MID       " C, H: "
#define S_TEMPERATURE_END       " C" LINE_ENDING

#define S_CONNECTEDTO_START     "Connected: "
#define S_CONNECTEDTO_BODY      "body"
#define S_CONNECTEDTO_HOLLOWSHAFT "hollow shaft"
#define S_CONNECTEDTO_END       LINE_ENDING LINE_ENDING

void vDiagPrintText(uint8_t ucIndex);


#endif /* DIAGNOSTIC_DATA_H_ */