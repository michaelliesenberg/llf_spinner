/*
 * led.c
 *
 * Created: 04.01.2018 07:52:56
 *  Author: Eyermann
 *
 * This file contains routines to drive the LED
 */ 

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <util/delay.h>

#include "led.h"


void vLedBlink3(void) {
    uint8_t uiCounter = 0;
    ioport_set_pin_level(LED, LED_OFF);
    
    for(uiCounter = 0; uiCounter < 5; uiCounter++) {
        ioport_toggle_pin_level(LED);
        _delay_ms(200);
    }
}